<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:52
 */
namespace Controller;

use DAL\OfertaDAO;
use Model\Oferta;

class OfertaController
{
    private $oferDAO;

    /**
     * OfertaController constructor.
     */
    public function __construct()
    {
        $this->oferDAO = new OfertaDAO();
    }

    public function Gravar(Oferta $oferta, $idCaixa)
    {

        return $this->oferDAO->Gravar($oferta, $idCaixa);

    }

    public function ObterOfertas($idIgreja)
    {
        if($idIgreja > 0){
            return $this->oferDAO->ObterOfertas($idIgreja);
        }else{
            return null;
        }
    }

    public function RelatorioOfertas($idIgreja, $inicio, $fim)
    {
        $ofertas = $this->oferDAO->RelatorioOfertas($idIgreja, $inicio, $fim);
        $total = 0;

        $html = '<style>
                    th, td {
                        text-align: left;
                    }
                    tr:nth-child(2n+2) {
                        background: #DCDCDC;
                    }
                    #footer {
                        font-size: 12px;
                    }
                    #header{
                
                    }
                    #borda{
                        border: 2px solid #000000;
                    }
                </style>';

        $html .= '<table bgcolor="#FFFFFF" align="center" border="0" cellpadding="10" cellspacing="0" width="100%" style="padding: 20px 40px">';
        $html .= '<tr>';
        $html .= '<td style="text-align: center; font-size: 16px"> RELATÓRIO DE OFERTAS - Período: '.date('d/m/Y', strtotime($inicio)).' a '.date('d/m/Y', strtotime($fim)).'</td>';
        $html .= '</tr>';
        $html .= '</table>';

        $html .= '<table bgcolor="#FFFFFF" align="center" border="0" cellpadding="10" cellspacing="0" width="100%" style="min-width: 600px; font-size: 12px; padding: 0 40px">
                    <tbody>
                    <tr>
                        <th>#CÓD </th>
                        <th>MEMBRO</th>
                        <th>DATA LANÇAMENTO</th>        
                        <th>VALOR</th>
                    </tr>';
        if(count($ofertas) > 0){
            foreach ($ofertas as $oferta){
                $html .= '<tr>';
                $html .= '<td>'. $oferta->getId() .'</td>';
                $html .= '<td>'. $oferta->membro->getNome() .'</td>';
                $html .= '<td>'. date('d/m/Y', strtotime($oferta->getData())) .'</td>';
                $html .= '<td>'. number_format($oferta->getValor(), 2, ',', '.') .'</td>';
                $html .= '</tr>';
                (float) $total += (float) $oferta->getValor();
            }
        }else{
            $html .= '<tr>';
            $html .= '<td colspan="5" style="text-align: left; text-transform: uppercase"><b>Não há ofertas registradas nesse período.</b></td>';
            $html .= '</tr>';
        }

        $html .= '<tr>';
        $html .= '<td colspan="5" style="text-align: right"><b>TOTAL: '.number_format($total, 2, ',', '.').'</b></td>';
        $html .= '</tr>';

        $html .= '</tbody>';
        $html .= '</table>';

        return $html;
    }
}