<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:52
 */

namespace Controller;

use DAL\MovimentoCaixaDAO;
use Model\MovimentoCaixa;

class MovimentoCaixaController
{
    private $movDAO;

    /**
     * MovimentoCaixaController constructor.
     * @param $movDAO
     */
    public function __construct()
    {
        $this->movDAO = new MovimentoCaixaDAO();
    }

    public function Gravar(MovimentoCaixa $movimento)
    {
        if ($movimento->getValor() != "" && strlen($movimento->getDescricao()) > 5 && $movimento->getData() != "" && $movimento->getTipo() != "") {
            return $this->movDAO->Gravar($movimento);
        } else {
            return false;
        }
    }

    public function ObterMovimentacoes($idIgreja)
    {
        if ($idIgreja > 0) {
            return $this->movDAO->ObterMovimentacoes($idIgreja);
        } else {
            return null;
        }
    }

    public function RelatorioMovimentacoes($idIgreja, $inicio, $fim)
    {
        $movimentos = $this->movDAO->RelatorioMovimentacoes($idIgreja, $inicio, $fim);
        $total = 0;

        $html = '<style>
                    th, td {
                        text-align: left;
                    }
                    tr:nth-child(2n+2) {
                        background: #DCDCDC;
                    }
                    #footer {
                        font-size: 12px;
                    }
                    #header{
                
                    }
                    #borda{
                        border: 2px solid #000000;
                    }
                </style>';

        $html .= '<table bgcolor="#FFFFFF" align="center" border="0" cellpadding="10" cellspacing="0" width="100%" style="padding: 20px 40px">';
        $html .= '<tr>';
        $html .= '<td style="text-align: center; font-size: 16px"> RELATÓRIO DE MOVIMENTAÇÕES DE CAIXA - Período: ' . date('d/m/Y', strtotime($inicio)) . ' a ' . date('d/m/Y', strtotime($fim)) . '</td>';
        $html .= '</tr>';
        $html .= '</table>';

        $html .= '<table bgcolor="#FFFFFF" align="center" border="0" cellpadding="10" cellspacing="0" width="100%" style="min-width: 600px; font-size: 12px; padding: 0 40px">
                    <tbody>
                    <tr>
                        <th>#CÓD </th>
                        <th>DESCRIÇÃO</th>
                        <th>DATA</th>                                              
                        <th>USUÁRIO</th>
                         <th>TIPO</th>       
                        <th>VALOR</th>
                    </tr>';
        if (count($movimentos) > 0) {
            foreach ($movimentos as $mov) {
                $html .= '<tr>';
                $html .= '<td>' . $mov->getId() . '</td>';
                $html .= '<td>' . $mov->getDescricao() . '</td>';
                $html .= '<td>' . date('d/m/Y H:i', strtotime($mov->getData())) . '</td>';
                $html .= '<td>' . $mov->usuario->getNome() . '</td>';
                if ($mov->getTipo() == 1) {
                    $html .= '<td>CRÉDITO</td>';
                } else {
                    $html .= '<td>DÉBITO</td>';
                }
                $html .= '<td>' . number_format($mov->getValor(), 2, ',', '.') . '</td>';
                $html .= '</tr>';
                if ($mov->getTipo() == 1) {
                    (float)$total += (float)$mov->getValor();
                } else {
                    (float)$total -= (float)$mov->getValor();
                }
            }
        } else {
            $html .= '<tr>';
            $html .= '<td colspan="5" style="text-align: left; text-transform: uppercase"><b>Não há movimentações registradas nesse período.</b></td>';
            $html .= '</tr>';
        }

        $html .= '<tr>';
        $html .= '<td colspan="6" style="text-align: right"><b>TOTAL: ' . number_format($total, 2, ',', '.') . '</b></td>';
        $html .= '</tr>';

        $html .= '</tbody>';
        $html .= '</table>';

        return $html;
    }

    public function RelatorioFinanceiro($idIgreja, $inicio, $fim)
    {
        $dados = $this->movDAO->RelatorioFinanceiro($idIgreja, $inicio, $fim);
        $total = 0;

        $html = '<style>
                    th, td {
                        text-align: left;
                    }
                    tr:nth-child(2n+2) {
                        background: #DCDCDC;
                    }
                    #footer {
                        font-size: 12px;
                    }
                    #header{
                
                    }
                    #borda{
                        border: 2px solid #000000;
                    }
                </style>';

        $html .= '<table bgcolor="#FFFFFF" align="center" border="0" cellpadding="10" cellspacing="0" width="100%" style="padding: 20px 40px">';
        $html .= '<tr>';
        $html .= '<td style="text-align: center; font-size: 16px"> RELATÓRIO FINANCEIRO DE CAIXA - Período: ' . date('d/m/Y', strtotime($inicio)) . ' a ' . date('d/m/Y', strtotime($fim)) . '</td>';
        $html .= '</tr>';
        $html .= '</table>';

        $html .= '<table bgcolor="#FFFFFF" align="center" border="0" cellpadding="10" cellspacing="0" width="100%" style="min-width: 600px; font-size: 12px; padding: 0 40px">
                    <tbody>';

        if (count($dados) > 0) {
            $html .= '<tr>
                        <th>SOMA DOS CRÉDITOS </th>
                        <td>' . number_format($dados[0]['soma'], 2, ',', '.') . '</td>';
             $html .= '</tr>';
            $html .= '<tr>';
             $html .= '<th>SOMA DOS DÉBITOS </th>
                        <td>' . number_format($dados[1]['soma'], 2, ',', '.') . '</td>
                      </tr>';

            (float) $total = (float) $dados[0]['soma'] - $dados[1]['soma'];
            $html .= '<tr>';
            $html .= '<td colspan="6" style="text-align: right"><b>SALDO DO PERÍODO: ' . number_format($total, 2, ',', '.') . '</b></td>';
            $html .= '</tr>';

        } else {
            $html .= '<tr>';
            $html .= '<td colspan="5" style="text-align: left; text-transform: uppercase"><b>Não há registros nesse período.</b></td>';
            $html .= '</tr>';
        }

        $html .= '</tbody>';
        $html .= '</table>';

        return $html;
    }
}