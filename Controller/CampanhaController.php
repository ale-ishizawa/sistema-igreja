<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:47
 */
namespace Controller;

use DAL\CampanhaDAO;
use Model\Campanha;

class CampanhaController
{
    private $camDAO;

    public function __construct()
    {
        $this->camDAO = new CampanhaDAO();
    }

    public function Gravar(Campanha $campanha)
    {
        if(strlen($campanha->getNome()) >= 3 && $campanha->getValorAlvo() != "" && $campanha->getDataInicio() != ""
            && $campanha->getDataFim() != "" && $campanha->igreja->getId() > 0){
            return $this->camDAO->Gravar($campanha);
        }else{
            return false;
        }
    }

    public function Encerrar($idCampanha)
    {
        if($idCampanha > 0){
            return $this->camDAO->Encerrar($idCampanha);
        }else{
            return false;
        }
    }

    public function Editar(Campanha $campanha)
    {
        if(strlen($campanha->getNome()) >= 3 && $campanha->getValorAlvo() != "" && $campanha->getDataInicio() != ""
            && $campanha->getDataFim() != "" && $campanha->igreja->getId() > 0 && $campanha->getId() > 0){
            return $this->camDAO->Editar($campanha);
        }else{
            return false;
        }
    }

    public function Deletar($id)
    {
        if($id > 0){
            return $this->camDAO->Deletar($id);
        }else{
            return false;
        }
    }

    public function ObterCampanhas($idIgreja)
    {
        if($idIgreja > 0){
            return $this->camDAO->ObterCampanhas($idIgreja);
        }else{
            return null;
        }
    }

    public function ObterCampanha($id)
    {
        if($id > 0){
            return $this->camDAO->ObterCampanha($id);
        }else{
            return null;
        }
    }

    public function VerificaLancamentoCampanha($id)
    {
        if($id > 0){
            return $this->camDAO->VerificaLancamentoCampanha($id);
        }else{
            return null;
        }
    }

    public function RelatorioCampanhas($idIgreja, $inicio, $fim)
    {
        $campanhas = $this->camDAO->RelatorioCampanhas($idIgreja, $inicio, $fim);
        $total = 0;

        $html = '<style>
                    th, td {
                        text-align: left;
                    }
                    tr:nth-child(2n+2) {
                        background: #DCDCDC;
                    }
                    #footer {
                        font-size: 12px;
                    }
                    #header{
                
                    }
                    #borda{
                        border: 2px solid #000000;
                    }
                </style>';

        $html .= '<table bgcolor="#FFFFFF" align="center" border="0" cellpadding="10" cellspacing="0" width="100%" style="padding: 20px 40px">';
        $html .= '<tr>';
        $html .= '<td style="text-align: center; font-size: 16px"> RELATÓRIO DE CAMPANHAS - Período: '.date('d/m/Y', strtotime($inicio)).' a '.date('d/m/Y', strtotime($fim)).'</td>';
        $html .= '</tr>';
        $html .= '</table>';

        $html .= '<table bgcolor="#FFFFFF" align="center" border="0" cellpadding="10" cellspacing="0" width="100%" style="min-width: 600px; font-size: 12px; padding: 0 40px">
                    <tbody>
                    <tr>
                        <th>DESCRIÇÃO</th>
                        <th>NOME</th>                        
                        <th>VALOR ALVO</th>
                        <th>VALOR ATINGIDO NO PERÍODO</th>
                    </tr>';
        if(count($campanhas) > 0){
            foreach ($campanhas as $row){
                $html .= '<tr>';
                $html .= '<td>'. $row['descricao'] .'</td>';
                $html .= '<td>'. $row['nome'] .'</td>';
                $html .= '<td align="right">'. number_format($row['valoralvo'], 2, ',', '.') .'</td>';
                $html .= '<td align="right">'. number_format($row['soma'], 2, ',', '.') .'</td>';
                $html .= '</tr>';
                (float) $total += (float) $row['soma'];
            }
        }else{
            $html .= '<tr>';
            $html .= '<td colspan="5" style="text-align: left; text-transform: uppercase"><b>Não há campanhas registradas nesse período.</b></td>';
            $html .= '</tr>';
        }

        $html .= '<tr>';
        $html .= '<td colspan="5" style="text-align: right"><b>SOMA VALOR TOTAL ATINGIDO DAS CAMPANHAS: '.number_format($total, 2, ',', '.').'</b></td>';
        $html .= '</tr>';

        $html .= '</tbody>';
        $html .= '</table>';

        return $html;
    }
}