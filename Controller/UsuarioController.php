<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 29/04/2018
 * Time: 22:02
 */
namespace Controller;
use DAL\UsuarioDAO;
use Model\Usuario;


class UsuarioController
{
    private $usuDAO;

    public function __construct()
    {
        $this->usuDAO = new UsuarioDAO();
    }

    public function Gravar(Usuario $usuario)
    {
        if (strlen($usuario->getNome()) >= 3 && strlen($usuario->getSenha()) >= 5 && strpos($usuario->getEmail(), "@") && strpos($usuario->getEmail(), ".")
            && $usuario->getAtivo() == 1 && $usuario->getPerfil() > 0 && strlen($usuario->getUsuario()) >= 3) {

            return $this->usuDAO->Gravar($usuario);
        } else {
            return false;
        }
    }

    public function VerificaUsuarioExiste(string $user)
    {
        if(strlen($user) >= 3){
            return $this->usuDAO->VerificaUsuarioExiste($user);
        }else{
            -10;
        }
    }

    public function VerificaEmailExiste(string $email)
    {
        if (strpos($email, "@") > 0 && strpos($email, ".") > 0) {
            return $this->usuDAO->VerificaEmailExiste($email);
        } else {
            -10;
        }
    }

    public function ObterUsuarios()
    {
        return $this->usuDAO->ObterUsuarios();
    }

}