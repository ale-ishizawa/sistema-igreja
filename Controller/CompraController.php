<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:48
 */
namespace Controller;

use DAL\CompraDAO;
use Model\Compra;

class CompraController
{
    private $comDAO;

    /**
     * CompraController constructor.
     */
    public function __construct()
    {
        $this->comDAO = new CompraDAO();
    }

    public function Gravar(Compra $compra)
    {
        if(strlen($compra->getDescricao()) >= 10 && $compra->getDataCompra() != ""){
            return $this->comDAO->Gravar($compra);
        }else{
            return false;
        }
    }

    /**
     * @param $idIgrejaSessao
     * @return array|null
     * @throws \Exception
     */
    public function ObterCompras($idIgrejaSessao)
    {
        if($idIgrejaSessao > 0){
            return $this->comDAO->ObterCompras($idIgrejaSessao);
        }else{
            return null;
        }

    }

    public function Estornar($idCompra, $idCaixa)
    {
        if($idCompra > 0){
            return $this->comDAO->Estornar($idCompra, $idCaixa);
        }else{
            return false;
        }
    }

    public function RelatorioCompras($idIgreja, $inicio, $fim)
    {
        $compras = $this->comDAO->RelatorioCompras($idIgreja, $inicio, $fim);
        $total = 0;

        $html = '<style>
                    th, td {
                        text-align: left;
                    }
                    tr:nth-child(2n+2) {
                        background: #DCDCDC;
                    }
                    #footer {
                        font-size: 12px;
                    }
                    #header{
                
                    }
                    #borda{
                        border: 2px solid #000000;
                    }
                </style>';

        $html .= '<table bgcolor="#FFFFFF" align="center" border="0" cellpadding="10" cellspacing="0" width="100%" style="padding: 20px 40px">';
        $html .= '<tr>';
        $html .= '<td style="text-align: center; font-size: 16px"> RELATÓRIO DE COMPRAS - Período: '.date('d/m/Y', strtotime($inicio)).' a '.date('d/m/Y', strtotime($fim)).'</td>';
        $html .= '</tr>';
        $html .= '</table>';

        $html .= '<table bgcolor="#FFFFFF" align="center" border="0" cellpadding="10" cellspacing="0" width="100%" style="min-width: 600px; font-size: 12px; padding: 0 40px">
                    <tbody>
                    <tr>
                        <th>#CÓD </th>
                        <th>DESCRIÇÃO</th>
                        <th>DATA DA COMPRA</th>                        
                        <th>USUÁRIO</th>
                        <th>VALOR</th>
                    </tr>';
        if(count($compras) > 0){
            foreach ($compras as $compra){
                $html .= '<tr>';
                $html .= '<td>'. $compra->getId() .'</td>';
                $html .= '<td>'. $compra->getDescricao() .'</td>';
                $html .= '<td>'. date('d/m/Y', strtotime($compra->getDataCompra())) .'</td>';
                $html .= '<td>'. $compra->getUsuario() .'</td>';
                $html .= '<td>'. number_format($compra->getValor(), 2, ',', '.') .'</td>';
                $html .= '</tr>';
                (float) $total += (float) $compra->getValor();
            }
        }else{
            $html .= '<tr>';
            $html .= '<td colspan="5" style="text-align: left; text-transform: uppercase"><b>Não há compras registradas nesse período.</b></td>';
            $html .= '</tr>';
        }

        $html .= '<tr>';
        $html .= '<td colspan="5" style="text-align: right"><b>TOTAL: '.number_format($total, 2, ',', '.').'</b></td>';
        $html .= '</tr>';

        $html .= '</tbody>';
        $html .= '</table>';

        return $html;
    }

}