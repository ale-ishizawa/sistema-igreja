<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:49
 */
namespace Controller;

use DAL\IgrejaDAO;
use Model\Igreja;

class IgrejaController
{
    private $igrejaDAO;

    public function __construct()
    {
        $this->igrejaDAO = new IgrejaDAO();
    }

    public function Gravar(Igreja $igreja)
    {
        if(strlen($igreja->getNome()) >= 3 && strpos($igreja->getEmail(), "@") && strpos($igreja->getEmail(), ".")
            && strlen($igreja->getBairro()) >= 3 && strlen($igreja->getLogradouro()) >= 3 && strlen($igreja->getSite()) >= 10){
            return $this->igrejaDAO->Gravar($igreja);
        }else{
            return false;
        }
    }

    public function Editar(Igreja $igreja)
    {
        if(strlen($igreja->getNome()) >= 3 && strpos($igreja->getEmail(), "@") && strpos($igreja->getEmail(), ".")
            && strlen($igreja->getBairro()) >= 3 && strlen($igreja->getLogradouro()) >= 3 && strlen($igreja->getSite()) >= 10
            && $igreja->getId() > 0){
            return $this->igrejaDAO->Editar($igreja);
        }else{
            return false;
        }
    }

    public function ObterIgrejasSelect()
    {
        return $this->igrejaDAO->ObterIgrejasSelect();
    }

    public function ObterIgreja($idIgreja)
    {
        if($idIgreja > 0){
            return $this->igrejaDAO->ObterIgreja($idIgreja);
        }else{
            return null;
        }
    }

    public function Inativar($idgreja)
    {
        if($idgreja > 0){
            return $this->igrejaDAO->Inativar();
        }else{
            return null;
        }
    }

}