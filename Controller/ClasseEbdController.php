<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:48
 */
namespace Controller;

use Model\ClasseEbd;
use DAL\ClasseEbdDAO;

class ClasseEbdController
{
    private $classeDAO;

    /**
     * ClasseEbdController constructor.
     */
    public function __construct()
    {
        $this->classeDAO = new ClasseEbdDAO();
    }

    public function Gravar(ClasseEbd $classe)
    {
        if (strlen($classe->getNome()) >= 3) {
            return $this->classeDAO->Gravar($classe);
        } else {
            return false;
        }
    }

    public function Deletar($id)
    {
        if ($id > 0) {
            return $this->classeDAO->Deletar($id);
        } else {
            return false;
        }
    }

    public function Editar(ClasseEbd $classe)
    {
        if ($classe->getId() > 0 && strlen($classe->getNome()) >= 3) {
            return $this->classeDAO->Editar($classe);
        } else {
            return false;
        }
    }

    public function ObterClasses($idIgreja)
    {
        if($idIgreja > 0){
            return $this->classeDAO->ObterClasses($idIgreja);
        }else{
            return null;
        }
    }

    public function ObterClassePorCod($id)
    {
        if($id > 0){
            return $this->classeDAO->ObterClassePorCod($id);
        }else{
            return false;
        }
    }


}