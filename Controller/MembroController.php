<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:50
 */

namespace Controller;

use Model\Membro;
use DAL\MembroDAO;

class MembroController
{
    private $memDAO;

    public function __construct()
    {
        $this->memDAO = new MembroDAO();
    }

    public function ObterMembrosAutocomplete($nome, $idIgreja)
    {
        if ($idIgreja > 0 && $nome != "") {
            return $this->memDAO->ObterMembrosId($nome, $idIgreja);
        } else {
            return null;
        }

    }

    public function ObterMembroPorCod($id)
    {
        if ($id > 0) {
            return $this->memDAO->ObterMembroPorCod($id);
        } else {
            return null;
        }
    }

    public function Gravar(Membro $membro)
    {
//        if(strlen($membro->getNome()) >= 3 && $membro->igreja->getId() > 0 && $membro->cidade->getNome() != ""){
        return $this->memDAO->Gravar($membro);
//        }else{
//            return false;
//        }
    }

    public function Editar(Membro $membro)
    {
//        if (strlen($membro->getNome()) >= 3 && strpos($membro->getEmail(), '@') && strpos($membro->getEmail(), '.') && $membro->igreja->getId() > 0
//            && $membro->getId() > 0) {
        return $this->memDAO->Editar($membro);
//        } else {
//            return false;
//        }
    }

    public function ObterMembros($idIgreja)
    {
        if ($idIgreja > 0) {
            return $this->memDAO->ObterMembros($idIgreja);
        } else {
            return null;
        }
    }

    public function Inativar($id)
    {
        if ($id > 0) {
            return $this->memDAO->Inativar($id);
        } else {
            return false;
        }
    }

    public function CountMembros($idIgreja)
    {
        if ($idIgreja > 0) {
            return $this->memDAO->CountMembros($idIgreja);
        } else {
            return null;
        }
    }

    public function RelatorioMembros($idIgreja, $sexo, $situacao)
    {
        $membros = $this->memDAO->RelatorioMembros($idIgreja, $sexo, $situacao);

        $html = '<style>
                    th, td {
                        text-align: left;
                    }
                    tr:nth-child(2n+2) {
                        background: #DCDCDC;
                    }
                    #footer {
                        font-size: 12px;
                    }
                    #header{
                
                    }
                    #borda{
                        border: 2px solid #000000;
                    }
                </style>';

        $html .= '<table bgcolor="#FFFFFF" align="center" border="0" cellpadding="10" cellspacing="0" width="100%" style="padding: 20px 40px">';
        $html .= '<tr>';
        $html .= '<td style="text-align: center; font-size: 20px"> RELATÓRIO DE MEMBROS</td>';
        $html .= '</tr>';
        $html .= '</table>';

        $html .= '<table bgcolor="#FFFFFF" align="center" border="0" cellpadding="10" cellspacing="0" width="100%" style="min-width: 600px; font-size: 12px; padding: 0 40px">
                    <tbody>
                    <tr>
                        <th>#CÓD </th>
                        <th>NOME</th>
                        <th>LOGRADOURO</th>
                        <th>NÚMERO</th>
                        <th>BAIRRO</th>
                        <th>TELEFONE</th>
                        <th>EMAIL</th>
                        <th>SEXO</th>
                        <th>SITUAÇÃO</th>
                    </tr>';
        foreach ($membros as $membro){
            $html .= '<tr>';
            $html .= '<td>'. $membro['idmembro'] .'</td>';
            $html .= '<td>'. $membro['nome'] .'</td>';
            $html .= '<td>'. $membro['logradouro'] .'</td>';
            $html .= '<td>'. $membro['numero'] .'</td>';
            $html .= '<td>'. $membro['bairro'] .'</td>';
            $html .= '<td>'. $membro['celular'] .'</td>';
            $html .= '<td>'. $membro['email'] .'</td>';
            if($membro['sexo'] == "M"){
                $html .= '<td>M</td>';
            }else{
                $html .= '<td>F</td>';
            }
            if($membro['ativo'] == 1){
                $html .= '<td>Ativo</td>';
            }else{
                $html .= '<td>Inativo</td>';
            }

            $html .= '</tr>';
        }

        $html .= '</tbody>';
        $html .= '</table>';

        return $html;
    }
}