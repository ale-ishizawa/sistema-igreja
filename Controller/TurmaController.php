<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:53
 */
namespace Controller;

use DAL\TurmaDAO;
use Model\Turma;

class TurmaController
{

    private $turDAO;

    /**
     * TurmaController constructor.
     */
    public function __construct()
    {
        $this->turDAO = new TurmaDAO();
    }

    public function Gravar(Turma $turma)
    {
        if($turma->membro->getId() > 0 && $turma->classe->getId() > 0){
            return $this->turDAO->Gravar($turma);
        }else{
            return false;
        }
    }

    public function Deletar($idMembro, $idClasse)
    {
        if($idMembro > 0 && $idClasse > 0){
            return $this->turDAO->Deletar($idMembro, $idClasse);
        }else{
            return false;
        }
    }

    public function VerificaCadastro(Turma $turma)
    {
        return $this->turDAO->VerificaCadastro($turma);
    }

    public function ObterMembrosTurma($idClasse)
    {
        return $this->turDAO->ObterMembrosTurma($idClasse);
    }

}