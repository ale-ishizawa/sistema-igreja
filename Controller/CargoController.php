<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:47
 */
namespace Controller;

use DAL\CargoDAO;
use Model\Cargo;

class CargoController
{
    private $cargoDAO;

    /**
     * CargoController constructor.
     */
    public function __construct()
    {
        $this->cargoDAO = new CargoDAO();
    }

    public function Gravar(Cargo $cargo)
    {
        if(strlen($cargo->getNome()) >= 3){
            return $this->cargoDAO->Gravar($cargo);
        }else{
            return false;
        }
    }

    public function Editar(Cargo $cargo)
    {
        if($cargo->getId() > 0 && strlen($cargo->getNome()) > 3){
            return $this->cargoDAO->Editar($cargo);
        }else{
            return false;
        }
    }

    public function ObterCargos()
    {
        return $this->cargoDAO->ObterCargos();
    }

    public function Deletar($id)
    {
        if($id > 0){
            return $this->cargoDAO->Deletar($id);
        }else{
            return false;
        }
    }

    public function ObterCargosSelect()
    {
        return $this->cargoDAO->ObterCargosSelect();
    }

    public function ObterCargoPorCod($id)
    {
        return $this->cargoDAO->ObterCargoPorCod($id);
    }
}