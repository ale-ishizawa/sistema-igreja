<?php
/**
 * Created by PhpStorm.
 * User: DEV02
 * Date: 23/04/2018
 * Time: 17:37
 */
namespace Controller;

use DAL\CaixaDAO;
use Model\Caixa;

class CaixaController
{
    public $cDAO;

    /**
     * CaixaController constructor.
     */
    public function __construct()
    {
        $this->cDAO = new CaixaDAO();
    }

    public function VerificaStatusCaixa($idIgreja)
    {
        return $this->cDAO->VerificaStatusCaixa($idIgreja);
    }

    public function ResumoCaixa($idIgreja)
    {
        return $this->cDAO->ResumoCaixa($idIgreja);
    }

    public function AbrirCaixa(Caixa $caixa)
    {
        if($caixa->getValorInicial() != "" && $caixa->getHoraAbertura() != null && $caixa->igreja->getId() > 0 &&
            $caixa->getDataCaixa() != null && $caixa->igreja->getId() >0 && $caixa->usuario->getId() > 0){
            return $this->cDAO->AbrirCaixa($caixa);
        }else{
            return false;
        }
    }

    public function FecharCaixa(Caixa $caixa)
    {
        if($caixa->getId() > 0 && $caixa->getStatus() == 2 && $caixa->getHoraFechamento() != ""){
            return $this->cDAO->FecharCaixa($caixa);
        }else{
            return false;
        }
    }

    public function ObterCaixas($idIgreja)
    {
        if($idIgreja > 0){
            return $this->cDAO->ObterCaixas($idIgreja);
        }else{
            return null;
        }
    }

    public function RelatorioCaixa($idIgreja, $inicio, $fim)
    {
        $caixas = $this->cDAO->RelatorioCaixa($idIgreja, $inicio, $fim);
        $total = 0;

        $html = '<style>
                    th, td {
                        text-align: left;
                    }
                    tr:nth-child(2n+2) {
                        background: #DCDCDC;
                    }
                    #footer {
                        font-size: 12px;
                    }
                    #header{
                
                    }
                    #borda{
                        border: 2px solid #000000;
                    }
                </style>';

        $html .= '<table bgcolor="#FFFFFF" align="center" border="0" cellpadding="10" cellspacing="0" width="100%" style="padding: 20px 40px">';
        $html .= '<tr>';
        $html .= '<td style="text-align: center; font-size: 16px"> RELATÓRIO DE CAIXA - Período: '.date('d/m/Y', strtotime($inicio)).' a '.date('d/m/Y', strtotime($fim)).'</td>';
        $html .= '</tr>';
        $html .= '</table>';

        $html .= '<table bgcolor="#FFFFFF" align="center" border="0" cellpadding="10" cellspacing="0" width="100%" style="min-width: 600px; font-size: 12px; padding: 0 40px">
                    <tbody>
                    <tr>
                        <th>DATA </th>
                        <th>HORA ABERTURA</th>
                        <th>HORA FECHAMENTO</th>        
                        <th>USUÁRIO</th>                
                        <th>VALOR INICIAL</th>
                        <th>VALOR FINAL</th>
                       
                    </tr>';
        if(count($caixas) > 0){
            foreach ($caixas as $caixa){
                $html .= '<tr>';
                $html .= '<td>'. date('d/m/Y', strtotime($caixa->getDataCaixa())) .'</td>';
                $html .= '<td>'. date('H:i', strtotime($caixa->getHoraAbertura())).'</td>';
                $html .= '<td>'. date('H:i', strtotime($caixa->getHoraFechamento())) .'</td>';
                $html .= '<td>'. $caixa->usuario->getNome() .'</td>';
                $html .= '<td>'. number_format($caixa->getValorInicial(), 2, ',', '.') .'</td>';
                $html .= '<td>'. number_format($caixa->getValorFinal(), 2, ',', '.') .'</td>';
                $html .= '</tr>';
                (float) $total += (float) $caixa->getValorFinal();
            }
        }else{
            $html .= '<tr>';
            $html .= '<td colspan="5" style="text-align: left; text-transform: uppercase"><b>Não há registros nesse período.</b></td>';
            $html .= '</tr>';
        }

        $html .= '<tr>';
        $html .= '<td colspan="5" style="text-align: right"><b>TOTAL FINAL DO CAIXA NO PERÍODO: '.number_format($total, 2, ',', '.').'</b></td>';
        $html .= '</tr>';

        $html .= '</tbody>';
        $html .= '</table>';

        return $html;
    }
}