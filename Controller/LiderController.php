<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:49
 */
namespace Controller;

use DAL\LiderDAO;
use Model\Lider;

class LiderController
{
    private $liderDAO;

    /**
     * LiderController constructor.
     */
    public function __construct()
    {
        $this->liderDAO = new LiderDAO();
    }

    public function Gravar(Lider $lider)
    {
        if($lider->getId() > 0){
            return $this->liderDAO->Gravar($lider);
        }else{
            return false;
        }
    }

    public function Deletar($idMembro)
    {
        if($idMembro > 0){
            return $this->liderDAO->Deletar($idMembro);
        }else{
            return false;
        }
    }

    public function ObterLideres($idIgreja)
    {
        return $this->liderDAO->ObterLideres($idIgreja);
    }

    public function VerificaCadastro(Lider $lider)
    {
        return $this->liderDAO->VerificaCadastro($lider);
    }

}