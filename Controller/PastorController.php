<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:52
 */
namespace Controller;

use DAL\PastorDAO;
use Model\Pastor;

class PastorController
{
    private $pastorDAO;

    /**
     * PastorController constructor.
     */
    public function __construct()
    {
        $this->pastorDAO = new PastorDAO();
    }


    public function Gravar(Pastor $pastor)
    {
        if($pastor->getId() > 0){
            return $this->pastorDAO->Gravar($pastor);
        }else{
            return false;
        }
    }

    public function Deletar($idMembro)
    {
        if($idMembro > 0){
            return $this->pastorDAO->Deletar($idMembro);
        }else{
            return false;
        }
    }

    public function ObterPastores($idIgreja)
    {
        return $this->pastorDAO->ObterPastores($idIgreja);
    }

    public function VerificaCadastro(Pastor $pastor)
    {
        return $this->pastorDAO->VerificaCadastro($pastor);
    }
}