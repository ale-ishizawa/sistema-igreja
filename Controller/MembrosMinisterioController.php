<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:51
 */
namespace Controller;

use DAL\MembrosMinisterioDAO;
use Model\MembrosMinisterio;

class MembrosMinisterioController
{
    private $mmDAO;

    /**
     * MembrosMinisterioController constructor.
     */
    public function __construct()
    {
        $this->mmDAO = new MembrosMinisterioDAO();
    }

    public function Gravar(MembrosMinisterio $membro)
    {
        if($membro->membro->getId() > 0 && $membro->ministerio->getId() > 0){
            return $this->mmDAO->Gravar($membro);
        }else{
            return false;
        }
    }

    public function Deletar($idMembro, $idMinisterio)
    {
        if($idMembro > 0 && $idMinisterio > 0){
            return $this->mmDAO->Deletar($idMembro, $idMinisterio);
        }else{
            return false;
        }
    }

    public function VerificaCadastro(MembrosMinisterio $memMinisterio)
    {
        return $this->mmDAO->VerificaCadastro($memMinisterio);
    }

    public function ObterMembrosMinisterio($idMinisterio)
    {
        return $this->mmDAO->ObterMembrosMinisterio($idMinisterio);
    }
}