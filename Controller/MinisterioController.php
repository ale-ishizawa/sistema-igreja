<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:51
 */

namespace Controller;

use Model\Ministerio;
use DAL\MinisterioDAO;

class MinisterioController
{
    private $minDAO;

    /**
     * MinisterioController constructor.
     */
    public function __construct()
    {
        $this->minDAO = new MinisterioDAO();
    }

    public function Gravar(Ministerio $ministerio)
    {
        if (strlen($ministerio->getNome()) >= 5) {
            return $this->minDAO->Gravar($ministerio);
        } else {
            return false;
        }
    }

    public function Deletar($id)
    {
        if ($id > 0) {
            return $this->minDAO->Deletar($id);
        } else {
            return false;
        }
    }

    public function Editar(Ministerio $ministerio)
    {
        if ($ministerio->getId() > 0 && strlen($ministerio->getNome()) >= 5) {
            return $this->minDAO->Editar($ministerio);
        } else {
            return false;
        }
    }

    public function ObterMinisterios($idIgreja)
    {
        if($idIgreja > 0){
            return $this->minDAO->ObterMinisterios($idIgreja);
        }else{
            return null;
        }
    }

    public function ObterMinisterioPorCod($id)
    {
        if($id > 0){
            return $this->minDAO->ObterMinisterioPorCod($id);
        }else{
            return false;
        }
    }

}