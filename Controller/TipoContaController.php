<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:52
 */
namespace Controller;

use DAL\TipoContaDAO;
use Model\TipoConta;

class TipoContaController
{
    private $tipoDAO;

    /**
     * TipoContaController constructor.
     */
    public function __construct()
    {
        $this->tipoDAO = new TipoContaDAO();
    }

    public function Gravar(TipoConta $tipo)
    {
        if(strlen($tipo->getDescricao()) >= 3){
            return $this->tipoDAO->Gravar($tipo);
        }else{
            return false;
        }
    }

    public function ObterTiposConta()
    {
        return $this->tipoDAO->ObterTiposConta();
    }

    public function Deletar($id)
    {
        if($id > 0){
            return $this->tipoDAO->Deletar($id);
        }else{
            return false;
        }
    }

    public function Editar(TipoConta $tipoConta)
    {
        if(strlen($tipoConta->getDescricao()) >= 3){
            return $this->tipoDAO->Editar($tipoConta);
        }else{
            return false;
        }
    }

    public function ObterTipoContaPorCod($id)
    {
        return $this->tipoDAO->ObterTipoContaPorCod($id);
    }
}