<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:48
 */
namespace Controller;

use DAL\ContaPagarDAO;
use Model\ContaPagar;

class ContaPagarController
{
    private $contaDAO;

    /**
     * ContaPagarController constructor.
     */
    public function __construct()
    {
        $this->contaDAO = new ContaPagarDAO();
    }

    public function Gravar(ContaPagar $contaPagar)
    {
        if($contaPagar->getValor() != "" && $contaPagar->getDataVencimento() != "" && $contaPagar->igreja->getId() > 0 && $contaPagar->tipoConta->getId() > 0 ){
            return $this->contaDAO->Gravar($contaPagar);
        }else{
            return false;
        }
    }

    public function Editar(ContaPagar $conta)
    {
        if($conta->getId() > 0 && $conta->tipoConta->getId() > 0 && $conta->getDataVencimento() != "" && $conta->getValor() != ""){
            return $this->contaDAO->Editar($conta);
        }else{
            return false;
        }
    }

    public function Deletar($id)
    {
        if($id > 0){
            return $this->contaDAO->Deletar($id);
        }else{
            return false;
        }
    }

    public function ObterContasPagar($idIgreja)
    {
        if($idIgreja > 0){
            return $this->contaDAO->ObterContasPagar($idIgreja);
        }else{
            return null;
        }
    }

    public function BaixarConta($idConta, $idCaixa)
    {
        if($idConta > 0 && $idCaixa > 0){
            return $this->contaDAO->BaixarConta($idConta, $idCaixa);
        }else{
            return false;
        }
    }

    public function ObterContaPorCod($id)
    {
        if($id > 0){
            return $this->contaDAO->ObterContaPorCod($id);
        }else{
            return null;
        }
    }

}