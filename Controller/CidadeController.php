<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:48
 */
namespace Controller;

use DAL\CidadeDAO;
use Model\Cidade;

class CidadeController
{
    private $cidDAO;

    /**
     * CidadeController constructor.
     */
    public function __construct()
    {
        $this->cidDAO = new CidadeDAO();
    }

    public function VerificaExisteCidade(Cidade $cidade)
    {
        if($cidade->getNome() != "" && $cidade->getUf() != ""){
            return $this->cidDAO->VerificaExisteCidade($cidade);
        }else{
            return -10;
        }
    }

}