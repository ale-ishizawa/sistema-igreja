<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:49
 */
namespace Controller;

use DAL\DizimoDAO;
use Model\Dizimo;

class DizimoController
{
    private $dizDAO;

    /**
     * DizimoController constructor.
     */
    public function __construct()
    {
        $this->dizDAO = new DizimoDAO();
    }

    public function Gravar(Dizimo $dizimo, $idCaixa)
    {
        if($dizimo->getValor() != "" && $dizimo->membro->getId() > 0){
            return $this->dizDAO->Gravar($dizimo, $idCaixa);
        }else{
            return false;
        }
    }

    public function ObterDizimos($idIgreja)
    {
        if($idIgreja > 0){
            return $this->dizDAO->ObterDizimos($idIgreja);
        }else{
            return null;
        }
    }

    public function RelatorioDizimos($idIgreja, $inicio, $fim)
    {
        $dizimos = $this->dizDAO->RelatorioDizimos($idIgreja, $inicio, $fim);
        $total = 0;

        $html = '<style>
                    th, td {
                        text-align: left;
                    }
                    tr:nth-child(2n+2) {
                        background: #DCDCDC;
                    }
                    #footer {
                        font-size: 12px;
                    }
                    #header{
                
                    }
                    #borda{
                        border: 2px solid #000000;
                    }
                </style>';

        $html .= '<table bgcolor="#FFFFFF" align="center" border="0" cellpadding="10" cellspacing="0" width="100%" style="padding: 20px 40px">';
        $html .= '<tr>';
        $html .= '<td style="text-align: center; font-size: 16px"> RELATÓRIO DE DÍZIMOS - Período: '.date('d/m/Y', strtotime($inicio)).' a '.date('d/m/Y', strtotime($fim)).'</td>';
        $html .= '</tr>';
        $html .= '</table>';

        $html .= '<table bgcolor="#FFFFFF" align="center" border="0" cellpadding="10" cellspacing="0" width="100%" style="min-width: 600px; font-size: 12px; padding: 0 40px">
                    <tbody>
                    <tr>
                        <th>#CÓD </th>
                        <th>MEMBRO</th>
                        <th>DATA LANÇAMENTO</th>                        
                        <th>VALOR</th>
                    </tr>';
        if(count($dizimos) > 0){
            foreach ($dizimos as $dizimo){
                $html .= '<tr>';
                $html .= '<td>'. $dizimo->getId() .'</td>';
                $html .= '<td>'. $dizimo->membro->getNome() .'</td>';
                $html .= '<td>'. date('d/m/Y', strtotime($dizimo->getData())) .'</td>';
                $html .= '<td>'. number_format($dizimo->getValor(), 2, ',', '.') .'</td>';
                $html .= '</tr>';
                (float) $total += (float) $dizimo->getValor();
            }
        }else{
            $html .= '<tr>';
            $html .= '<td colspan="5" style="text-align: left; text-transform: uppercase"><b>Não há dízimos registrados nesse período.</b></td>';
            $html .= '</tr>';
        }

        $html .= '<tr>';
        $html .= '<td colspan="5" style="text-align: right"><b>TOTAL: '.number_format($total, 2, ',', '.').'</b></td>';
        $html .= '</tr>';

        $html .= '</tbody>';
        $html .= '</table>';

        return $html;
    }

}