<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 26/04/2018
 * Time: 00:15
 */
session_start();
session_destroy();

header("Location: index.php");