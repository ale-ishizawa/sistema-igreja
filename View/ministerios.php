<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 28/05/2018
 * Time: 13:12
 */
session_start();
require_once './_autoload.php';
include "View/Template/header.php";
include "View/Template/menu.php";

use Controller\MinisterioController;
use Controller\LiderController;
use Model\Ministerio;

$ministerio = new Ministerio();
$ministerioController = new MinisterioController();
$liderController = new LiderController();
$idIgrejaSessao = isset($_SESSION['idigreja']) ? $_SESSION['idigreja'] : null;

$resultado = "";
$erros = [];
$codigo = "";
$alterando = false;

//UPDATE
$codEditar = filter_input(INPUT_GET, 'editar');
if ($codEditar != NULL) {
    $m = $ministerioController->ObterMinisterioPorCod($codEditar);
    if ($m != NULL) {
        $codigo = $codEditar;
        $ministerio->setId($m->getId());
        $ministerio->setDescricao($m->getDescricao());
        $ministerio->setNome($m->getNome());
        $ministerio->lider->setId($m->lider->getId());
        $alterando = true;
    }
}

//DELETE
$codExcluir = filter_input(INPUT_GET, 'del');
if ($codExcluir != NULL) {
    if ($ministerioController->Deletar($codExcluir)) {
        $resultado = '<di v class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro excluído com sucesso.
                        </div>';
    } else {
        $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao excluir registro.
                        </div>';
    }
}

$btnSalvar = filter_input(INPUT_POST, 'btnSalvar') != NULL;
$btnEditar = filter_input(INPUT_POST, 'btnEditar') != NULL;

if ($btnSalvar || $btnEditar) {
    $erros = Validar();

    //Se não existir erros de validação, envia os dados para a Controladora
    if (empty($erros)) {
        if ($btnEditar) {
            $codigo = filter_input(INPUT_POST, "txtCodigo", FILTER_VALIDATE_INT);
            if ($codigo === false) {
                $erros[] = "- Código não informado corretamente.";
            }
            if (empty($erros)) {
                //EDIÇÃO
                $ministerio->setId($codigo);
                $ministerio->setNome(filter_input(INPUT_POST, 'txtNome'));
                $ministerio->setDescricao(filter_input(INPUT_POST, 'txtDescricao'));
                $ministerio->lider->setId(filter_input(INPUT_POST, 'selLider'));

                if ($ministerioController->Editar($ministerio)) {
                    $ministerio = new Ministerio();
                    $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro salvo com sucesso.
                        </div>';
                } else {
                    $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao salvar registro.
                        </div>';
                }
            }
        } else {
            //CADASTRO
            $ministerio->setNome(filter_input(INPUT_POST, 'txtNome'));
            $ministerio->setDescricao(filter_input(INPUT_POST, 'txtDescricao'));
            $ministerio->lider->setId(filter_input(INPUT_POST, 'selLider'));
            $ministerio->igreja->setId($idIgrejaSessao);

            if ($ministerioController->Gravar($ministerio)) {
                $ministerio = new Ministerio();
                $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro salvo com sucesso.
                        </div>';
            } else {
                $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao salvar registro.
                        </div>';
            }
        }
    }
}

function Validar()
{
    $listaErros = [];

    if (strlen(filter_input(INPUT_POST, 'txtNome', FILTER_SANITIZE_STRING)) < 3) {
        $listaErros[] = "- Nome do Ministério inválido. (mínimo 3 caracteres)";
    }

    return $listaErros;
}

?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">Ministérios</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Início</a></li>
                    <li class="breadcrumb-item active">Ministérios</li>
                </ol>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10">
                    <i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

        <!-- Row -->
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">Cadastrar Ministério</h4>
                        <form method="post" action="?page=ministerios">
                            <input type="hidden" name="txtCodigo" id="txtCodigo" value="<?= $codigo ?>"/>
                            <div class="form-group">
                                <label class="control-label">Nome</label>
                                <input type="text" id="txtNome" name="txtNome" class="form-control"
                                       placeholder="Nome do Ministério" required value="<?= $ministerio->getNome() ?>">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Descrição</label>
                                <textarea type="text" id="txtDescricao" name="txtDescricao" class="form-control"
                                          placeholder="Descrição do Ministério" required
                                          rows="3"><?= $ministerio->getDescricao() ?> </textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Líder do Ministério</label>
                                <select class="form-control" id="selLider" name="selLider" required>
                                    <option value="0">Selecione o Líder...</option>
                                    <?php $listaLideres = $liderController->ObterLideres($idIgrejaSessao);
                                    foreach ($listaLideres as $lider) { ?>
                                        <option value="<?= $lider->getId() ?>" <?php echo $ministerio->lider->getId() == $lider->getId() ? 'selected' : '' ?>>
                                            <?= $lider->getNome() ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-actions">
                                <input type="submit" class="btn btn-success"
                                       style="margin-top: 32px; <?= $alterando ? 'display:none' : "" ?>"
                                       name="btnSalvar" id="btnSalvar" value="SALVAR"/>
                                <input type="submit" class="btn btn-success"
                                       style="margin-top: 32px; <?= $alterando ? "" : 'display:none' ?>"
                                       name="btnEditar" id="btnEditar" value="ALTERAR"/>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <br/>
                                    <?php echo $resultado ?>
                                </div>
                            </div>
                            <?php if (!empty($erros)) { ?>
                                <div class="form-group">
                                    <div class="col-xs-12" style="border: solid 2px red">
                                        <ul style="list-style: none;">
                                            <?php
                                            foreach ($erros as $e) {
                                                ?>
                                                <li><?= $e; ?></li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">Ministérios da Igreja</h4>
                        <div class="table-responsive m-t-40" style="min-height: 350px">
                            <?php $ministerios = $ministerioController->ObterMinisterios($idIgrejaSessao);
                            if (count($ministerios) > 0) { ?>
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Ações</th>
                                        <th>Nome</th>
                                        <th>Descrição</th>
                                        <th>Líder</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($ministerios as $min) { ?>
                                        <tr>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info dropdown-toggle"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        <i class="ti-settings"></i>
                                                    </button>
                                                    <div class="dropdown-menu animated flipInX">
                                                        <a class="dropdown-item"
                                                           href="?page=ministerios&editar=<?= $min->getId() ?>">Editar</a>
                                                        <a class="dropdown-item"
                                                           href="?page=ministerios&del=<?= $min->getId() ?>"
                                                           onclick="return confirm('Deseja excluir o ministério <?= $min->getNome() ?>?');">Excluir
                                                            Ministério</a>
                                                        <a class="dropdown-item"
                                                           href="?page=membros-ministerio&ver=<?= $min->getId() ?>">Visualizar
                                                            Membros</a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><?= $min->getNome() ?></td>
                                            <td><?= $min->getDescricao() ?></td>
                                            <td><?= $min->lider->getNome() ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            <?php } else { ?>
                                <div class="alert alert-warning">Não há ministérios cadastrados no sistema.</div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <?php include "View/Template/service-panel.php"; ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

    <?php include "View/Template/footer.php"; ?>

