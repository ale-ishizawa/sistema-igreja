<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 21/05/2018
 * Time: 00:30
 */

require_once './_autoload.php';
include "View/Template/header.php";
include "View/Template/menu.php";

use Controller\IgrejaController;
use Controller\CampanhaController;
use Model\Campanha;
use Model\Igreja;

session_start();

$idIgrejaSessao = isset($_SESSION['idigreja']) ? $_SESSION['idigreja'] : "";

$igrejaController = new IgrejaController();
$campanhaController = new CampanhaController();

$resultado = "";
$erros = [];

//DELETE
$codExcluir = filter_input(INPUT_GET, 'del');
if($codExcluir != NULL){
    if($campanhaController->Deletar($codExcluir)){
        $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro excluído com sucesso.
                        </div>';
    } else {
        $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao excluir registro.
                        </div>';
    }
}

?>

<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">Campanhas</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Início</a></li>
                    <li class="breadcrumb-item active">Campanhas</li>
                </ol>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10">
                    <i class="ti-settings text-white"></i></button>
                <button class="btn pull-right hidden-sm-down btn-success"><i class="mdi mdi-plus-circle"></i> Create
                </button>
                <div class="dropdown pull-right m-r-10 hidden-sm-down">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> January 2017
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"><a class="dropdown-item" href="#">February
                            2017</a> <a class="dropdown-item" href="#">March 2017</a> <a class="dropdown-item" href="#">April
                            2017</a></div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

        <!-- Inicio Listagem -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <form method="post" action="?page=campanhas"
                    <form class="card-block">
                        <h4 class="card-title">Campanhas da Igreja</h4>
                        <div class="table-responsive m-t-40" style="min-height: 300px">
                            <?php $campanhas = $campanhaController->ObterCampanhas($idIgrejaSessao);
                            if (count($campanhas) > 0) { ?>
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Ações</th>
                                        <th>Nome</th>
                                        <th>Descrição</th>
                                        <th>Date de Início</th>
                                        <th>Data de Encerramento</th>
                                        <th>Valor Alvo</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($campanhas as $cam){ ?>
                                        <tr>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-danger dropdown-toggle"
                                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="ti-settings"></i>
                                                    </button>
                                                    <div class="dropdown-menu animated flipInX">
                                                        <a class="dropdown-item"
                                                           href="?page=cadastro-campanha&editar=<?= $cam->getId() ?>">Editar</a>
                                                        <a class="dropdown-item" href="?page=campanhas&del=<?= $cam->getId() ?>"
                                                           onclick="return confirm('Deseja excluir a campanha <?= $cam->getNome() ?>?');">Excluir</a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><?= $cam->getNome() ?></td>
                                            <td><?= $cam->getDescricao() ?></td>
                                            <td><?= date("d/m/Y", strtotime($cam->getDataInicio())) ?></td>
                                            <td><?= date("d/m/Y", strtotime($cam->getDataFim())) ?></td>
                                            <td><?= number_format($cam->getValorAlvo(), 2, ',', '.') ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            <?php } else { ?>
                                <div class="alert alert-warning">Não há campanhas cadastradas no sistema.</div>
                            <?php } ?>
                        </div>
                        <br/>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <?php echo $resultado ?>
                            </div>
                        </div>
                        <?php if (!empty($erros)) { ?>
                            <div class="form-group">
                                <div class="col-xs-12" style="border: solid 2px red">
                                    <ul style="list-style: none;">
                                        <?php
                                        foreach ($erros as $e) {
                                            ?>
                                            <li><?= $e; ?></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>
                    </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fim Listagem -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
         <?php include "View/Template/service-panel.php"; ?>

    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <?php include "View/Template/footer.php"; ?>
