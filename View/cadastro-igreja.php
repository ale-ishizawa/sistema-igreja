<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 07/05/2018
 * Time: 01:25
 */
require_once './_autoload.php';
include "View/Template/header.php";
include "View/Template/menu.php";

use Model\Igreja;
use Controller\IgrejaController;

$igreja = new Igreja();
$resultado = "";
$erros = [];
$alterando = false;
$codigo = "";

$igrejaController = new IgrejaController();


//Editar
$codEditar = filter_input(INPUT_GET, 'editar');
if ($codEditar != NULL) {
    $i = $igrejaController->ObterIgreja($codEditar);
    if ($i != NULL) {
        $codigo = $codEditar;
        $igreja->setId($i->getId());
        $igreja->setNome($i->getNome());
        $igreja->setCnpj($i->getCnpj());
        $igreja->setEmail($i->getEmail());
        $igreja->setBairro($i->getBairro());
        $igreja->setComplemento($i->getComplemento());
        $igreja->setNumero($i->getNumero());
        $igreja->setLogradouro($i->getLogradouro());
        $igreja->setCep($i->getCep());
        $igreja->setIe($i->getIe());
        $igreja->setSede($i->getSede());
        $igreja->setSite($i->getSite());
        $igreja->setTelefone($i->getTelefone());
        $igreja->cidade->setUf($i->cidade->getUf());
        $igreja->cidade->setNome($i->cidade->getNome());
        $alterando = true;
    }
}

$btnSalvar = filter_input(INPUT_POST, 'btnSalvar') != NULL;
$btnEditar = filter_input(INPUT_POST, 'btnEditar') != NULL;

if ($btnSalvar || $btnEditar) {
    $erros = Validar();

    if (empty($erros)) {
        if($btnEditar){
            //EDIÇÃO
            $codigo = filter_input(INPUT_POST, "txtCodigo", FILTER_VALIDATE_INT);
            if ($codigo === false) {
                $erros[] = "- Código não informado corretamente.";
            }
            if (empty($erros)) {
                //EDIÇÃO DO CADASTRO VALIDADA, REALIZA O UPDATE
                $igreja->setId($codigo);
                $igreja->setNome(filter_input(INPUT_POST, "txtNome", FILTER_SANITIZE_STRING));
                $igreja->setCnpj(filter_input(INPUT_POST, "txtCnpj", FILTER_SANITIZE_STRING));
                $igreja->setEmail(filter_input(INPUT_POST, "txtEmail", FILTER_SANITIZE_STRING));
                $igreja->setBairro(filter_input(INPUT_POST, "txtBairro", FILTER_SANITIZE_STRING));
                $igreja->setComplemento(filter_input(INPUT_POST, "txtComplemento", FILTER_SANITIZE_STRING));
                $igreja->setNumero(filter_input(INPUT_POST, "txtNumero", FILTER_SANITIZE_STRING));
                $igreja->setLogradouro(filter_input(INPUT_POST, "txtLogradouro", FILTER_SANITIZE_STRING));
                $igreja->setCep(filter_input(INPUT_POST, "txtCep", FILTER_SANITIZE_STRING));
                $igreja->setIe(filter_input(INPUT_POST, "txtIe", FILTER_SANITIZE_STRING));
                $igreja->setSite(filter_input(INPUT_POST, "txtSite", FILTER_SANITIZE_STRING));
                $igreja->setTelefone(filter_input(INPUT_POST, "txtTelefone", FILTER_SANITIZE_STRING));
                $igreja->setSede(filter_input(INPUT_POST, 'ckbSede') == 'on' ? 1 : 0);

                //Obtendo valores da requisição VIA CEP
                $nomeCidade = isset($_POST['txtCidade']) ? $_POST['txtCidade'] : "";
                $Uf = isset($_POST['txtUf']) ? $_POST['txtUf'] : "";
                $igreja->cidade->setNome($nomeCidade);
                $igreja->cidade->setUf($Uf);

                if ($igrejaController->Editar($igreja)) {
                    $igreja = new Igreja();
                    $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                                     </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro salvo com sucesso.
                                 </div>';
                } else {
                    $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                                     </button> <h3 class="text-success"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao salvar registro.
                                 </div>';
                }
            }
        } else {
            //Inserção de Novo Registro
            $igreja->setNome(filter_input(INPUT_POST, "txtNome", FILTER_SANITIZE_STRING));
            $igreja->setCnpj(filter_input(INPUT_POST, "txtCnpj", FILTER_SANITIZE_STRING));
            $igreja->setEmail(filter_input(INPUT_POST, "txtEmail", FILTER_SANITIZE_STRING));
            $igreja->setBairro(filter_input(INPUT_POST, "txtBairro", FILTER_SANITIZE_STRING));
            $igreja->setComplemento(filter_input(INPUT_POST, "txtComplemento", FILTER_SANITIZE_STRING));
            $igreja->setNumero(filter_input(INPUT_POST, "txtNumero", FILTER_SANITIZE_STRING));
            $igreja->setLogradouro(filter_input(INPUT_POST, "txtLogradouro", FILTER_SANITIZE_STRING));
            $igreja->setCep(filter_input(INPUT_POST, "txtCep", FILTER_SANITIZE_STRING));
            $igreja->setIe(filter_input(INPUT_POST, "txtIe", FILTER_SANITIZE_STRING));
            $igreja->setSite(filter_input(INPUT_POST, "txtSite", FILTER_SANITIZE_STRING));
            $igreja->setTelefone(filter_input(INPUT_POST, "txtTelefone", FILTER_SANITIZE_STRING));
            $igreja->setSede(filter_input(INPUT_POST, 'ckbSede') == 'on' ? 1 : 0);
            $igreja->setAtivo(1);

            if ($igrejaController->Gravar($igreja)) {
                $igreja = new Igreja();
                $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                                     </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro salvo com sucesso.
                                 </div>';
            } else {
                $resultado = '<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                                     </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao salvar registro.
                                 </div>';
            }
        }
    }
}

function Validar()
{
    $listaErros = [];

    //Validações...
    if (strlen(filter_input(INPUT_POST, "txtNome", FILTER_SANITIZE_STRING)) < 3) {
        $listaErros[] = "- Nome inválido. (min 3 caracteres)";
    }
    if (strlen(filter_input(INPUT_POST, "txtBairro", FILTER_SANITIZE_STRING)) < 3) {
        $listaErros[] = "- Bairro inválido. (min 3 caracteres)";
    }
    if (strlen(filter_input(INPUT_POST, "txtLogradouro", FILTER_SANITIZE_STRING)) < 3) {
        $listaErros[] = "- Logradouro inválido. (min 3 caracteres)";
    }
    if (strlen(filter_input(INPUT_POST, "txtSite", FILTER_SANITIZE_STRING)) < 3) {
        $listaErros[] = "- Site inválido. (min 10 caracteres)";
    }

    return $listaErros;
}

?>

<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">Igrejas</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Início</a></li>
                    <li class="breadcrumb-item active">Igrejas</li>
                </ol>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10">
                    <i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-outline-info">
                    <div class="card-header">
                        <h4 class="m-b-0 text-white">Cadastro de Igreja</h4>
                    </div>
                    <div class="card-block">
                        <form action="?page=cadastro-igreja" method="post">
                            <div class="form-body">
                                <input type="hidden" name="txtCodigo" id="txtCodigo" value="<?= $codigo ?>"/>
                                <h3 class="card-title">Dados Jurídicos</h3>
                                <hr>
                                <div class="row p-t-20">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Nome</label>
                                            <input type="text" id="txtNome" name="txtNome" class="form-control"
                                                   placeholder="Nome" required value="<?= $igreja->getNome() ?>" >
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">CNPJ</label>
                                            <input type="text" id="txtCnpj" name="txtCnpj" class="form-control"
                                                   placeholder="CNPJ" required value="<?= $igreja->getCnpj() ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Inscrição Estadual</label>
                                            <input type="text" id="txtIe" name="txtIe" class="form-control"
                                                   placeholder="IE" value="<?= $igreja->getIe() ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Email</label>
                                            <input type="email" class="form-control" placeholder="Email" name="txtEmail"
                                                   id="txtEmail"/ required value="<?= $igreja->getEmail() ?>" >
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Telefone</label>
                                            <input type="text" name="txtTelefone" id="txtTelefone" class="form-control"
                                                   placeholder="Telefone" value="<?= $igreja->getTelefone() ?>" data-mask="(99) 9999-9999" >
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="control-label">WebSite</label>
                                            <input type="text" name="txtSite" id="txtSite" class="form-control"
                                                   placeholder="Site" value="<?= $igreja->getSite() ?>">
                                        </div>
                                    </div>
                                </div>
                                <h3 class="box-title m-t-40">Endereço</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>CEP</label>
                                            <input type="text" class="form-control" data-mask="99999-999" name="txtCep" id="txtCep" required value="<?= $igreja->getCep() ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>Av/Rua</label>
                                            <input type="text" class="form-control" name="txtLogradouro" id="txtLogradouro" value="<?= $igreja->getLogradouro() ?>" required>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Número</label>
                                            <input type="text" class="form-control" name="txtNumero" id="txtNumero" required value="<?= $igreja->getNumero() ?>" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Complemento</label>
                                            <input type="text" class="form-control" name="txtComplemento"
                                                   id="txtComplemento" value="<?= $igreja->getComplemento() ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Bairro</label>
                                            <input type="text" class="form-control" name="txtBairro" id="txtBairro" required value="<?= $igreja->getBairro() ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Cidade</label>
                                            <input type="text" class="form-control" name="txtCidade" readonly id="txtCidade" value="<?= $igreja->cidade->getNome() ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>UF</label>
                                            <input type="text" class="form-control" name="txtUf" readonly id="txtUf" value="<?= $igreja->cidade->getUf() ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="checkbox checkbox-success">
                                            <input id="ckbSede" type="checkbox" <?= $igreja->getSede() == 1 ? 'checked' : ''?> >
                                            <label for="ckbSede"> Igreja Sede ? </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="form-actions">
                                <input type="submit" class="btn btn-success"
                                       style="margin-top: 32px; <?= $alterando ? 'display:none' : "" ?>"
                                       name="btnSalvar" id="btnSalvar" value="SALVAR"/>
                                <input type="submit" class="btn btn-success"
                                       style="margin-top: 32px; <?= $alterando ? "" : 'display:none' ?>"
                                       name="btnEditar" id="btnEditar" value="ALTERAR"/>
                            </div>
                            <br/>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <?php echo $resultado ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <?php if(count($erros) > 0){ ?>
                                    <ul style="list-style: none; border: 2px solid red">
                                        <?php
                                        foreach ($erros as $e) {
                                            ?>
                                            <li><?= $e; ?></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                    <?php } ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <?php include "View/Template/service-panel.php"; ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <?php include "View/Template/footer.php"; ?>
    <script type="text/javascript" src="View/scripts/via-cep.js"></script>
    <script type="text/javascript" src="js/mask.js"></script>