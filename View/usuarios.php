<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 20/05/2018
 * Time: 19:47
 */
require_once './_autoload.php';
include "View/Template/header.php";
include "View/Template/menu.php";

use Controller\IgrejaController;
use Controller\UsuarioController;
use Model\Usuario;
use Model\Igreja;

$igrejaController = new IgrejaController();
$usuarioController = new UsuarioController();

$resultado = "";
$erros = [];
$alterando = false;


//CADASTRAR OU EDITAR
$btnSalvar = filter_input(INPUT_POST, 'btnSalvar') != NULL;
$btnEditar = filter_input(INPUT_POST, 'btnEditar') != NULL;

if ($btnSalvar || $btnAlterar) {
    $erros = Validar();

    //Se a validação estiver OK
    if (empty($erros)) {
        if ($btnEditar) {
            //Edição de Registro já cadastrado
            $codigo = filter_input(INPUT_POST, "txtCodigo", FILTER_VALIDATE_INT);
            if ($codigo === false) {
                $erros->add("- Código não informado corretamente.");
            }
            if (!$erros->possuiErros()) {

            }

        } else {
            //Inserção de Novo Registro
            $usuario = new Usuario();

            $senha = filter_input(INPUT_POST, "txtSenha", FILTER_SANITIZE_STRING);
            $senha = password_hash($senha, PASSWORD_DEFAULT);

            $usuario->setAtivo(1);
            $usuario->setEmail(filter_input(INPUT_POST, "txtEmail", FILTER_SANITIZE_STRING));
            $usuario->setNome(filter_input(INPUT_POST, "txtNome", FILTER_SANITIZE_STRING));
            $usuario->setPerfil(filter_input(INPUT_POST, "selPerfil"));
            $usuario->setSenha($senha);
            $usuario->setUsuario(filter_input(INPUT_POST, "txtUsuario", FILTER_SANITIZE_STRING));

            if ($usuarioController->Gravar($usuario)) {
                $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro salvo com sucesso.
                        </div>';
            } else {
                $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao salvar registro.
                        </div>';
            }
        }

    }
}

function Validar()
{
    $listaErros = [];
    $uController = new UsuarioController();

    if (strlen(filter_input(INPUT_POST, 'txtUsuario', FILTER_SANITIZE_STRING)) < 3) {
        $listaErros[] = "- Usuário inválido. (mínimo 3 caracteres)";
    }
    if (strlen(filter_input(INPUT_POST, 'txtNome', FILTER_SANITIZE_STRING)) < 3) {
        $listaErros[] = "- Nome inválido. (mínimo 3 caracteres)";
    }
    if (strlen(filter_input(INPUT_POST, 'txtSenha', FILTER_SANITIZE_STRING)) < 5) {
        $listaErros[] = "- Senha inválida. (mínimo 5 caracteres)";
    }
    if (filter_input(INPUT_POST, "txtEmail", FILTER_SANITIZE_EMAIL)) {
        if ($uController->VerificaEmailExiste(filter_input(INPUT_POST, "txtEmail", FILTER_SANITIZE_STRING)) == 1) {
            $listaErros[] = "- E-mail já cadastrado.";
        }
    } else {
        $listaErros[] = "- E-mail inválido.";
    }

    return $listaErros;
}


?>

<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">Usuários</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Início</a></li>
                    <li class="breadcrumb-item active">Usuários</li>
                </ol>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10">
                    <i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-outline-info">
                    <div class="card-block">
                        <h4 class="card-title">Cadastrar Usuário</h4>
                        <form method="post" action="?page=usuarios">
                            <div class="form-body">
                                <div class="row p-t-20">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Nome</label>
                                            <input type="text" id="txtNome" name="txtNome" class="form-control"
                                                   placeholder="Nome" required maxlength="50">
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Email</label>
                                            <input type="text" id="txtEmail" name="txtEmail"
                                                   class="form-control form-control-danger" placeholder="Email"
                                                   required maxlength="60">
                                        </div>
                                        <!--/span-->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Usuário</label>
                                            <input type="text" id="txtUsuario" name="txtUsuario" class="form-control"
                                                   placeholder="Usuário" required maxlength="20">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Senha</label>
                                            <input type="password" id="txtSenha" name="txtSenha" class="form-control"
                                                   placeholder="Senha" required maxlength="30">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Confirmar Senha</label>
                                            <input type="password" id="txtConfirmarSenha" name="txtConfirmarSenha"
                                                   class="form-control" placeholder="Confirmar Senha" required maxlength="30">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Perfil</label>
                                            <select class="form-control" id="selPerfil" name="selPerfil">
                                                <option value="0">Selecione o Perfil...</option>
                                                <option value="1">Administrador</option>
                                                <option value="2">Pastor</option>
                                                <option value="3">Presidente/Vice</option>
                                                <option value="4">Secretário (a)</option>
                                                <option value="5">Tesoureiro (a)</option>
                                                <option value="6">Membro</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Igreja</label>
                                            <select class="form-control" id="selIgreja" name="selIgreja">
                                                <option value="0">Selecione a Igreja...</option>
                                                <?php $listaIgrejas = $igrejaController->ObterIgrejasSelect();
                                                foreach ($listaIgrejas as $igreja) { ?>
                                                    <option value="<?= $igreja->getId() ?>"><?= $igreja->getNome() ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="form-actions">
                                <input type="submit" class="btn btn-success"
                                       style="margin-top: 32px; <?= $alterando ? 'display:none' : "" ?>"
                                       name="btnSalvar" id="btnSalvar" value="SALVAR"/>
                                <input type="submit" class="btn btn-success col-lg-12"
                                       style="margin-top: 32px; <?= $alterando ? "" : 'display:none' ?>"
                                       name="btnEditar" id="btnEditar" value="ALTERAR"/>
                            </div>
                            <br/>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <?php echo $resultado ?>
                                </div>
                            </div>
                            <?php if(!empty($erros)){?>
                            <div class="form-group">
                                <div class="col-xs-12" style="border: solid 2px red">
                                    <ul style="list-style: none;">
                                        <?php
                                        foreach ($erros as $e) {
                                            ?>
                                            <li><?= $e; ?></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                           <?php } ?>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->
        <!-- Inicio Listagem -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title" style="margin: 20px;">Usuários Cadastrados</h4>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nome</th>
                                    <th>Perfil</th>
                                    <th>Igreja</th>
                                    <th class="text-nowrap">Ação</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $usuarios = $usuarioController->ObterUsuarios();
                                if (count($usuarios) > 0) {
                                    foreach ($usuarios as $user) { ?>
                                        <tr>
                                            <td><?= $user->getId() ?></td>
                                            <td><?= $user->getNome() ?></td>
                                            <td><?= $user->getPerfil() ?></td>
                                            <td><?= $user->igreja->getNome() ?></td>
                                            <td><a class="btn btn-danger"
                                                   href="?page=usuarios&del=<?= $user->getId() ?>"
                                                   onclick="return confirm('Deseja excluir o usuário <?= $user->getNome() ?>?');">
                                                    Excluir</a></td>
                                        </tr>
                                    <?php }
                                } else { ?>
                                    <tr>
                                        <td colspan="3">
                                            <div class="alert alert-warning">Não há usuários cadastrados no sistema.
                                            </div>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fim Listagem -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <?php include "View/Template/service-panel.php"; ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <?php include "View/Template/footer.php"; ?>
