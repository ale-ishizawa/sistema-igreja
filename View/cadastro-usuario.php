<?php
require_once './_autoload.php';

/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 29/04/2018
 * Time: 21:46
 */

use Model\Usuario;
use Controller\UsuarioController;
use Controller\IgrejaController;

$igrejaController = new IgrejaController();

$resultado = "";
$erros = [];

$btnCadastrar = filter_input(INPUT_POST, 'btnCadastrar') != NULL;

if ($btnCadastrar) {
    $usuarioController = new UsuarioController();

    $erros = Validar();

    if (empty($erros)) {
        $usuario = new Usuario();

        $senha = filter_input(INPUT_POST, "txtSenha", FILTER_SANITIZE_STRING);
        $senha = password_hash($senha, PASSWORD_DEFAULT);

        $usuario->setAtivo(1);
        $usuario->setEmail(filter_input(INPUT_POST, "txtEmail", FILTER_SANITIZE_STRING));
        $usuario->setNome(filter_input(INPUT_POST, "txtNome", FILTER_SANITIZE_STRING));
        $usuario->setPerfil(filter_input(INPUT_POST, "selPerfil"));
        $usuario->setSenha($senha);
        $usuario->setUsuario(filter_input(INPUT_POST, "txtUsuario", FILTER_SANITIZE_STRING));
        $usuario->igreja->setId(filter_input(INPUT_POST, 'selIgreja'));

        if ($usuarioController->Gravar($usuario)) {
            $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Usuário cadastrado no sistema.
                        </div>';
        }    else {
            $resultado = "ERRO";
        }
    }
}


function Validar()
{
    $listaErros = [];
    $usuarioController = new UsuarioController();

    if (strlen(filter_input(INPUT_POST, "txtNome", FILTER_SANITIZE_STRING)) < 3) {
        $listaErros[] = "- Nome inválido. (min 3 caracteres)";
    }

    if (strlen(filter_input(INPUT_POST, "txtUsuario", FILTER_SANITIZE_STRING)) >= 3) {
        if ($usuarioController->VerificaUsuarioExiste(filter_input(INPUT_POST, "txtUsuario", FILTER_SANITIZE_STRING)) == 1) {
            $listaErros[] = "- Usuário já cadastrado.";
        }
    } else {
        $listaErros[] = "- Usuário inválido. (min 3 caracteres)";
    }

    if (filter_input(INPUT_POST, "txtEmail", FILTER_SANITIZE_EMAIL)) {
        if ($usuarioController->VerificaEmailExiste(filter_input(INPUT_POST, "txtEmail", FILTER_SANITIZE_STRING)) == 1) {
            $listaErros[] = "- E-mail já cadastrado.";
        }
    } else {
        $listaErros[] = "- E-mail inválido.";
    }

    if (filter_input(INPUT_POST, "txtSenha", FILTER_SANITIZE_STRING) != filter_input(INPUT_POST, "txtSenha2", FILTER_SANITIZE_STRING)
        || strlen(filter_input(INPUT_POST, "txtSenha", FILTER_SANITIZE_STRING) < 5)) {
        $listaErros[] = "- Senhas inválidas. (min 5 caracteres)";
    }

    return $listaErros;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Monster Admin Template - The Most Complete & Trusted Bootstrap 4 Admin Template</title>
    <!-- Bootstrap Core CSS -->
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/plugins/toast-master/css/jquery.toast.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
</head>

<body>
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<section id="wrapper" class="login-register login-sidebar"
         style="background-image:url(assets/images/background/bg-church.jpg);">
    <div class="login-box card">
        <div class="card-block">
            <form method="post" action="?page=cadastro-usuario" class="form-horizontal form-material">
                <a href="javascript:void(0)" class="text-center db"><img src="assets/images/logo-ibrf.png"
                                                                         alt="Home" width="150px" height="100px"/><br/></a>
                <h3 class="box-title m-t-40 m-b-0">Cadastro de Usuário</h3>
                <small></small>
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" required="" name="txtNome" id="txtNome"
                               placeholder="Nome">
                    </div>
                </div>
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" required="" name="txtUsuario" id="txtUsuario"
                               placeholder="Usuário">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" required="" name="txtEmail" id="txtEmail"
                               placeholder="Email">
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" required="" name="txtSenha" id="txtSenha"
                               placeholder="Senha">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" required="" name="txtSenha2" id="txtSenha2"
                               placeholder="Confirmar Senha">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <select class="form-control" id="selPerfil" name="selPerfil">
                            <option value="0">Selecione o Perfil...</option>
                            <option value="1">Administrador</option>
                            <option value="2">Secretário (a)</option>
                            <option value="3">Tesoureiro (a)</option>
                            <!--                            <option value="4"> Membro</option>-->
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <select class="form-control" id="selIgreja" name="selIgreja" required>
                            <option value="0">Selecione a Igreja...</option>
                            <?php $listaIgrejas = $igrejaController->ObterIgrejasSelect();
                            foreach ($listaIgrejas as $igreja) { ?>
                                <option value="<?= $igreja->getId() ?>">
                                    <?= $igreja->getNome() ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <input type="submit" class="btn btn-success btn-lg btn-block text-uppercase waves-light"
                               name="btnCadastrar" id="btnCadastrar" value="CADASTRAR"/>
                    </div>
                </div>
                <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        <p>Já possui cadastro ? <a href="?page=login" class="text-info m-l-5"><b>Entrar</b></a></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <?php echo $resultado ?>
                    </div>
                </div>
                <div class="form-group">
                    <div id="login-alert" class="alert alert-danger col-sm-12">
                        <div class="col-xs-12">
                            <ul style="list-style: none;">
                                <?php
                                foreach ($erros as $e) {
                                    ?>
                                    <li><?= $e; ?></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<script src="assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="assets/plugins/bootstrap/js/tether.min.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!--Menu sidebar -->
<script src="js/sidebarmenu.js"></script>
<!--stickey kit -->
<script src="assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
<script src="assets/plugins/toast-master/js/jquery.toast.js"></script>

<!--Custom JavaScript -->
<script src="js/custom.min.js"></script>
<!-- ============================================================== -->
<!-- Style switcher -->
<!-- ============================================================== -->
<script src="assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>

<script type="text/javascript">
    $(".tst2").click(function () {
        $.toast({
            heading: 'Welcome to Monster admin',
            text: 'Use the predefined ones, or specify a custom position object.',
            position: 'top-right',
            loaderBg: '#ff6849',
            icon: 'warning',
            hideAfter: 3500,
            stack: 6
        });

    });
</script>
