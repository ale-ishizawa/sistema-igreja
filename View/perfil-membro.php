<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 25/05/2018
 * Time: 12:13
 */
require_once './_autoload.php';
include "View/Template/header.php";
include "View/Template/menu.php";

//session_start();

use Model\Membro;
use Controller\MembroController;


$membroController = new MembroController();

$idMembro = $_GET['editar'];

$membro = $membroController->ObterMembroPorCod($idMembro);


?>

    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">Perfil </h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                    <li class="breadcrumb-item">Membros</li>
                    <li class="breadcrumb-item active">Perfil</li>
                </ol>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10">
                    <i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

        <!-- Row -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-12 col-xlg-9 col-md-7">
                <div class="card">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs profile-tab" role="tablist">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home" role="tab">Dados
                                Pessoais</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#profile"
                                                role="tab">Endereço</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#settings"
                                                role="tab">Contatos</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content" style="margin: 25px !important;">
                        <div class="tab-pane active" id="home" role="tabpanel">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 col-xs-6 b-r"><strong>Nome Completo</strong>
                                        <br>
                                        <p class="text-muted"><?= strtoupper($membro->getNome()) ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"><strong>CPF</strong>
                                        <br>
                                        <p class="text-muted"><?= $membro->getCpf() ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"><strong>RG</strong>
                                        <br>
                                        <p class="text-muted"><?= $membro->getRg() ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"><strong>Órgão Emissor</strong>
                                        <br>
                                        <p class="text-muted"><?= $membro->getOrgao() ?></p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-3 col-xs-6 b-r"><strong>Naturalidade</strong>
                                        <br>
                                        <p class="text-muted"><?= $membro->getNaturalidade() ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"><strong>Data de Nascimento</strong>
                                        <br>
                                        <p class="text-muted"><?= date('d/m/Y', strtotime($membro->getNascimento())) ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"><strong>Nível Escolar</strong>
                                        <br>
                                        <p class="text-muted"><?php switch ($membro->getNivelEscolar()) {
                                                case 1:
                                                    echo 'Ensino Fundamental (1º grau)';
                                                    break;
                                                case 2:
                                                    echo 'Ensino Medio (2º grau)';
                                                    break;
                                                case 3:
                                                    echo 'Ensino Superior';
                                                    break;
                                                case 4:
                                                    echo 'Pós-graduação';
                                                    break;
                                                case 5:
                                                    echo 'Mestrado';
                                                    break;
                                                case 6:
                                                    echo 'Doutorado';
                                                    break;
                                                case 7:
                                                    echo 'Pós-doutorado';
                                                    break;
                                            } ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"><strong>Gênero</strong>
                                        <br>
                                        <p class="text-muted"><?= $membro->getSexo() == "M" ? "Masculino" : "Feminino" ?></p>
                                    </div>
                                </div>
                                <hr>
                                <?php
                                if ($membro->getEstadoCivil() == 2) {
                                    ?>
                                    <div class="row">
                                        <div class="col-md-3 col-xs-6 b-r"><strong>Estado Civil</strong>
                                            <br>
                                            <p class="text-muted">Casado (a)</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"><strong>Cônjuge</strong>
                                            <br>
                                            <p class="text-muted"><?= strtoupper($membro->getConjuge()) ?></p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"><strong>Data de Casamento</strong>
                                            <br>
                                            <p class="text-muted"><?= date('d/m/Y', strtotime($membro->getDataCasamento())) ?></p>
                                        </div>
                                        <div class="col-md-3 col-xs-6"><strong>Igreja Origem</strong>
                                            <br>
                                            <p class="text-muted"><?= $membro->getIgrejaOrigem() ?></p>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-3 col-xs-6 b-r"><strong>Nome do Pai</strong>
                                            <br>
                                            <p class="text-muted"> <?= strtoupper($membro->getNomePai()) ?> </p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"><strong>Nome da Mãe</strong>
                                            <br>
                                            <p class="text-muted"><?= strtoupper($membro->getNomeMae()) ?></p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"><strong>Data de Batismo</strong>
                                            <br>
                                            <p class="text-muted"><?= date('d/m/Y', strtotime($membro->getDataBatismo())) ?></p>
                                        </div>
                                        <div class="col-md-3 col-xs-6"><strong>Cargo na Igreja</strong>
                                            <br>
                                            <p class="text-muted"><?= $membro->cargo->getNome() ?></p>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="row">
                                        <div class="col-md-3 col-xs-6 b-r"><strong>Estado Civil</strong>
                                            <br>
                                            <p class="text-muted"><?php switch ($membro->getEstadoCivil()) {
                                                    case 1:
                                                        echo 'Solteiro (a)';
                                                        break;
                                                    case 3:
                                                        echo 'Separado (a)';
                                                        break;
                                                    case 4:
                                                        echo 'Divorciado (a)';
                                                        break;
                                                    case 5:
                                                        echo 'Viúvo (a)';
                                                        break;
                                                    case 6:
                                                        echo 'Amasiado (a)';
                                                        break;
                                                } ?></p>
                                        </div>
                                        <div class="col-md-3 col-xs-6"><strong>Igreja Origem</strong>
                                            <br>
                                            <p class="text-muted"><?= $membro->getIgrejaOrigem() ?></p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"><strong>Nome do Pai</strong>
                                            <br>
                                            <p class="text-muted"> <?= strtoupper($membro->getNomePai()) ?> </p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"><strong>Nome da Mãe</strong>
                                            <br>
                                            <p class="text-muted"><?= strtoupper($membro->getNomeMae()) ?></p>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-3 col-xs-6 b-r"><strong>Data de Batismo</strong>
                                            <br>
                                            <p class="text-muted"> <?= date('d/m/Y', strtotime($membro->getDataBatismo())) ?> </p>
                                        </div>
                                        <div class="col-md-3 col-xs-6"><strong>Cargo na Igreja</strong>
                                            <br>
                                            <p class="text-muted"><?= $membro->cargo->getNome() ?></p>
                                        </div>
                                    </div>
                                <?php } ?>
                                <hr>
                            </div>
                        </div>
                        <!--second tab-->
                        <div class="tab-pane" id="profile" role="tabpanel">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 col-xs-6 b-r"><strong>Logradouro</strong>
                                        <br>
                                        <p class="text-muted"><?= strtoupper($membro->getLogradouro()) ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"><strong>Número</strong>
                                        <br>
                                        <p class="text-muted"><?= $membro->getNumero() ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"><strong>Complemento</strong>
                                        <br>
                                        <p class="text-muted"><?= $membro->getComplemento() ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"><strong>Bairro</strong>
                                        <br>
                                        <p class="text-muted"><?= $membro->getBairro() ?></p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-3 col-xs-6 b-r"><strong>CEP</strong>
                                        <br>
                                        <p class="text-muted"><?= $membro->getCep() ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"><strong>Cidade</strong>
                                        <br>
                                        <p class="text-muted"><?= strtoupper($membro->cidade->getNome()) ?></p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"><strong>UF</strong>
                                        <br>
                                        <p class="text-muted"><?= strtoupper($membro->cidade->getUf()) ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="settings" role="tabpanel">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 col-xs-6 b-r"><strong>Email</strong>
                                        <br>
                                        <p class="text-muted"><?= $membro->getEmail() ?></p>
                                    </div>
                                    <div class="col-md-4 col-xs-6 b-r"><strong>Telefone</strong>
                                        <br>
                                        <p class="text-muted"><?= $membro->getTelefone() ?></p>
                                    </div>
                                    <div class="col-md-4 col-xs-6 b-r"><strong>Celular</strong>
                                        <br>
                                        <p class="text-muted"><?= $membro->getCelular() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
        <!-- Row -->

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <?php include "View/Template/service-panel.php"; ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

<?php include "View/Template/footer.php"; ?>