<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 29/05/2018
 * Time: 14:44
 */
require_once './_autoload.php';
include "View/Template/header.php";
include "View/Template/menu.php";

session_start();
setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');

use Model\Caixa;
use Controller\CaixaController;

//Obtém o id da igreja relacionado ao usuário logado
$idIgrejaSessao = isset($_SESSION['idigreja']) ? $_SESSION['idigreja'] : null;
$idUsuarioSessao = isset($_SESSION['idusuario']) ? $_SESSION['idusuario'] : null;

$caixaController = new CaixaController();
$caixa = new Caixa();
$resultado = "";
$erros = [];
$fechando = false;

$statusCaixa = $caixaController->VerificaStatusCaixa($idIgrejaSessao);
$idCaixa = $statusCaixa['idcaixa'];
if ($statusCaixa == false) {
    $fechando = false;
    $resultado = '<div class="alert alert-danger">Para habilitar o gerenciamento do módulo financeiro por favor abra o caixa!</div>';
} else {
    if ($statusCaixa['statuscaixa'] == 1) {
        $fechando = true;
    } else {
        $fechando = false;
        $resultado = '<div class="alert alert-danger">Para habilitar o gerenciamento do módulo financeiro por favor abra o caixa!</div>';
    }
}

//Parâmetro enviado via URL para definir qual mensagem será exibida para o usuário quando ele Abrir o caixa.
$abertoParam = filter_input(INPUT_GET, 'aberto');
if ($abertoParam == 1) {
    $caixa = new Caixa();
    $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Caixa aberto!.
                        </div>';
    $fechando = true;
}
if ($abertoParam == 2) {
    $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao abrir o caixa.
                        </div>';
}

//Parâmetro enviado via URL para definir qual mensagem será exibida para o usuário quando ele Fechar o caixa.
$fechadoParam = filter_input(INPUT_GET, 'fechado');
if ($fechadoParam == 1) {
    $caixa = new Caixa();
    $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Caixa Fechado!.
                        </div>';
}
if ($fechadoParam == 2) {
    $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao fechar o caixa.
                        </div>';
}

$btnAbrir = filter_input(INPUT_POST, 'btnAbrir') != NULL;
$btnFechar = filter_input(INPUT_POST, 'btnFechar') != NULL;

if ($btnAbrir || $btnFechar) {
    if ($btnAbrir) {
        //ABERTURA DO CAIXA
        $caixa->setStatus(1);
        $valor = filter_input(INPUT_POST, 'txtValorInicial', FILTER_SANITIZE_STRING);
        $valor = substr($valor, 3);
        if (strstr($valor, ','))
            $valor = str_replace(',', '', $valor);

        $caixa->setValorInicial($valor);
        $horaAbertura = date('Y-m-d H:i:s');
        $caixa->setHoraAbertura($horaAbertura);
        $dataCaixa = date('Y-m-d');
        $caixa->setDataCaixa($dataCaixa);
        $caixa->igreja->setId($idIgrejaSessao);
        $caixa->usuario->setId($idUsuarioSessao);

        if ($caixaController->AbrirCaixa($caixa)) {
            echo "<script>window.location.href = '?page=caixa&aberto=1' </script>";
        } else {
            echo "<script>window.location.href = '?page=caixa&aberto=2' </script>";
        }
    } else {
        //FECHAMENTO DE CAIXA
        if ($idCaixa > 0) {
            $caixa->setId($idCaixa);
            $horaFechamento = date('Y-m-d H:i:s');
            $caixa->setHoraFechamento($horaFechamento);
            $dataCaixa = date('Y-m-d');
            $caixa->setDataCaixa($dataCaixa);
            $caixa->setStatus(2);
            $caixa->usuario->setId($idUsuarioSessao);
            $caixa->igreja->setId($idIgrejaSessao);

            if ($caixaController->FecharCaixa($caixa)) {
                echo "<script>window.location.href = '?page=caixa&fechado=1' </script>";

            } else {
                echo "<script>window.location.href = '?page=caixa&fechado=2' </script>";

            }
        }
    }
}

?>


<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">Gerenciamento de Caixa</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Início</a></li>
                    <li class="breadcrumb-item active">Caixa</li>
                </ol>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10">
                    <i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

        <!-- Row -->
        <div class="row">
            <div class="col-lg-3">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">Abrir/Fechar Caixa</h4>
                        <br/>
                        <form method="post" action="?page=caixa">
                            <?php if (!$fechando) { ?>
                                <div class="form-group">
                                    <label class="control-label">Valor Inicial</label>
                                    <input type="text" id="txtValorInicial" name="txtValorInicial" class="form-control"
                                           placeholder="Valor inicial do caixa" required data-affixes-stay="true"
                                           data-prefix="R$ " data-thousands="," data-decimal=".">
                                </div>
                            <?php } ?>
                            <div class="form-actions">
                                <input type="submit" class="btn btn-success"
                                       style="margin-top: 32px; <?= $fechando ? 'display:none' : "" ?>"
                                       name="btnAbrir" id="btnAbrir" value="ABRIR CAIXA"/>
                                <input type="submit" class="btn btn-danger"
                                       style="margin-top: 32px; <?= $fechando ? "" : 'display:none' ?>"
                                       name="btnFechar" id="btnFechar" value="FECHAR CAIXA"/>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <br/>
                                    <?php echo $resultado ?>
                                </div>
                            </div>
                            <?php if (!empty($erros)) { ?>
                                <div class="form-group">
                                    <div class="col-xs-12" style="border: solid 2px red">
                                        <ul style="list-style: none;">
                                            <?php
                                            foreach ($erros as $e) {
                                                ?>
                                                <li><?= $e; ?></li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">Histórico de status do Caixa</h4>
                        <div class="table-responsive m-t-40" style="min-height: 180px">
                            <?php $caixas = $caixaController->ObterCaixas($idIgrejaSessao);
                            if (count($caixas) > 0) { ?>
                                <table id="myTable" class="table table-bordered table-striped" style="font-size: 13px;">
                                    <thead>
                                    <tr>
                                        <th>Data</th>
                                        <th>Status</th>
                                        <th>Aberto</th>
                                        <th>Fechado</th>
                                        <th>Valor Inicial</th>
                                        <th>Valor Final</th>
                                        <th>Usuário</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($caixas as $caixa) { ?>
                                        <tr>
                                            <td><?= date('d/m/Y', strtotime($caixa->getDataCaixa())) ?></td>
                                            <td><?= $caixa->getStatus() == 1 ? 'Foi Aberto' : 'Foi Fechado' ?></td>
                                            <td><?= $caixa->getHoraAbertura() == null ? '-' : date('H:i', strtotime($caixa->getHoraAbertura())) ?></td>
                                            <td><?= $caixa->getHoraFechamento() == null ? '-' : date('H:i', strtotime($caixa->getHoraFechamento())) ?></td>
                                            <td><?= number_format($caixa->getValorInicial(), 2, ',', '.') ?></td>
                                            <td><?= $caixa->getValorFinal() == null ? '-' : number_format($caixa->getValorFinal(), 2, ',', '.') ?></td>
                                            <td><?= $caixa->usuario->getNome() ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            <?php } else { ?>
                                <div class="alert alert-warning">Não há atualizações de caixa hoje.</div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <?php include "View/Template/service-panel.php"; ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <?php include "View/Template/footer.php"; ?>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
    <script>
        $(function () {
            $('#txtValorInicial').maskMoney();
        })
    </script>
