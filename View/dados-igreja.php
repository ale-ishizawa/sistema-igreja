<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 27/05/2018
 * Time: 15:19
 */

require_once './_autoload.php';
include "View/Template/header.php";
include "View/Template/menu.php";

session_start();

use Model\Igreja;
use Controller\IgrejaController;

$igrejaController = new IgrejaController();
$idIgreja = $_SESSION['idigreja'];

$igreja = $igrejaController->ObterIgreja($idIgreja);

?>
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">Dados da Igreja </h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                    <li class="breadcrumb-item">Igreja</li>
                    <li class="breadcrumb-item active">Dados da Igreja</li>
                </ol>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10">
                    <i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

        <!-- Row -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-12 col-xlg-9 col-md-7">
                <div class="card">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs profile-tab" role="tablist">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home" role="tab">Dados
                                da Igreja </a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content" style="margin: 25px !important;">
                        <div class="tab-pane active" id="home" role="tabpanel">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 col-xs-6 b-r"><strong>Nome Completo</strong>
                                        <br>
                                        <p class="text-muted"><?= strtoupper($igreja->getNome()) ?></p>
                                    </div>
                                    <div class="col-md-4 col-xs-6 b-r"><strong>CNPJ</strong>
                                        <br>
                                        <p class="text-muted"><?= $igreja->getCnpj() ?></p>
                                    </div>
                                    <div class="col-md-4 col-xs-6 b-r"><strong>Inscrição Estadual</strong>
                                        <br>
                                        <p class="text-muted"><?= $igreja->getIe() ?></p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-4 col-xs-6 b-r"><strong>Email</strong>
                                        <br>
                                        <p class="text-muted"><?= $igreja->getEmail() ?></p>
                                    </div>
                                    <div class="col-md-4 col-xs-6 b-r"><strong>Telefone</strong>
                                        <br>
                                        <p class="text-muted"><?= $igreja->getTelefone() ?></p>
                                    </div>
                                    <div class="col-md-4 col-xs-6 b-r"><strong>WebSite</strong>
                                        <br>
                                        <p class="text-muted"><?= $igreja->getSite() ?></p>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-4 col-xs-6 b-r"><strong>Logradouro</strong>
                                        <br>
                                        <p class="text-muted"><?= strtoupper($igreja->getLogradouro()) ?></p>
                                    </div>
                                    <div class="col-md-4 col-xs-6 b-r"><strong>Número</strong>
                                        <br>
                                        <p class="text-muted"><?= $igreja->getNumero() ?></p>
                                    </div>
                                    <div class="col-md-4 col-xs-6 b-r"><strong>Complemento</strong>
                                        <br>
                                        <p class="text-muted"><?= $igreja->getComplemento() ?></p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-4 col-xs-6"><strong>Bairro</strong>
                                        <br>
                                        <p class="text-muted"><?= $igreja->getBairro() ?></p>
                                    </div>
                                    <div class="col-md-4 col-xs-6"><strong>Cidade</strong>
                                        <br>
                                        <p class="text-muted"><?= strtoupper($igreja->cidade->getNome()) ?></p>
                                    </div>
                                    <div class="col-md-4 col-xs-6"><strong>UF</strong>
                                        <br>
                                        <p class="text-muted"><?= strtoupper($igreja->cidade->getUf()) ?></p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-4 col-xs-6"><strong>Igreja Sede ?</strong>
                                        <br>
                                        <p class="text-muted"><?= $igreja->getSede() == 1 ? "Sim" : "Não" ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
        <!-- Row -->

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <?php include "View/Template/service-panel.php"; ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

<?php include "View/Template/footer.php"; ?>