<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 03/06/2018
 * Time: 23:06
 */
require_once './_autoload.php';
include "View/Template/header.php";
include "View/Template/menu.php";

use Controller\CaixaController;
use Controller\MovimentoCaixaController;
use Model\MovimentoCaixa;

session_start();

$movimento = new MovimentoCaixa();
$movimentoController = new MovimentoCaixaController();
$idIgrejaSessao = isset($_SESSION['idigreja']) ? $_SESSION['idigreja'] : null;

$resultado = "";
$erros = [];
$codigo = "";

$idCaixa = VerificaCaixa($idIgrejaSessao);
if ($idCaixa == 0) {
    echo "<script>window.location.href = '?page=caixa' </script>";
} else {

//Inserção
    $btnSalvar = filter_input(INPUT_POST, 'btnSalvar') != NULL;
    if ($btnSalvar) {

        $erros = Validar();

        //Verifico se não possui erros de validação
        if (empty($erros)) {
            //Inserção de Novo Registro
            $dataMovimento = date('Y-m-d H:i:s');
            $movimento->setData($dataMovimento);
            $movimento->setTipo(filter_input(INPUT_POST, 'selTipo'));
            $movimento->usuario->setId($_SESSION['idusuario']);
            $movimento->setDescricao(filter_input(INPUT_POST, 'txtDescricao', FILTER_SANITIZE_STRING));

            $valor = filter_input(INPUT_POST, 'txtValor', FILTER_SANITIZE_STRING);
            $valor = substr($valor, 3);
            if(strstr($valor, ','))
                $valor = str_replace(',','', $valor);
            $movimento->setValor($valor);

            $movimento->igreja->setId($idIgrejaSessao);
            $movimento->caixa->setId($idCaixa);

            if ($movimentoController->Gravar($movimento)) {
                $movimento = new MovimentoCaixa();
                $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                                     </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro salvo com sucesso.
                                 </div>';
            } else {
                $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                                     </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao salvar registro.
                                 </div>';
            }
        }
    }
}


function Validar()
{
    $listaErros = [];

    if (strlen(filter_input(INPUT_POST, 'txtDescricao', FILTER_SANITIZE_STRING)) <= 10) {
        $listaErros[] = "- Descrição inválida. (min. 10 caracteres)";
    }
    if (filter_input(INPUT_POST, 'txtValor', FILTER_SANITIZE_STRING) == "") {
        $listaErros[] = "- Informe o Valor da compra.";
    }
    if (filter_input(INPUT_POST, 'selTipo', FILTER_SANITIZE_STRING) == "0") {
        $listaErros[] = "- Selecione o Tipo de Movimento.";
    }

    return $listaErros;
}

function VerificaCaixa($idIgrejaSessao)
{
    $caixaController = new CaixaController();
    $statusCaixa = $caixaController->VerificaStatusCaixa($idIgrejaSessao);
    if ($statusCaixa == false) {
        return 0;
    } else {
        if ($statusCaixa['statuscaixa'] == 1) {
            $idCaixa = $statusCaixa['idcaixa'];
            return $idCaixa;
        } else {
            return 0;
        }
    }
}

?>

<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">Movimentações de Caixa</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Início</a></li>
                    <li class="breadcrumb-item">Caixa</li>
                    <li class="breadcrumb-item active">Movimentações de Caixa</li>
                </ol>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10">
                    <i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">Novo Movimento de Caixa</h4>
                        <form method="post" action="?page=movimentacoes-caixa">
                            <br/>
                            <div class="form-group">
                                <label class="control-label">Tipo de Movimento</label>
                                <select class="form-control" id="selTipo" name="selTipo" required>
                                    <option value="0">Selecione o Tipo de movimento...</option>
                                    <option value="1">Depósito</option>
                                    <option value="2">Saque</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="txtCodigo" id="txtCodigo" value="<?= $codigo ?>"/>
                                <label class="control-label">Descrição</label>
                                <textarea type="text" id="txtDescricao" name="txtDescricao" class="form-control"
                                          placeholder="Descrição" rows="3" minlength="5"
                                          maxlength="255" required> </textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Valor</label>
                                <input type="text" id="txtValor" name="txtValor" class="form-control"
                                          placeholder="Valor" required data-affixes-stay="true" data-prefix="R$ " data-thousands="," data-decimal="."/>
                            </div>
                            <div class="form-actions">
                                <input type="submit" class="btn btn-success"
                                       style="margin-top: 20px;"
                                       name="btnSalvar" id="btnSalvar" value="SALVAR"/>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <br/>
                                    <?php echo $resultado ?>
                                </div>
                            </div>
                            <?php if (!empty($erros)) { ?>
                                <div class="form-group">
                                    <div class="col-xs-12" style="border: solid 2px red">
                                        <ul style="list-style: none;">
                                            <?php
                                            foreach ($erros as $e) {
                                                ?>
                                                <li><?= $e; ?></li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">Histórico de Movimentações</h4>
                        <h6 class="card-subtitle">Movimentações nos últimos 30 dias.</h6>
                        <div class="table-responsive m-t-40" style="min-height: 180px">
                            <?php $movimentos = $movimentoController->ObterMovimentacoes($idIgrejaSessao);
                            if (count($movimentos) > 0) { ?>
                                <table id="myTable" class="table table-bordered table-striped" style="font-size: 14px;">
                                    <thead>
                                    <tr>
<!--                                        <th>Ações</th>-->
                                        <th>Data</th>
                                        <th>Descrição</th>
                                        <th>Tipo</th>
                                        <th>Valor</th>
                                        <th>Usuário</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($movimentos as $mov) { ?>
                                        <tr>
                                            <!--<td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info dropdown-toggle"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        <i class="ti-settings"></i>
                                                    </button>
                                                    <div class="dropdown-menu animated flipInX">
                                                        <a class="dropdown-item"
                                                           href="?page=compras&del=<?= $mov->getId() ?>"
                                                           onclick="return confirm('Deseja estornar a compra <?= $mov->getDescricao() ?> ?');">Estornar</a>
                                                    </div>
                                                </div>
                                            </td> -->
                                            <td><?= date('d/m/Y', strtotime($mov->getData())) ?></td>
                                            <td><?= $mov->getDescricao() ?></td>
                                            <td><?= $mov->getTipo() == 1 ? '<span class="label label-success">CRÉDITO</span>' : '<span class="label label-danger">DÉBITO</span>' ?></td>
                                            <td><?= number_format($mov->getValor(), 2, ',', '.') ?></td>
                                            <td><?= $mov->usuario->getNome() ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            <?php } else { ?>
                                <div class="alert alert-warning">Não há histórico de movimentações nos últimos 30 dias.</div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <?php include "View/Template/service-panel.php"; ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

    <?php include "View/Template/footer.php"; ?>
    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
    <script>
        //For MaskMoney
        $(function() {
            $('#txtValor').maskMoney();
        })

        //For DataTable
        $(document).ready(function() {
            var aux = [{ "bSortable": true }, { "bSortable": true }, { "bSortable": true }, { "bSortable": true }, { "bSortable": true }];
            $('#myTable').DataTable({
                "pageLength": 10,
                "language": {
                    "lengthMenu": "EXIBIR _MENU_",
                    "zeroRecords": "Registros não encontrados",
                    "info": "Página _PAGE_ de _PAGES_",
                    "infoEmpty": "Registro não encontrado",
                    "infoFiltered": "(Um total de _MAX_ registros)",
                    "sSearch": "PESQUISAR: ",
                    "oPaginate": {
                        "sFirst": "Início",
                        "sPrevious": "Anterior",
                        "sNext": "Próximo",
                        "sLast": "Último"
                    }
                },
                "aoColumns": aux
            });
            getCss();
        });

        function getCss()
        {
            $('label select').css('margin-left', '10px');
            $('#myTable_filter label').css('text-transform', 'uppercase');
            $('#myTable_length label').css('text-transform', 'uppercase');
            $('#myTable_length label').css('font-weight', 'bold');
            $('#myTable_length label').css('font-size', '0.9em');
            $('#myTable_length label').css('color', '#777171');
            $('#myTable_filter label').css('font-weight', 'bold');
            $('#myTable_filter label').css('font-size', '0.9em');
            $('#myTable_filter label').css('color', '#777171');
            $('input[type=search]').css('padding', '5px 5px');
            $('input[type=search]').css('width', '165px');
            $('#myTable_filter').css('margin-top', '25px');
            $('#myTable_length').css('margin-top', '25px');
        }
    </script>