<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 09/06/2018
 * Time: 14:43
 */
require_once './_autoload.php';
include "View/Template/header.php";
include "View/Template/menu.php";

use Controller\PastorController;
use Model\Pastor;

session_start();

$pastor = new Pastor();
$pastorController = new PastorController();
$idIgrejaSessao = isset($_SESSION['idigreja']) ? $_SESSION['idigreja'] : null;

$resultado = "";
$erros = [];
$codigo = "";


//DELETE
$codExcluir = filter_input(INPUT_GET, 'del');
if ($codExcluir != NULL) {
    if ($pastorController->Deletar($codExcluir)) {
        $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro excluído com sucesso.
                        </div>';
    } else {
        $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao excluir registro.
                        </div>';
    }
}

//Inserção
$btnSalvar = filter_input(INPUT_POST, 'btnSalvar') != NULL;
if ($btnSalvar) {
    $erros = Validar();

    //Verifico se não possui erros de validação
    if (empty($erros)) {
        //Inserção de Novo Registro
        $pastor->setId(filter_input(INPUT_POST, 'txtIdPastor'));

        //Verifico se o líder já esta cadastrado
        if ($pastorController->VerificaCadastro($pastor) == false) {
            if ($pastorController->Gravar($pastor)) {
                $pastor = new Pastor();
                $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                                     </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro salvo com sucesso.
                                 </div>';
            } else {
                $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                                     </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao salvar registro.
                                 </div>';
            }
        } else {
            $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                                     </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Atenção! </h3> O membro informado ja está cadastrado como um Pastor.
                              </div>';
        }
    }
}


function Validar()
{
    $listaErros = [];

    if (filter_input(INPUT_POST, 'txtIdPastor') == "") {
        $listaErros[] = "- Informe o nome do Pastor.";
    }

    return $listaErros;
}

?>

<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">Pastores</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Início</a></li>
                    <li class="breadcrumb-item active">Pastores</li>
                </ol>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10">
                    <i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">Cadastrar Pastor</h4>
                        <form method="post" action="?page=pastores">
                            <div class="form-group">
                                <input type="hidden" name="txtCodigo" id="txtCodigo" value="<?= $codigo ?>"/>
                                <label class="control-label">Nome do Pastor</label>
                                <input type="text" id="txtPastor" name="txtPastor" class="form-control"
                                       placeholder="Informe o nome de um membro" required maxlength="60"/>
                                <input type="hidden" name="txtIdPastor" id="txtIdPastor"/>
                            </div>
                            <div class="form-actions">
                                <input type="submit" class="btn btn-success"
                                       style="margin-top: 32px;"
                                       name="btnSalvar" id="btnSalvar" value="SALVAR"/>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <br/>
                                    <?php echo $resultado ?>
                                </div>
                            </div>
                            <?php if (!empty($erros)) { ?>
                                <div class="form-group">
                                    <div class="col-xs-12" style="border: solid 2px red">
                                        <ul style="list-style: none;">
                                            <?php
                                            foreach ($erros as $e) {
                                                ?>
                                                <li><?= $e; ?></li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">Pastores cadastrados</h4>
                        <div class="table-responsive m-t-40" style="min-height: 250px">
                            <?php $pastores = $pastorController->ObterPastores($idIgrejaSessao);
                            if (count($pastores) > 0) { ?>
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Ações</th>
                                        <th>Pastor</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($pastores as $pas) { ?>
                                        <tr>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info dropdown-toggle"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        <i class="ti-settings"></i>
                                                    </button>
                                                    <div class="dropdown-menu animated flipInX">
                                                        <a class="dropdown-item"
                                                           href="?page=pastores&del=<?= $pas->getId() ?>"
                                                           onclick="return confirm('Deseja excluir o pastor <?= $pas->getNome() ?> ?');">Excluir</a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><?= $pas->getNome() ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            <?php } else { ?>
                                <div class="alert alert-warning">Não há pastores cadastrados nessa igreja.</div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <?php include "View/Template/service-panel.php"; ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

    <?php include "View/Template/footer.php"; ?>
    <script type="text/javascript">

        $(function () {

            // Single Select
            $("#txtPastor").autocomplete({
                source: function (request, response) {
                    // Fetch data
                    $.ajax({
                        url: "Util/search-membro.php",
                        type: 'post',
                        dataType: "json",
                        data: {
                            search: request.term
                        },
                        success: function (data) {
                            response(data);
                        }
                    });
                },
                select: function (event, ui) {
                    // Set selection
                    $('#txtPastor').val(ui.item.label); // display the selected text
                    $('#txtIdPastor').val(ui.item.value); // save selected id to input
                    return false;
                }
            });
        });
    </script>
