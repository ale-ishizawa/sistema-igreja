<?php
/**
 * Created by PhpStorm.
 * User: Alessandro
 * Date: 13/06/2018
 * Time: 17:35
 */
session_start();
require_once './_autoload.php';
include "View/Template/header.php";
include "View/Template/menu.php";

use Controller\ClasseEbdController;
use Model\ClasseEbd;

$classeEbd = new ClasseEbd();
$classeController = new ClasseEbdController();
$idIgrejaSessao = isset($_SESSION['idigreja']) ? $_SESSION['idigreja'] : null;

$resultado = "";
$erros = [];
$codigo = "";
$alterando = false;

//UPDATE
$codEditar = filter_input(INPUT_GET, 'editar');
if ($codEditar != NULL) {
    $c = $classeController->ObterClassePorCod($codEditar);
    if ($c != NULL) {
        $codigo = $codEditar;
        $classeEbd->setId($c->getId());
        $classeEbd->setNome($c->getNome());
        $classeEbd->setDescricao($c->getDescricao());
        $classeEbd->setDataEncerramento($c->getDataEncerramento());
        $classeEbd->setDataInicio($c->getDataInicio());
        $alterando = true;
    }
}

//DELETE
$codExcluir = filter_input(INPUT_GET, 'del');
if ($codExcluir != NULL) {
    if ($classeController->Deletar($codExcluir)) {
        $resultado = '<di v class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro excluído com sucesso.
                        </div>';
    } else {
        $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao excluir registro.
                        </div>';
    }
}

$btnSalvar = filter_input(INPUT_POST, 'btnSalvar') != NULL;
$btnEditar = filter_input(INPUT_POST, 'btnEditar') != NULL;

if ($btnSalvar || $btnEditar) {
    $erros = Validar();

    //Se não existir erros de validação, envia os dados para a Controladora
    if (empty($erros)) {
        if ($btnEditar) {
            $codigo = filter_input(INPUT_POST, "txtCodigo", FILTER_VALIDATE_INT);
            if ($codigo === false) {
                $erros[] = "- Código não informado corretamente.";
            }
            if (empty($erros)) {
                //EDIÇÃO
                $classeEbd->setId($codigo);
                $classeEbd->setNome(filter_input(INPUT_POST, 'txtNome'));
                $classeEbd->setDescricao(filter_input(INPUT_POST, 'txtDescricao'));
                $classeEbd->setDataInicio(filter_input(INPUT_POST, 'txtDataInicio'));
                $classeEbd->setDataEncerramento(filter_input(INPUT_POST, 'txtDataEncerramento'));
                $classeEbd->igreja->setId($idIgrejaSessao);

                if ($classeController->Editar($classeEbd)) {
                    $classeEbd = new ClasseEbd();
                    $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro salvo com sucesso.
                        </div>';
                } else {
                    $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao salvar registro.
                        </div>';
                }
            }
        } else {
            //CADASTRO
            $classeEbd->setNome(filter_input(INPUT_POST, 'txtNome'));
            $classeEbd->setDescricao(filter_input(INPUT_POST, 'txtDescricao'));
            $classeEbd->setDataInicio(filter_input(INPUT_POST, 'txtDataInicio'));
            $classeEbd->setDataEncerramento(filter_input(INPUT_POST, 'txtDataEncerramento'));
            $classeEbd->igreja->setId($idIgrejaSessao);

            if ($classeController->Gravar($classeEbd)) {
                $classeEbd = new ClasseEbd();
                $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro salvo com sucesso.
                        </div>';
            } else {
                $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao salvar registro.
                        </div>';
            }
        }
    }
}

function Validar()
{
    $listaErros = [];

    if (strlen(filter_input(INPUT_POST, 'txtNome', FILTER_SANITIZE_STRING)) < 3) {
        $listaErros[] = "- Nome da classe inválido. (mínimo 3 caracteres)";
    }
    if(filter_input(INPUT_POST, 'txtDataInicio', FILTER_SANITIZE_STRING) == ""){
        $listaErros[] = "- Informe a data de início.";
    }

    return $listaErros;
}

?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">Classes EBD</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Início</a></li>
                    <li class="breadcrumb-item active">Classes EBD</li>
                </ol>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10">
                    <i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

        <!-- Row -->
        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">Cadastrar Classe Ebd</h4>
                        <form method="post" action="?page=classes-ebd">
                            <br/>
                            <input type="hidden" name="txtCodigo" id="txtCodigo" value="<?= $codigo ?>"/>
                            <div class="form-group">
                                <label class="control-label">Nome da Classe</label>
                                <input type="text" id="txtNome" name="txtNome" class="form-control" maxlength="100"
                                       placeholder="Nome da Classe" required value="<?= $classeEbd->getNome() ?>">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Descrição</label>
                                <textarea type="text" id="txtDescricao" name="txtDescricao" class="form-control" maxlength="255"
                                          placeholder="Descrição da classe"
                                          rows="3"><?= $classeEbd->getDescricao() ?> </textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Data de Início</label>
                                <input type="date" id="txtDataInicio" name="txtDataInicio" class="form-control"
                                       required value="<?= $classeEbd->getDataInicio() ?>">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Data de Encerramento</label>
                                <input type="date" id="txtDataEncerramento" name="txtDataEncerramento" class="form-control"
                                        value="<?= $classeEbd->getDataEncerramento() ?>">
                            </div>
                            <div class="form-actions">
                                <input type="submit" class="btn btn-success"
                                       style="margin-top: 32px; <?= $alterando ? 'display:none' : "" ?>"
                                       name="btnSalvar" id="btnSalvar" value="SALVAR"/>
                                <input type="submit" class="btn btn-success"
                                       style="margin-top: 32px; <?= $alterando ? "" : 'display:none' ?>"
                                       name="btnEditar" id="btnEditar" value="ALTERAR"/>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <br/>
                                    <?php echo $resultado ?>
                                </div>
                            </div>
                            <?php if (!empty($erros)) { ?>
                                <div class="form-group">
                                    <div class="col-xs-12" style="border: solid 2px red">
                                        <ul style="list-style: none;">
                                            <?php
                                            foreach ($erros as $e) {
                                                ?>
                                                <li><?= $e; ?></li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">Classes EBD</h4>
                        <div class="table-responsive m-t-40" style="min-height: 350px">
                            <?php $classes = $classeController->ObterClasses($idIgrejaSessao);
                            if (count($classes) > 0) { ?>
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Ações</th>
                                        <th>Nome</th>
                                        <th>Descrição</th>
                                        <th>Início</th>
                                        <th>Encerramento</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($classes as $cla) { ?>
                                        <tr>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info dropdown-toggle"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        <i class="ti-settings"></i>
                                                    </button>
                                                    <div class="dropdown-menu animated flipInX">
                                                        <a class="dropdown-item"
                                                           href="?page=classes-ebd&editar=<?= $cla->getId() ?>">Editar</a>
                                                        <a class="dropdown-item"
                                                           href="?page=classes-ebd&del=<?= $cla->getId() ?>"
                                                           onclick="return confirm('Deseja excluir a classe<?= $cla->getNome() ?>?');">Excluir
                                                            Classe</a>
                                                        <a class="dropdown-item"
                                                           href="?page=turma-ebd&ver=<?= $cla->getId() ?>">Visualizar
                                                            Turma</a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><?= $cla->getNome() ?></td>
                                            <td><?= $cla->getDescricao() ?></td>
                                            <td><?= date('d/m/Y', strtotime($cla->getDataInicio())) ?></td>
                                            <td><?= $cla->getDataEncerramento() == '0000-00-00' ? '-' : date('d/m/Y', strtotime($cla->getDataEncerramento())) ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            <?php } else { ?>
                                <div class="alert alert-warning">Não há classes cadastradas no sistema.</div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <?php include "View/Template/service-panel.php"; ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

    <?php include "View/Template/footer.php"; ?>

