<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 30/04/2018
 * Time: 01:05
 */
require_once './_autoload.php';
include "View/Template/header.php";
include "View/Template/menu.php";

use Model\Membro;
use Controller\MembroController;
use Controller\IgrejaController;
use Controller\CargoController;

$membro = new Membro();
$resultado = "";
$erros = [];
$alterando = false;
$codigo = "";

$membroController = new MembroController();
$igrejaController = new IgrejaController();
$cargoController = new CargoController();

$codEditar = filter_input(INPUT_GET, 'editar');
if ($codEditar != NULL) {
    $m = $membroController->ObterMembroPorCod($codEditar);
    if ($m != NULL) {
        $codigo = $codEditar;
        $membro->setAtivo($m->getAtivo());
        $membro->setNome($m->getNome());
        $membro->setSexo($m->getSexo());
        $membro->setLogradouro($m->getLogradouro());
        $membro->setNumero($m->getNumero());
        $membro->setBairro($m->getBairro());
        $membro->setComplemento($m->getComplemento());
        $membro->setCep($m->getCep());
        $membro->setTelefone($m->getTelefone());
        $membro->setCelular($m->getCelular());
        $membro->setEmail($m->getEmail());
        $membro->setNivelEscolar($m->getNivelEscolar());
        $membro->setNascimento($m->getNascimento());
        $membro->setNaturalidade($m->getNaturalidade());
        $membro->setRg($m->getRg());
        $membro->setOrgao($m->getOrgao());
        $membro->setCpf($m->getCpf());
        $membro->setEstadoCivil($m->getEstadoCivil());
        $membro->setConjuge($m->getConjuge());
        $membro->setDataCasamento($m->getDataCasamento());
        $membro->setDataBatismo($m->getDataBatismo());
        $membro->setNomePai($m->getNomePai());
        $membro->setNomeMae($m->getNomeMae());
        $membro->setIgrejaOrigem($m->getIgrejaOrigem());
        $membro->igreja->setId($m->igreja->getId());
        $membro->cargo->setId($m->cargo->getId());
        $membro->cidade->setNome($m->cidade->getNome());
        $membro->cidade->setUf($m->cidade->getUf());
        $alterando = true;
    }
}

$btnSalvar = filter_input(INPUT_POST, 'btnSalvar') != NULL;
$btnEditar = filter_input(INPUT_POST, 'btnEditar') != NULL;

if ($btnSalvar || $btnEditar) {

    $erros = Validar($membro);

    if (empty($erros)) {
        if ($btnEditar) {
            //EDIÇÃO
            $codigo = filter_input(INPUT_POST, "txtCodigo", FILTER_VALIDATE_INT);
            if ($codigo === false) {
                $erros[] = "- Código não informado corretamente.";
            }
            if (empty($erros)) {
                //EDIÇÃO DO CADASTRO VALIDADA, REALIZA O UPDATE
                $membro->setId($codigo);
                $membro->setAtivo(1);
                $membro->setNome(filter_input(INPUT_POST, "txtNome"));
                $membro->setSexo(filter_input(INPUT_POST, "selSexo"));
                $membro->setLogradouro(filter_input(INPUT_POST, "txtLogradouro"));
                $membro->setNumero(filter_input(INPUT_POST, "txtNumero"));
                $membro->setBairro(filter_input(INPUT_POST, "txtBairro"));
                $membro->setComplemento(filter_input(INPUT_POST, "txtComplemento"));
                $membro->setCep(filter_input(INPUT_POST, "txtCep"));
                $membro->setTelefone(filter_input(INPUT_POST, "txtTelefone"));
                $membro->setCelular(filter_input(INPUT_POST, "txtCelular"));
                $membro->setEmail(filter_input(INPUT_POST, "txtEmail"));
                $membro->setNivelEscolar(filter_input(INPUT_POST, "selNivelEscolar"));
                $membro->setNascimento(filter_input(INPUT_POST, "txtNascimento"));
                $membro->setNaturalidade(filter_input(INPUT_POST, "txtNaturalidade"));
                $membro->setRg(filter_input(INPUT_POST, "txtRg"));
                $membro->setOrgao(filter_input(INPUT_POST, "selOrgao"));
                $membro->setCpf(filter_input(INPUT_POST, "txtCpf"));
                $membro->setEstadoCivil(filter_input(INPUT_POST, "selEstadoCivil"));
                $membro->setConjuge(filter_input(INPUT_POST, "txtConjuge"));
                $dataCasamento = filter_input(INPUT_POST, "txtCasamento");
                $membro->setDataCasamento(filter_input(INPUT_POST, "txtCasamento") == "" ? null : $dataCasamento);
                $dataBatismo = filter_input(INPUT_POST, "txtBatismo");
                $membro->setDataBatismo(filter_input(INPUT_POST, "txtBatismo") == "" ? null : $dataBatismo);
                $membro->setNomePai(filter_input(INPUT_POST, "txtPai"));
                $membro->setNomeMae(filter_input(INPUT_POST, "txtMae"));
                $membro->setIgrejaOrigem(filter_input(INPUT_POST, "txtOrigem"));
                $membro->igreja->setId(filter_input(INPUT_POST, "selIgreja"));
                $cargo = filter_input(INPUT_POST, "selCargo");
                $membro->cargo->setId($cargo == "0" ? null : $cargo);

                //Obtendo valores da requisição VIA CEP
                $nomeCidade = isset($_POST['txtCidade']) ? $_POST['txtCidade'] : "";
                $Uf = isset($_POST['txtUf']) ? $_POST['txtUf'] : "";
                $membro->cidade->setNome($nomeCidade);
                $membro->cidade->setUf($Uf);

                if ($membroController->Editar($membro)) {
                    $membro = new Membro();
                    $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro salvo com sucesso.
                        </div>';
                } else {
                    $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao salvar registro.
                        </div>';
                }
            }
        } else {
            //CADASTRO
            $membro->setAtivo(1);
            $membro->setNome(filter_input(INPUT_POST, "txtNome"));
            $membro->setSexo(filter_input(INPUT_POST, "selSexo"));
            $membro->setLogradouro(filter_input(INPUT_POST, "txtLogradouro"));
            $membro->setNumero(filter_input(INPUT_POST, "txtNumero"));
            $membro->setBairro(filter_input(INPUT_POST, "txtBairro"));
            $membro->setComplemento(filter_input(INPUT_POST, "txtComplemento"));
            $membro->setCep(filter_input(INPUT_POST, "txtCep"));
            $membro->setTelefone(filter_input(INPUT_POST, "txtTelefone"));
            $membro->setCelular(filter_input(INPUT_POST, "txtCelular"));
            $membro->setEmail(filter_input(INPUT_POST, "txtEmail"));
            $membro->setNivelEscolar(filter_input(INPUT_POST, "selNivelEscolar"));
            $membro->setNascimento(filter_input(INPUT_POST, "txtNascimento"));
            $membro->setNaturalidade(filter_input(INPUT_POST, "txtNaturalidade"));
            $membro->setRg(filter_input(INPUT_POST, "txtRg"));
            $membro->setOrgao(filter_input(INPUT_POST, "selOrgao"));
            $membro->setCpf(filter_input(INPUT_POST, "txtCpf"));
            $membro->setEstadoCivil(filter_input(INPUT_POST, "selEstadoCivil"));
            $membro->setConjuge(filter_input(INPUT_POST, "txtConjuge"));
            $dataCasamento = filter_input(INPUT_POST, "txtCasamento");
            $membro->setDataCasamento(filter_input(INPUT_POST, "txtCasamento") == "" ? null : $dataCasamento);
            $dataBatismo = filter_input(INPUT_POST, "txtBatismo");
            $membro->setDataBatismo(filter_input(INPUT_POST, "txtBatismo") == "" ? null : $dataBatismo);
            $membro->setNomePai(filter_input(INPUT_POST, "txtPai"));
            $membro->setNomeMae(filter_input(INPUT_POST, "txtMae"));
            $membro->setIgrejaOrigem(filter_input(INPUT_POST, "txtOrigem"));
            $membro->igreja->setId(filter_input(INPUT_POST, "selIgreja"));
            $cargo = filter_input(INPUT_POST, "selCargo");
            $membro->cargo->setId($cargo == "0" ? null : $cargo);

            //Obtendo valores da requisição VIA CEP
            $nomeCidade = isset($_POST['txtCidade']) ? $_POST['txtCidade'] : "";
            $Uf = isset($_POST['txtUf']) ? $_POST['txtUf'] : "";
            $membro->cidade->setNome($nomeCidade);
            $membro->cidade->setUf($Uf);

            if ($membroController->Gravar($membro)) {
                $membro = new Membro();
                $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                                     </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro salvo com sucesso.
                                 </div>';
            } else {
                $resultado = '<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                                     </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao salvar registro.
                                 </div>';
            }
        }
    }
}

function Validar($membro)
{
    $listaErros = [];

    $mController = new MembroController();

    if (strlen(filter_input(INPUT_POST, "txtNome", FILTER_SANITIZE_STRING)) < 3) {
        $listaErros[] = "- Nome inválido. (min 3 caracteres)";
    }
    if (filter_input(INPUT_POST, "selIgreja") == "0") {
        $listaErros[] = "- Selecione uma Igreja.";
    }

    if(!empty($listaErros)){
        GuardarValores($membro);
    }
    return $listaErros;
}

function GuardarValores($membro)
{
    $membro->setNome(filter_input(INPUT_POST, "txtNome"));
    $membro->setSexo(filter_input(INPUT_POST, "selSexo"));
    $membro->setLogradouro(filter_input(INPUT_POST, "txtLogradouro"));
    $membro->setNumero(filter_input(INPUT_POST, "txtNumero"));
    $membro->setBairro(filter_input(INPUT_POST, "txtBairro"));
    $membro->setComplemento(filter_input(INPUT_POST, "txtComplemento"));
    $membro->setCep(filter_input(INPUT_POST, "txtCep"));
    $membro->setTelefone(filter_input(INPUT_POST, "txtTelefone"));
    $membro->setCelular(filter_input(INPUT_POST, "txtCelular"));
    $membro->setEmail(filter_input(INPUT_POST, "txtEmail"));
    $membro->setNivelEscolar(filter_input(INPUT_POST, "selNivelEscolar"));
    $membro->setNascimento(filter_input(INPUT_POST, "txtNascimento"));
    $membro->setNaturalidade(filter_input(INPUT_POST, "txtNaturalidade"));
    $membro->setRg(filter_input(INPUT_POST, "txtRg"));
    $membro->setOrgao(filter_input(INPUT_POST, "selOrgao"));
    $membro->setCpf(filter_input(INPUT_POST, "txtCpf"));
    $membro->setEstadoCivil(filter_input(INPUT_POST, "selEstadoCivil"));
    $membro->setConjuge(filter_input(INPUT_POST, "txtConjuge"));
    $membro->setDataCasamento(filter_input(INPUT_POST, "txtCasamento"));
    $membro->setDataBatismo(filter_input(INPUT_POST, "txtBatismo"));
    $membro->setNomePai(filter_input(INPUT_POST, "txtPai"));
    $membro->setNomeMae(filter_input(INPUT_POST, "txtMae"));
    $membro->setIgrejaOrigem(filter_input(INPUT_POST, "txtOrigem"));
    $membro->igreja->setId(filter_input(INPUT_POST, "selIgreja"));
    $membro->cargo->setId(filter_input(INPUT_POST, "selCargo"));
    $membro->cidade->setNome(filter_input(INPUT_POST, 'txtCidade'));
    $membro->cidade->setUf(filter_input(INPUT_POST, 'txtUf'));
}

?>


<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">Membros</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Início</a></li>
                    <li class="breadcrumb-item active">Membros</li>
                </ol>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10">
                    <i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-outline-info">
                    <div class="card-header">
                        <h4 class="m-b-0 text-white">Cadastro de Membro</h4>
                    </div>
                    <div class="card-block">
                        <form action="?page=cadastro-membro" method="post">
                            <div class="form-body">
                                <h3 class="card-title">Dados Pessoais</h3>
                                <hr>
                                <div class="row p-t-20">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="hidden" name="txtCodigo" id="txtCodigo"
                                                   value="<?= $codigo ?>"/>
                                            <label class="control-label">Nome</label>
                                            <input type="text" id="txtNome" name="txtNome" class="form-control" maxlength="50"
                                                   placeholder="Nome" required value="<?= $membro->getNome() ?>" minlength="3">
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Email</label>
                                            <input type="email" id="txtEmail" name="txtEmail" maxlength="50"
                                                   value="<?= $membro->getEmail() ?>"
                                                   class="form-control form-control-danger" placeholder="Email">
                                        </div>
                                        <!--/span-->
                                    </div>
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">CPF</label>
                                            <input type="text" class="form-control" placeholder="CPF" name="txtCpf" maxlength="15"
                                                   id="txtCpf"/ required value="<?= $membro->getCpf() ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">RG</label>
                                            <input type="text" name="txtRg" id="txtRg" class="form-control" maxlength="20"
                                                   placeholder="Documento RG" value="<?= $membro->getRg() ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group has-success">
                                            <label class="control-label">Órgão Emissor</label>
                                            <select class="form-control" name="selOrgao" id="selOrgao">
                                                <option value="SSP">SSP - Secretaria de Segurança Pública</option>
                                                <option value="COREN">COREN - Conselho Regional de Enfermagem</option>
                                                <option value="CRA">CRA - Conselho Regional de Administração</option>
                                                <option value="CRAS">CRAS - Conselho Regional de Assistentes Sociais
                                                </option>
                                                <option value="CRB">CRB - Conselho Regional de Biblioteconomia</option>
                                                <option value="CRC">CRC - Conselho Regional de Contabilidade</option>
                                                <option value="CRE">CRE - Conselho Regional de Estatística</option>
                                                <option value="CREA">CREA - Conselho Regional de Engenharia Arquitetura
                                                    e Agronomia
                                                </option>
                                                <option value="CRECI">CRECI - Conselho Regional de Corretores de
                                                    Imóveis
                                                </option>
                                                <option value="CREFIT">CREFIT - Conselho Regional de Fisioterapia e
                                                    Terapia Ocupacional
                                                </option>
                                                <option value="CRF">CRF - Conselho Regional de Farmácia</option>
                                                <option value="CRM">CRM - Conselho Regional de Medicina</option>
                                                <option value="CRN">CRN - Conselho Regional de Nutrição</option>
                                                <option value="CRO">CRO - Conselho Regional de Odontologia</option>
                                                <option value="CRP">CRP - Conselho Regional de Psicologia</option>
                                                <option value="CRPRE">CRPRE - Conselho Regional de Profissionais de
                                                    Relações Públicas
                                                </option>
                                                <option value="CRQ">CRQ - Conselho Regional de Química</option>
                                                <option value="CRRC">CRRC - Conselho Regional de Representantes
                                                    Comerciais
                                                </option>
                                                <option value="CRMV">CRMV - Conselho Regional de Medicina Veterinária
                                                </option>
                                                <option value="DPF">DPF - Polícia Federal</option>
                                                <option value="EST">EST - Documentos Estrangeiros</option>
                                                <option value="I CLA">I CLA - Carteira de Identidade Classista</option>
                                                <option value="MAE">MAE - Ministério da Aeronáutica</option>
                                                <option value="MEX">MEX - Ministério do Exército</option>
                                                <option value="MMA">MMA - Ministério da Marinha</option>
                                                <option value="OAB">OAB - Ordem dos Advogados do Brasil</option>
                                                <option value="OMB">OMB - Ordens dos Músicos do Brasil</option>
                                                <option value="IFP">IFP - Instituto de Identificação Félix Pacheco
                                                </option>
                                                <option value="OUT">OUT - Outros Emissores</option>
                                            </select>
                                        </div>
                                        <!--/span-->
                                    </div>
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Telefone</label>
                                            <input type="text" class="form-control" placeholder="Telefone"
                                                   name="txtTelefone" id="txtTelefone" data-mask="(99) 9999-9999"
                                                   value="<?= $membro->getTelefone() ?>"/>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Celular</label>
                                            <input type="text" class="form-control" placeholder="Celular"
                                                   name="txtCelular" id="txtCelular" data-mask="(99) 99999-9999"
                                                   value="<?= $membro->getCelular() ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Sexo</label>
                                            <select class="form-control" name="selSexo" id="selSexo">
                                                <option value="M" <?= ($membro->getSexo() == "M") ? 'selected' : '' ?>>
                                                    Masculino
                                                </option>
                                                <option value="F" <?= ($membro->getSexo() == "F") ? 'selected' : '' ?>>
                                                    Feminino
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Naturalidade</label>
                                            <input type="text" class="form-control" placeholder="Naturalidade"
                                                   name="txtNaturalidade" id="txtNaturalidade" maxlength="60"
                                                   value="<?= $membro->getNaturalidade() ?>"/ >
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Data de Nascimento</label>
                                            <input type="date" class="form-control" placeholder="Data de Nascimento"
                                                   name="txtNascimento" id="txtNascimento" required
                                            value="<?= $membro->getNascimento() == null ? "" : date('Y-m-d', strtotime($membro->getNascimento())) ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Data de Batismo</label>
                                            <input type="date" class="form-control" placeholder="Data de Batismo"
                                                   name="txtBatismo" id="txtBatismo"
                                                   value="<?= $membro->getDataBatismo() == null ? "" : date('Y-m-d', strtotime($membro->getDataBatismo())) ?>"/
                                            >
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Nível Escolar</label>
                                            <select name="selNivelEscolar" id="selNivelEscolar"
                                                    class="form-control" required>
                                                <option value="0">Selecione...</option>
                                                <option value="1" <?= ($membro->getNivelEscolar() == 1) ? 'selected' : '' ?>>
                                                    Ensino Fundamental (1º grau)
                                                </option>
                                                <option value="2" <?= ($membro->getNivelEscolar() == 2) ? 'selected' : '' ?>>
                                                    Ensino Medio (2º grau)
                                                </option>
                                                <option value="3" <?= ($membro->getNivelEscolar() == 3) ? 'selected' : '' ?>>
                                                    Ensino Superior
                                                </option>
                                                <option value="4" <?= ($membro->getNivelEscolar() == 4) ? 'selected' : '' ?>>
                                                    Pós-graduação
                                                </option>
                                                <option value="5" <?= ($membro->getNivelEscolar() == 5) ? 'selected' : '' ?>>
                                                    Mestrado
                                                </option>
                                                <option value="6" <?= ($membro->getNivelEscolar() == 6) ? 'selected' : '' ?>>
                                                    Doutorado
                                                </option>
                                                <option value="7" <?= ($membro->getNivelEscolar() == 7) ? 'selected' : '' ?>>
                                                    Pós-doutorado
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Estado Civil</label>
                                            <select name="selEstadoCivil" id="selEstadoCivil"
                                                    class="form-control" required>
                                                <option value="0">Selecione...</option>
                                                <option value="1" <?= ($membro->getEstadoCivil() == 1) ? 'selected' : '' ?>>
                                                    Solteiro (a)
                                                </option>
                                                <option value="2" <?= ($membro->getEstadoCivil() == 2) ? 'selected' : '' ?>>
                                                    Casado (a)
                                                </option>
                                                <option value="3" <?= ($membro->getEstadoCivil() == 3) ? 'selected' : '' ?>>
                                                    Separado (a)
                                                </option>
                                                <option value="4" <?= ($membro->getEstadoCivil() == 4) ? 'selected' : '' ?>>
                                                    Divorciado (a)
                                                </option>
                                                <option value="5" <?= ($membro->getEstadoCivil() == 5) ? 'selected' : '' ?>>
                                                    Viúvo (a)
                                                </option>
                                                <option value="6" <?= ($membro->getEstadoCivil() == 6) ? 'selected' : '' ?>>
                                                    Amasiado (a)
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Cônjuge</label>
                                            <input type="text" class="form-control" placeholder="Cônjuge" maxlength="60"
                                                   name="txtConjuge" id="txtConjuge"/
                                            value="<?= $membro->getConjuge() ?>" >
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Data de Casamento</label>
                                            <input type="date" class="form-control" placeholder="Data de Casamento"
                                                   name="txtCasamento" id="txtCasamento"
                                                   value="<?= $membro->getDataCasamento() == null ? "" : date('Y-m-d', strtotime($membro->getDataCasamento())) ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Nome do Pai</label>
                                            <input type="text" id="txtPai" name="txtPai" class="form-control" maxlength="60"
                                                   placeholder="Nome do Pai" value="<?= $membro->getNomePai() ?>">
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Nome da Mãe</label>
                                            <input type="text" id="txtMae" name="txtMae"
                                                   class="form-control form-control-danger" placeholder="Nome da Mãe" maxlength="60"
                                                   value="<?= $membro->getNomeMae() ?>">
                                        </div>
                                        <!--/span-->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Igreja de Origem</label>
                                            <input type="text" id="txtOrigem" name="txtOrigem"
                                                   class="form-control form-control-danger"
                                                   placeholder="Igreja de Origem" maxlength="50"
                                                   value="<?= $membro->getIgrejaOrigem() ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Cargo</label>
                                            <select class="form-control" id="selCargo" name="selCargo">
                                                <option value="0">Não Possui</option>
                                                <?php $listaCargos = $cargoController->ObterCargosSelect();
                                                foreach ($listaCargos as $cargo) { ?>
                                                    <option value="<?= $cargo->getId() ?>" <?php echo $membro->cargo->getId() == $cargo->getId() ? 'selected' : '' ?>>
                                                        <?= $cargo->getNome() ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Membro da Igreja</label>
                                            <select class="form-control" id="selIgreja" name="selIgreja" required>
                                                <option value="0">Selecione a Igreja...</option>
                                                <?php $listaIgrejas = $igrejaController->ObterIgrejasSelect();
                                                foreach ($listaIgrejas as $igreja) { ?>
                                                    <option value="<?= $igreja->getId() ?>" <?php echo $membro->igreja->getId() == $igreja->getId() ? 'selected' : '' ?>>
                                                        <?= $igreja->getNome() ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <h3 class="box-title m-t-40">Endereço</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>CEP</label>
                                            <input type="text" data-mask="99999-999" class="form-control" name="txtCep"
                                                   id="txtCep" required value="<?= $membro->getCep() ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>Av/Rua</label>
                                            <input type="text" class="form-control" name="txtLogradouro" maxlength="80"
                                                   id="txtLogradouro" required value="<?= $membro->getLogradouro()?>" minlength="5">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Número</label>
                                            <input type="text" class="form-control" name="txtNumero" id="txtNumero" maxlength="12"
                                                   required value="<?= $membro->getNumero() ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Complemento</label>
                                            <input type="text" class="form-control" name="txtComplemento" maxlength="40"
                                                   id="txtComplemento" value="<?= $membro->getComplemento() ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Bairro</label>
                                            <input type="text" class="form-control" name="txtBairro" id="txtBairro" maxlength="40"
                                                   required value="<?= $membro->getBairro() ?>" minlength="3">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Cidade</label>
                                            <input type="text" class="form-control" name="txtCidade" id="txtCidade"
                                                   required readonly value="<?= $membro->cidade->getNome() ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>UF</label>
                                            <input type="text" class="form-control" name="txtUf" id="txtUf"
                                                   required readonly value="<?= $membro->cidade->getUf() ?>">
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            </div>
                            <div class="form-actions">
                                <input type="submit" class="btn btn-success"
                                       style="margin-top: 32px; <?= $alterando ? 'display:none' : "" ?>"
                                       name="btnSalvar" id="btnSalvar" value="SALVAR"/>
                                <input type="submit" class="btn btn-success"
                                       style="margin-top: 32px; <?= $alterando ? "" : 'display:none' ?>"
                                       name="btnEditar" id="btnEditar" value="ALTERAR"/>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <br/>
                                    <?php echo $resultado ?>
                                </div>
                            </div>
                            <?php if (!empty($erros)) { ?>
                                <div class="form-group">
                                    <div class="col-xs-12" style="border: solid 2px red">
                                        <ul style="list-style: none;">
                                            <?php
                                            foreach ($erros as $e) {
                                                ?>
                                                <li><?= $e; ?></li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <?php include "View/Template/service-panel.php"; ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <?php include "View/Template/footer.php"; ?>
    <script type="text/javascript" src="View/scripts/via-cep.js"></script>
    <script type="text/javascript" src="js/mask.js"></script>
