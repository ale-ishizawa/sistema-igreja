<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 13/05/2018
 * Time: 14:35
 */
require_once './_autoload.php';
include "template/header.php";
include "template/menu.php";
session_start();

//use Controller\MembroController;

$idIgrejaSessao = isset($_SESSION['idigreja']) ? $_SESSION['idigreja'] : null;
//$membroController = new MembroController();


?>
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-6 col-8 align-self-center">
                    <h3 class="text-themecolor m-b-0 m-t-0">Página Inicial</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Início</li>
                    </ol>
                </div>
            </div>

            <!-- Start Page Content -->

            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <?php include "View/Template/service-panel.php"; ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->

<?php include "View/Template/footer.php"; ?>