<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 10/06/2018
 * Time: 20:48
 */
require_once './_autoload.php';
require_once './vendor/autoload.php';
ob_start();  // start output buffering

use Controller\DizimoController;
session_start();

$idIgrejaSessao = isset($_SESSION['idigreja']) ? $_SESSION['idigreja'] : null;
$inicio = isset($_POST['txtInicio']) ? $_POST['txtInicio'] : null;
$fim = isset($_POST['txtFim']) ? $_POST['txtFim'] : null;

$dizimoController = new DizimoController();
$html = $dizimoController->RelatorioDizimos($idIgrejaSessao, $inicio, $fim);

$mpdf = new \Mpdf\Mpdf([
    'margin_left' => 0,
    'margin_right' => 0,
    'margin_top' => 30,
    'margin_header' => 0,
    'margin_footer' => 0
]);

$mpdf->SetHTMLHeader('
                        <table width="100%" style=" background-color: #f5f5f5; padding: 20px 40px 20px; border: 2px">
                            <tr>
                                <td width="50%"><img src="assets/images/logo-ibrf.png" width="150px" height="100px"/> </td>
                                <td width="50%" style="text-align: right; font-size: 12px; line-height: 15px">Igreja Batista em Regente Feijó <br/>
                                   Telefone: (18) 3279-2488 <br/>
                                   Rua Júlio Mesquita, 421 - Centro<br/>
                                   Regente Feijó, SP
                                </td>
                            </tr>
                        </table>');
$mpdf->AddPage('P');
$footer = file_get_contents('View/Template/pdf/footer.phtml');

$mpdf->SetHTMLFooter($footer);

$mpdf->WriteHTML($html);
//    echo $html;

return $mpdf->Output('relatorio-dizimos.pdf', \Mpdf\Output\Destination::DOWNLOAD);


