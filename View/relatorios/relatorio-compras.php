<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 10/06/2018
 * Time: 18:40
 */

require_once './_autoload.php';
include "View/Template/header.php";
include "View/Template/menu.php";

use Controller\CompraController;
use Model\Compra;


//session_start();
$compraController = new CompraController();
$compra = new Compra();
$idIgrejaSessao = isset($_SESSION['idigreja']) ? $_SESSION['idigreja'] : null;

$resultado = "";
$erros = [];

?>

<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">Relatório de Compras</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Início</a></li>
                    <li class="breadcrumb-item active">Relatório de Compras</li>
                </ol>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10">
                    <i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">Relatório de Compras</h4>
                        <h6 class="card-subtitle">Informe o período que deseja consultar.</h6>
                        <form method="post" action="?page=gera-pdf-relatorio-compras">
                            <div class="row p-t-20">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Data Início</label>
                                        <input type="date" id="txtInicio" name="txtInicio" class="form-control" required>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Data Fim</label>
                                        <input type="date" id="txtFim" name="txtFim"
                                               class="form-control form-control-danger">
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-success col-md-12"
                                               style="margin-top: 32px;"
                                               name="btnGerarPdf" id="btnGerarPdf" value="GERAR PDF"/>
                                    </div>
                                    <!--/span-->
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <br/>
                                    <?php echo $resultado ?>
                                </div>
                            </div>
                            <?php if (!empty($erros)) { ?>
                                <div class="form-group">
                                    <div class="col-xs-12" style="border: solid 2px red">
                                        <ul style="list-style: none;">
                                            <?php
                                            foreach ($erros as $e) {
                                                ?>
                                                <li><?= $e; ?></li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <?php include "View/Template/service-panel.php"; ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <?php include "View/Template/footer.php"; ?>


