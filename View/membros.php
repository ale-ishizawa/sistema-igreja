<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 25/05/2018
 * Time: 10:08
 */
require_once './_autoload.php';
include "View/Template/header.php";
include "View/Template/menu.php";

session_start();

use Model\Membro;
use Controller\MembroController;

//Obtém o id da igreja relacionado ao usuário logado
$idIgrejaSessao = isset($_SESSION['idigreja']) ? $_SESSION['idigreja'] : null;
$membroController = new MembroController();

$resultado = "";
$erros = [];

$codExcluir = filter_input(INPUT_GET, 'del');
//Exclusão de Membro da igreja
if ($codExcluir != NULL) {
    if ($membroController->Inativar($codExcluir)) {
        $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro excluído com sucesso.
                        </div>';
    } else {
        $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao excluir registro.
                        </div>';
    }
}


?>

<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">Membros</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Início</a></li>
                    <li class="breadcrumb-item active">Membros</li>
                </ol>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10">
                    <i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">Membros Cadastrados</h4>
                        <!--                        <h6 class="card-subtitle">Membros</h6>-->
                        <div class="table-responsive m-t-40" style="min-height: 350px;">
                            <?php $membros = $membroController->ObterMembros($idIgrejaSessao);
                            if (count($membros) > 0) { ?>
                                <table id="myTable" class="table table-bordered table-striped" style="font-size: 14px;">
                                    <thead>
                                    <tr>
                                        <th>Ações</th>
                                        <th>Nome</th>
                                        <th>Celular</th>
                                        <th>Email</th>
                                        <th>Cargo</th>
                                        <th style="max-width: 50px;">Data de Nascimento</th>
                                        <th style="max-width: 50px;">Data de Batismo</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($membros as $mem) { ?>
                                        <tr>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-danger dropdown-toggle"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        <i class="ti-settings"></i>
                                                    </button>
                                                    <div class="dropdown-menu animated flipInX">
                                                        <a class="dropdown-item"
                                                           href="?page=cadastro-membro&editar=<?= $mem->getId() ?>">Editar</a>
                                                        <a class="dropdown-item"
                                                           href="?page=membros&del=<?= $mem->getId() ?>"
                                                           onclick="return confirm('Deseja excluir o membro <?= $mem->getNome() ?>?');">Excluir
                                                            Membro</a>
                                                        <a class="dropdown-item"
                                                           href="?page=perfil-membro&editar=<?= $mem->getId() ?>">Visualizar
                                                            Perfil</a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><?= $mem->getNome() ?></td>
                                            <td><?= $mem->getCelular() ?></td>
                                            <td><?= $mem->getEmail() ?></td>
                                            <td><?= $mem->cargo->getNome() ?></td>
                                            <td><?= date('d/m/Y', strtotime($mem->getNascimento())) ?></td>
                                            <td><?= date('d/m/Y', strtotime($mem->getDataBatismo())) ?></td>

                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            <?php } else { ?>
                                <div class="alert alert-warning">Não há membros cadastrados no sistema.</div>
                            <?php } ?>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <br/>
                                <?php echo $resultado ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <?php include "View/Template/service-panel.php"; ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

    <?php include "View/Template/footer.php"; ?>
    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            var aux = [{ "bSortable": false }, { "bSortable": true }, { "bSortable": true }, { "bSortable": true }, { "bSortable": true }, { "bSortable": true }, { "bSortable": true }];
            $('#myTable').DataTable({
                "pageLength": 10,
                "language": {
                    "lengthMenu": "EXIBIR _MENU_",
                    "zeroRecords": "Registros não encontrados",
                    "info": "Página _PAGE_ de _PAGES_",
                    "infoEmpty": "Registro não encontrado",
                    "infoFiltered": "(Um total de _MAX_ registros)",
                    "sSearch": "PESQUISAR: ",
                    "oPaginate": {
                        "sFirst": "Início",
                        "sPrevious": "Anterior",
                        "sNext": "Próximo",
                        "sLast": "Último"
                    }
                },
                "aoColumns": aux
            });
            getCss();
        });

        /**
         * Estilo para o css do datatable
         */

        function getCss()
        {
            $('label select').css('margin-left', '10px');
            $('#myTable_filter label').css('text-transform', 'uppercase');
            $('#myTable_length label').css('text-transform', 'uppercase');
            $('#myTable_length label').css('font-weight', 'bold');
            $('#myTable_length label').css('font-size', '0.9em');
            $('#myTable_length label').css('color', '#777171');
            $('#myTable_filter label').css('font-weight', 'bold');
            $('#myTable_filter label').css('font-size', '0.9em');
            $('#myTable_filter label').css('color', '#777171');
            $('input[type=search]').css('padding', '5px 5px');
            $('input[type=search]').css('width', '165px');
            $('#myTable_filter').css('margin-top', '25px');
            $('#myTable_length').css('margin-top', '25px');
        }

    </script>