<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 23:42
 */
require_once './_autoload.php';
include "View/Template/header.php";
include "View/Template/menu.php";

use Model\Cargo;
use Controller\CargoController;

$cargo = new Cargo();
$cargoController = new CargoController();

$resultado = "";
$erros = [];
$codigo = "";
$alterando = false;

//UPDATE
$codEditar = filter_input(INPUT_GET, 'editar');
if ($codEditar != NULL) {
    $c = $cargoController->ObterCargoPorCod($codEditar);
    if ($c != NULL) {
        $codigo = $codEditar;
        $cargo->setId($c->getId());
        $cargo->setDescricao($c->getDescricao());
        $cargo->setNome($c->getNome());
        $alterando = true;
    }
}

//DELETE
$codExcluir = filter_input(INPUT_GET, 'del');
if ($codExcluir != NULL) {
    if ($cargoController->Deletar($codExcluir)) {
        $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro excluído com sucesso.
                        </div>';
    } else {
        $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao excluir registro.
                        </div>';
    }
}

$btnSalvar = filter_input(INPUT_POST, 'btnSalvar') != NULL;
$btnEditar = filter_input(INPUT_POST, 'btnEditar') != NULL;

if ($btnSalvar || $btnEditar) {
    $erros = Validar();

    //Se não existir erros de validação, envia os dados para a Controladora
    if (empty($erros)) {
        if ($btnEditar) {
            $codigo = filter_input(INPUT_POST, "txtCodigo", FILTER_VALIDATE_INT);
            if ($codigo === false) {
                $erros[] = "- Código não informado corretamente.";
            }
            if (empty($erros)) {
                //EDIÇÃO
                $cargo->setId($codigo);
                $cargo->setNome(filter_input(INPUT_POST, 'txtNome'));
                $cargo->setDescricao(filter_input(INPUT_POST, 'txtDescricao'));

                if ($cargoController->Editar($cargo)) {
                    $cargo = new Cargo();
                    $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro salvo com sucesso.
                        </div>';
                } else {
                    $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao salvar registro.
                        </div>';
                }
            }
        } else {
            //CADASTRO
            $cargo->setNome(filter_input(INPUT_POST, 'txtNome'));
            $cargo->setDescricao(filter_input(INPUT_POST, 'txtDescricao'));

            if ($cargoController->Gravar($cargo)) {
                $cargo = new Cargo();
                $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro salvo com sucesso.
                        </div>';
            } else {
                $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao salvar registro.
                        </div>';
            }
        }
    }
}

function Validar()
{
    $listaErros = [];

    if (strlen(filter_input(INPUT_POST, 'txtNome', FILTER_SANITIZE_STRING)) < 3) {
        $listaErros[] = "- Nome do Cargo inválido. (mínimo 3 caracteres)";
    }

    return $listaErros;
}

?>

<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">Cargos</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Início</a></li>
                    <li class="breadcrumb-item active">Cargos</li>
                </ol>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10">
                    <i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->

        <!-- Row -->
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">Cadastrar Cargo</h4>
                        <form method="post" action="?page=cargos">
                            <input type="hidden" name="txtCodigo" id="txtCodigo" value="<?= $codigo ?>"/>
                            <div class="form-group">
                                <label class="control-label">Nome</label>
                                <input type="text" id="txtNome" name="txtNome" class="form-control"
                                       placeholder="Nome do Cargo" required value="<?= $cargo->getNome() ?>">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Descrição do Cargo</label>
                                <input type="text" id="txtDescricao" name="txtDescricao" class="form-control"
                                       placeholder="Descrição do Cargo" required
                                       value="<?= $cargo->getDescricao() ?>">
                            </div>
                            <div class="form-actions">
                                <input type="submit" class="btn btn-success"
                                       style="margin-top: 32px; <?= $alterando ? 'display:none' : "" ?>"
                                       name="btnSalvar" id="btnSalvar" value="SALVAR"/>
                                <input type="submit" class="btn btn-success"
                                       style="margin-top: 32px; <?= $alterando ? "" : 'display:none' ?>"
                                       name="btnEditar" id="btnEditar" value="ALTERAR"/>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <br/>
                                    <?php echo $resultado ?>
                                </div>
                            </div>
                            <?php if (!empty($erros)) { ?>
                                <div class="form-group">
                                    <div class="col-xs-12" style="border: solid 2px red">
                                        <ul style="list-style: none;">
                                            <?php
                                            foreach ($erros as $e) {
                                                ?>
                                                <li><?= $e; ?></li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">Cargos na Igreja</h4>
                        <div class="table-responsive m-t-40" style="min-height: 300px;">
                            <?php $cargos = $cargoController->ObterCargos();
                            if (count($cargos) > 0) { ?>
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Ação</th>
                                        <th>Nome</th>
                                        <th>Descrição</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($cargos as $car) { ?>
                                        <tr>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-danger dropdown-toggle"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        <i class="ti-settings"></i>
                                                    </button>
                                                    <div class="dropdown-menu animated flipInX">
                                                        <a class="dropdown-item"
                                                           href="?page=cargos&editar=<?= $car->getId() ?>">Editar</a>
                                                        <a class="dropdown-item"
                                                           href="?page=cargos&del=<?= $car->getId() ?>"
                                                           onclick="return confirm('Deseja excluir o cargo <?= $car->getNome() ?>?');">Excluir</a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><?= $car->getNome() ?></td>
                                            <td><?= $car->getDescricao() ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            <?php } else { ?>
                                <div class="alert alert-warning">Não há cargos cadastrados no sistema.</div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <?php include "View/Template/service-panel.php"; ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <?php include "View/Template/footer.php"; ?>

