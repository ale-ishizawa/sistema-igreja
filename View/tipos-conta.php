<?php
require_once './_autoload.php';
include "View/Template/header.php";
include "View/Template/menu.php";

use Model\TipoConta;
use Controller\TipoContaController;

$tipoController = new TipoContaController();
$tipoConta = new TipoConta();

$resultado = "";
$erros = [];
$codigo = "";
$alterando = false;

//DELETE
$codExcluir = filter_input(INPUT_GET, 'del');
if($codExcluir != NULL){
    if($tipoController->Deletar($codExcluir)){
        $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro excluído com sucesso.
                        </div>';
    } else {
        $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao excluir registro.
                        </div>';
    }
}

//UPDATE
$codEditar = filter_input(INPUT_GET, 'editar');
if($codEditar != NULL){
    $tc = $tipoController->ObterTipoContaPorCod($codEditar);
    if($tc != NULL){
        $codigo = $codEditar;
        $tipoConta->setId($tc->getId());
        $tipoConta->setDescricao($tc->getDescricao());
        $alterando = true;
    }
}

$btnSalvar = filter_input(INPUT_POST, 'btnSalvar') != NULL;
$btnEditar = filter_input(INPUT_POST, 'btnEditar') != NULL;

if($btnSalvar || $btnEditar){
    $erros = Validar();

    //Se não existir erros de validação, envia os dados para a Controladora
    if(empty($erros)){
        if($btnEditar){
            //EDIÇÃO
            $codigo = filter_input(INPUT_POST, "txtCodigo", FILTER_VALIDATE_INT);
            if ($codigo === false) {
                $erros[] = "- Código não informado corretamente.";
            }
            if(empty($erros)){
                $tipoConta->setId($codigo);
                $tipoConta->setDescricao(filter_input(INPUT_POST, 'txtDescricao'));

                if($tipoController->Editar($tipoConta)){
                    $tipoConta = new TipoConta();
                    $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro salvo com sucesso.
                        </div>';
                } else {
                    $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao salvar registro.
                        </div>';
                }
            }
        }else{
            //CADASTRO
            $descricao = filter_input(INPUT_POST, 'txtDescricao', FILTER_SANITIZE_STRING);

            $tipoConta->setDescricao($descricao);
            $tipoConta->setAtivo(1);

            if($tipoController->Gravar($tipoConta)){
                $tipoConta = new TipoConta();
                $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro salvo com sucesso.
                        </div>';
            }else{
                $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao salvar registro.
                        </div>';
            }
        }
    }
}

function Validar(){
    $listaErros = [];

    if(strlen(filter_input(INPUT_POST, 'txtDescricao', FILTER_SANITIZE_STRING)) < 3){
        $listaErros[] = "- Tipo de conta inválido. (mínimo 3 caracteres)";
    }

    return $listaErros;
}


?>

<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">Tipos de Conta</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Início</a></li>
                    <li class="breadcrumb-item active">Tipos de Conta</li>
                </ol>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

        <!-- Row -->
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">Cadastrar Tipo de Conta</h4>
                        <form method="post" action="?page=tipo-conta">
                            <input type="hidden" name="txtCodigo" id="txtCodigo" value="<?= $codigo ?>"/>
                            <div class="form-group">
                                <label class="control-label">Descrição</label>
                                <input type="text" id="txtDescricao" name="txtDescricao" class="form-control" minlength="3"
                                       placeholder="Descrição do tipo de conta" required value="<?= $tipoConta->getDescricao() ?>">
                            </div>
                            <div class="form-actions">
                                <input type="submit" class="btn btn-success"
                                       style="margin-top: 32px; <?= $alterando ? 'display:none' : "" ?>"
                                       name="btnSalvar" id="btnSalvar" value="SALVAR"/>
                                <input type="submit" class="btn btn-success"
                                       style="margin-top: 32px; <?= $alterando ? "" : 'display:none' ?>"
                                       name="btnEditar" id="btnEditar" value="ALTERAR"/>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <br/>
                                    <?php echo $resultado ?>
                                </div>
                            </div>
                            <?php if (!empty($erros)) { ?>
                                <div class="form-group">
                                    <div class="col-xs-12" style="border: solid 2px red">
                                        <ul style="list-style: none;">
                                            <?php
                                            foreach ($erros as $e) {
                                                ?>
                                                <li><?= $e; ?></li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">Tipos de Contas</h4>
                        <div class="table-responsive m-t-40" style="min-height: 180px">
                            <?php $tipos = $tipoController->ObterTiposConta();
                            if (count($tipos) > 0) { ?>
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Ações</th>
                                        <th>Descrição</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($tipos as $tipo){ ?>
                                        <tr>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-danger dropdown-toggle"
                                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="ti-settings"></i>
                                                    </button>
                                                    <div class="dropdown-menu animated flipInX">
                                                        <a class="dropdown-item"
                                                           href="?page=tipo-conta&editar=<?= $tipo->getId() ?>">Editar</a>
                                                        <a class="dropdown-item" href="?page=tipo-conta&del=<?= $tipo->getId() ?>"
                                                           onclick="return confirm('Deseja excluir o tipo de conta <?= $tipo->getDescricao() ?>?');">Excluir</a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><?= $tipo->getDescricao() ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            <?php } else { ?>
                                <div class="alert alert-warning">Não há tipos de conta cadastrados no sistema.</div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <?php include "View/Template/service-panel.php"; ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <?php include "View/Template/footer.php"; ?>

