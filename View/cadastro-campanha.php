<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 29/05/2018
 * Time: 00:27
 */
require_once './_autoload.php';
include "View/Template/header.php";
include "View/Template/menu.php";

use Model\Campanha;
use Model\Igreja;
use Controller\CampanhaController;
use Controller\IgrejaController;

session_start();

$campanha = new Campanha();
$igreja = new Igreja();
$campanhaController = new CampanhaController();
$igrejaController = new IgrejaController();

//Obtém o id da igreja relacionado ao usuário logado
$idIgrejaSessao = isset($_SESSION['idigreja']) ? $_SESSION['idigreja'] : null;

$resultado = "";
$erros = [];
$alterando = false;
$codigo = "";

//Editar
$codEditar = filter_input(INPUT_GET, 'editar');
if ($codEditar != NULL) {
    $cam = $campanhaController->ObterCampanha($codEditar);
    if ($cam != NULL) {
        $codigo = $codEditar;
        $campanha->setId($cam->getId());
        $campanha->setAtivo($cam->getAtivo());
        $campanha->setDataInicio($cam->getDataInicio());
        $campanha->setDataFim($cam->getDataFim());
        $campanha->setDescricao($cam->getDescricao());
        $campanha->setNome($cam->getNome());
        $campanha->setValorAlvo($cam->getValorAlvo());
        $campanha->igreja->setId($cam->igreja->getId());
        $alterando = true;
    }
}

$btnSalvar = filter_input(INPUT_POST, 'btnSalvar') != NULL;
$btnEditar = filter_input(INPUT_POST, 'btnEditar') != NULL;

if($btnSalvar || $btnEditar) {
//    $erros = Validar();
    if($btnEditar){
        //EDIÇÃO
        $codigo = filter_input(INPUT_POST, "txtCodigo", FILTER_VALIDATE_INT);
        if ($codigo === false) {
            $erros[] = "- Código não informado corretamente.";
        }
        if(empty($erros)){
            $campanha->setId($codigo);
            $campanha->setDataInicio(filter_input(INPUT_POST, 'txtInicio'));
            $campanha->setDataFim(filter_input(INPUT_POST, 'txtFim'));
            $valor = filter_input(INPUT_POST, 'txtAlvo', FILTER_SANITIZE_STRING);
            $valor = substr($valor, 3);
            if(strstr($valor, ','))
                $valor = str_replace(',','', $valor);

            $campanha->setValorAlvo($valor);
            $campanha->setNome(filter_input(INPUT_POST, 'txtNome'));
            $campanha->setDescricao(filter_input(INPUT_POST, 'txtDescricao'));
            if($idIgrejaSessao == null){
                $campanha->igreja->setId(filter_input(INPUT_POST, 'selIgreja'));
            }else{
                $campanha->igreja->setId($idIgrejaSessao);
            }

            if($campanhaController->Editar($campanha)){
                $campanha = new Campanha();
                $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro salvo com sucesso.
                        </div>';
            } else {
                $alterando = true;
                echo $codigo;
                $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao salvar registro.
                        </div>';
            }
        }
    }else{
        //CADASTRO
        $campanha->setDataInicio(filter_input(INPUT_POST, 'txtInicio'));
        $campanha->setDataFim(filter_input(INPUT_POST, 'txtFim'));
        $valor = filter_input(INPUT_POST, 'txtAlvo', FILTER_SANITIZE_STRING);
        $valor = substr($valor, 3);
        if(strstr($valor, ','))
            $valor = str_replace(',','', $valor);

        $campanha->setValorAlvo($valor);
        $campanha->setNome(filter_input(INPUT_POST, 'txtNome'));
        $campanha->setDescricao(filter_input(INPUT_POST, 'txtDescricao'));
        if($idIgrejaSessao == null){
            $campanha->igreja->setId(filter_input(INPUT_POST, 'selIgreja'));
        }else{
            $campanha->igreja->setId($idIgrejaSessao);
        }

        if($campanhaController->Gravar($campanha)){
            $campanha = new Campanha();
            $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro salvo com sucesso.
                        </div>';
        } else {
            $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao salvar registro.
                        </div>';
        }
    }
}

?>

<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">Cadastro de Campanha</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="?page=dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item">Campanhas</li>
                    <li class="breadcrumb-item active">Cadastro de Campanha</li>
                </ol>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10">
                    <i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-outline-info">
                    <div class="card-block">
                        <h4 class="card-title">Cadastrar Campanha</h4>
                        <form method="post" action="?page=cadastro-campanha">
                            <input type="hidden" name="txtCodigo" id="txtCodigo" value="<?= $codigo ?>"/>
                            <div class="form-body">
                                <div class="row p-t-20">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Nome</label>
                                            <input type="text" id="txtNome" name="txtNome" class="form-control"
                                                   placeholder="Nome" required minlength="3" value="<?= $campanha->getNome() ?>" />
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Descricao</label>
                                            <input type="text" id="txtDescricao" name="txtDescricao" maxlength="255"
                                                   class="form-control form-control-danger" placeholder="Descrição da campanha" value="<?= $campanha->getDescricao() ?>"/>
                                        </div>
                                        <!--/span-->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Data Início</label>
                                            <input type="date" id="txtInicio" name="txtInicio" class="form-control"
                                                   placeholder="Início" required value="<?= $campanha->getDataInicio() == null ? "" : date('Y-m-d', strtotime($campanha->getDataInicio())) ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Data Fim</label>
                                            <input type="date" id="txtFim" name="txtFim" class="form-control"
                                                   placeholder="Fim" value="<?= $campanha->getDataFim() == null ? "" : date('Y-m-d', strtotime($campanha->getDataFim())) ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Valor Alvo</label>
                                            <input type="text" id="txtAlvo" name="txtAlvo"
                                                   class="form-control" placeholder="Valor Alvo" value="<?= $campanha->getValorAlvo() ?>" data-affixes-stay="true" data-prefix="R$ " data-thousands="," data-decimal=".">
                                        </div>
                                    </div>
                                    <?php if ($idIgrejaSessao == "") { ?>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Igreja</label>
                                                <select class="form-control" id="selIgreja" name="selIgreja">
                                                    <option value="0">Selecione a Igreja...</option>
                                                    <?php $listaIgrejas = $igrejaController->ObterIgrejasSelect();
                                                    foreach ($listaIgrejas as $igreja) { ?>
                                                        <option value="<?= $igreja->getId() ?>" <?php echo $campanha->igreja->getId() == $igreja->getId() ? 'selected' : '' ?>>
                                                            <?= $igreja->getNome() ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" class="btn btn-success"
                                       style="margin-top: 32px; <?= $alterando ? 'display:none' : "" ?>"
                                       name="btnSalvar" id="btnSalvar" value="SALVAR"/>
                                <input type="submit" class="btn btn-success"
                                       style="margin-top: 32px; <?= $alterando ? "" : 'display:none' ?>"
                                       name="btnEditar" id="btnEditar" value="ALTERAR"/>
                            </div>
                            <br/>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <?php echo $resultado ?>
                                </div>
                            </div>
                            <?php if (!empty($erros)) { ?>
                                <div class="form-group">
                                    <div class="col-xs-12" style="border: solid 2px red">
                                        <ul style="list-style: none;">
                                            <?php
                                            foreach ($erros as $e) {
                                                ?>
                                                <li><?= $e; ?></li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <?php include "View/Template/service-panel.php"; ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <?php include "View/Template/footer.php"; ?>
    <script type="text/javascript" src="js/mask.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
    <script>
        $(function() {
            $('#txtAlvo').maskMoney();
        })
    </script>
