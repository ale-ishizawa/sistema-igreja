<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 03/06/2018
 * Time: 19:07
 */
require_once './_autoload.php';
include "View/Template/header.php";
include "View/Template/menu.php";

use Controller\CaixaController;
use Controller\ContaPagarController;
use Controller\TipoContaController;
use Model\ContaPagar;

session_start();

$conta = new ContaPagar();
$contaController = new ContaPagarController();
$tipoDeContaController = new TipoContaController();
$idIgrejaSessao = isset($_SESSION['idigreja']) ? $_SESSION['idigreja'] : null;

$resultado = "";
$erros = [];
$codigo = "";
$alterando = false;

$idCaixa = VerificaCaixa($idIgrejaSessao);
if ($idCaixa == 0) {
    echo "<script>window.location.href = '?page=caixa' </script>";
} else {

    //PAGAMENTO DE CONTA
    $codBaixar = filter_input(INPUT_GET, 'baixar');
    if ($codBaixar != NULL) {
        if ($contaController->BaixarConta($codBaixar, $idCaixa)) {
            $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Pagamento de conta efetuado com sucesso.
                        </div>';
        } else {
            $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao baixar uma conta.
                        </div>';
        }
    }

    //UPDATE
    $codEditar = filter_input(INPUT_GET, 'editar');
    if ($codEditar != NULL) {
        $c = $contaController->ObterContaPorCod($codEditar);
        if ($c != NULL) {
            $codigo = $codEditar;
            $conta->setId($c->getId());
            $conta->setDataVencimento($c->getDataVencimento());
            $conta->setAtivo($c->getAtivo());
            $conta->setValor($c->getValor());
            $conta->tipoConta->setId($c->tipoConta->getId());
            $alterando = true;
        }
    }

//DELETE
    $codExcluir = filter_input(INPUT_GET, 'del');
    if ($codExcluir != NULL) {
        if ($contaController->Deletar($codExcluir)) {
            $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro excluído com sucesso.
                        </div>';
        } else {
            $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao excluir registro.
                        </div>';
        }
    }

    $btnSalvar = filter_input(INPUT_POST, 'btnSalvar') != NULL;
    $btnEditar = filter_input(INPUT_POST, 'btnEditar') != NULL;

    if ($btnSalvar || $btnEditar) {
        $erros = Validar();

        //Se não existir erros de validação, envia os dados para a Controladora
        if (empty($erros)) {
            if ($btnEditar) {
                $codigo = filter_input(INPUT_POST, "txtCodigo", FILTER_VALIDATE_INT);
                if ($codigo === false) {
                    $erros[] = "- Código não informado corretamente.";
                }
                if (empty($erros)) {
                    //EDIÇÃO
                    $conta->setId($codigo);

                    $valor = filter_input(INPUT_POST, 'txtValor', FILTER_SANITIZE_STRING);
                    $valor = substr($valor, 3);
                    if (strstr($valor, ','))
                        $valor = str_replace(',', '', $valor);
                    $conta->setValor($valor);

                    $conta->setDataVencimento(filter_input(INPUT_POST, 'txtVencimento', FILTER_SANITIZE_STRING));
                    $conta->tipoConta->setId(filter_input(INPUT_POST, 'selTipoConta', FILTER_SANITIZE_STRING));

                    if ($contaController->Editar($conta)) {
                        $conta = new ContaPagar();
                        $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro salvo com sucesso.
                        </div>';
                    } else {
                        $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                          </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao salvar registro.
                        </div>';
                    }
                }
            } else {
                //Inserção de Novo Registro

                $valor = filter_input(INPUT_POST, 'txtValor', FILTER_SANITIZE_STRING);
                $valor = substr($valor, 3);
                if (strstr($valor, ','))
                    $valor = str_replace(',', '', $valor);

                $conta->setValor($valor);
                $conta->setAtivo(1);
                $conta->setDataVencimento(filter_input(INPUT_POST, 'txtVencimento', FILTER_SANITIZE_STRING));
                $conta->igreja->setId($idIgrejaSessao);
                $conta->tipoConta->setId(filter_input(INPUT_POST, 'selTipoConta', FILTER_SANITIZE_STRING));

                if ($contaController->Gravar($conta)) {

                    $conta = new ContaPagar();
                    $resultado = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                                     </button> <h3 class="text-success"><i class="fa fa-check-circle"></i> Sucesso!</h3> Registro salvo com sucesso.
                                 </div>';
                } else {
                    $resultado = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span>
                                     </button> <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Falha! </h3> Falha ao salvar registro.
                                 </div>';
                }
            }
        }
    }
}


function Validar()
{
    $listaErros = [];

    if (filter_input(INPUT_POST, 'selTipoConta', FILTER_SANITIZE_STRING) == "0") {
        $listaErros[] = "- Selecione um tipo de conta.";
    }
    if (filter_input(INPUT_POST, 'txtValor', FILTER_SANITIZE_STRING) == "") {
        $listaErros[] = "- Informe o Valor da conta.";
    }
    if (filter_input(INPUT_POST, 'txtVencimento', FILTER_SANITIZE_STRING) == "") {
        $listaErros[] = "- Informe a Data da compra.";
    }

    return $listaErros;
}

function VerificaCaixa($idIgrejaSessao)
{
    $caixaController = new CaixaController();
    $statusCaixa = $caixaController->VerificaStatusCaixa($idIgrejaSessao);
    if ($statusCaixa == false) {
        return 0;
    } else {
        if ($statusCaixa['statuscaixa'] == 1) {
            $idCaixa = $statusCaixa['idcaixa'];
            return $idCaixa;
        } else {
            return 0;
        }
    }
}

?>

<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">Contas a Pagar</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Início</a></li>
                    <li class="breadcrumb-item">Financeiro</li>
                    <li class="breadcrumb-item active">Contas a Pagar</li>
                </ol>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10">
                    <i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">Registrar Conta a Pagar</h4>
                        <form method="post" action="?page=contas">
                            <div class="form-group">
                                <input type="hidden" name="txtCodigo" id="txtCodigo" value="<?= $codigo ?>"/>
                                <label class="control-label">Tipo de Conta</label>
                                <select class="form-control" id="selTipoConta" name="selTipoConta" required>
                                    <option value="0">Selecione o Tipo de conta...</option>
                                    <?php $tiposDeConta = $tipoDeContaController->ObterTiposConta();
                                    foreach ($tiposDeConta as $tipo) { ?>
                                        <option value="<?= $tipo->getId() ?>" <?php echo $conta->tipoConta->getId() == $tipo->getId() ? 'selected' : '' ?>>
                                            <?= $tipo->getDescricao() ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Valor</label>
                                <input type="text" id="txtValor" name="txtValor" class="form-control"
                                       placeholder="Valor" required value="<?= $conta->getValor() ?>"
                                       data-affixes-stay="true" data-prefix="R$ " data-thousands="," data-decimal=".">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Data de Vencimento</label>
                                <input type="date" id="txtVencimento" name="txtVencimento" class="form-control"
                                       placeholder="Data de Vencimento" required
                                       value="<?= $conta->getDataVencimento() == null ? "" : date('Y-m-d', strtotime($conta->getDataVencimento())) ?>">
                            </div>
                            <div class="form-actions">
                                <input type="submit" class="btn btn-success"
                                       style="margin-top: 32px; <?= $alterando ? 'display:none' : "" ?>"
                                       name="btnSalvar" id="btnSalvar" value="SALVAR"/>
                                <input type="submit" class="btn btn-success"
                                       style="margin-top: 32px; <?= $alterando ? "" : 'display:none' ?>"
                                       name="btnEditar" id="btnEditar" value="ALTERAR"/>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <br/>
                                    <?php echo $resultado ?>
                                </div>
                            </div>
                            <?php if (!empty($erros)) { ?>
                                <div class="form-group">
                                    <div class="col-xs-12" style="border: solid 2px red">
                                        <ul style="list-style: none;">
                                            <?php
                                            foreach ($erros as $e) {
                                                ?>
                                                <li><?= $e; ?></li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">Contas a Pagar</h4>
                        <h6 class="card-subtitle">Contas que ainda não foram pagas.</h6>
                        <div class="table-responsive m-t-40" style="min-height: 300px">
                            <?php $contas = $contaController->ObterContasPagar($idIgrejaSessao);
                            if (count($contas) > 0) { ?>
                                <table id="myTable" class="table table-bordered table-striped" style="font-size: 13px;">
                                    <thead>
                                    <tr>
                                        <th>Ações</th>
                                        <th>Descrição</th>
                                        <th>Data de Vencimento</th>
                                        <th>Valor</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($contas as $con) { ?>
                                        <tr>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-info dropdown-toggle"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        <i class="ti-settings"></i>
                                                    </button>
                                                    <div class="dropdown-menu animated flipInX">
                                                        <a class="dropdown-item"
                                                           href="?page=contas&baixar=<?= $con->getId() ?>"
                                                           onclick="return confirm('Confirma o pagamento da conta <?= $con->tipoConta->getDescricao() ?> ?');">Baixar
                                                            Conta</a>
                                                        <a class="dropdown-item"
                                                           href="?page=contas&editar=<?= $con->getId() ?>">Editar</a>
                                                        <a class="dropdown-item"
                                                           href="?page=contas&del=<?= $con->getId() ?>"
                                                           onclick="return confirm('Deseja excluir a conta <?= $con->tipoConta->getDescricao() ?> ?');">Excluir</a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><?= $con->tipoConta->getDescricao() ?></td>
                                            <td><?= date('d/m/Y', strtotime($con->getDataVencimento())) ?></td>
                                            <td><?= number_format($con->getValor(), 2, ',', '.') ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            <?php } else { ?>
                                <div class="alert alert-warning">Não há contas a pagar em aberto.</div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <?php include "View/Template/service-panel.php"; ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

    <?php include "View/Template/footer.php"; ?>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script>
        $(function () {
            $('#txtValor').maskMoney();
        });

        $(document).ready(function () {
            var aux = [{ "bSortable": false }, { "bSortable": true }, { "bSortable": true }, { "bSortable": true }];
            $('#myTable').DataTable({
                "pageLength": 10,
                "language": {
                    "lengthMenu": "EXIBIR _MENU_",
                    "zeroRecords": "Registros não encontrados",
                    "info": "Página _PAGE_ de _PAGES_",
                    "infoEmpty": "Registro não encontrado",
                    "infoFiltered": "(Um total de _MAX_ registros)",
                    "sSearch": "PESQUISAR: ",
                    "oPaginate": {
                        "sFirst": "Início",
                        "sPrevious": "Anterior",
                        "sNext": "Próximo",
                        "sLast": "Último"
                    }
                },
                "aoColumns": aux
            });
            getCss();
        });

        /**
         * Estilo para o css do datatable
         */

        function getCss()
        {
            $('label select').css('margin-left', '10px');
            $('#myTable_filter label').css('text-transform', 'uppercase');
            $('#myTable_length label').css('text-transform', 'uppercase');
            $('#myTable_length label').css('font-weight', 'bold');
            $('#myTable_length label').css('font-size', '0.9em');
            $('#myTable_length label').css('color', '#777171');
            $('#myTable_filter label').css('font-weight', 'bold');
            $('#myTable_filter label').css('font-size', '0.9em');
            $('#myTable_filter label').css('color', '#777171');
            $('input[type=search]').css('padding', '5px 5px');
            $('input[type=search]').css('width', '165px');
            $('#myTable_filter').css('margin-top', '25px');
            $('#myTable_length').css('margin-top', '25px');
        }

    </script>
