<?php
/**
 * Created by PhpStorm.
 * User: DEV02
 * Date: 23/04/2018
 * Time: 16:36
 */
require_once './_autoload.php';
session_start();

use Controller\CaixaController;

//Obtém o id da igreja relacionado ao usuário logado
$idIgrejaSessao = isset($_SESSION['idigreja']) ? $_SESSION['idigreja'] : null;
$caixa = false;

$caixaController = new CaixaController();
$statusCaixa = $caixaController->VerificaStatusCaixa($idIgrejaSessao);
if ($statusCaixa == false) {
    $caixa = false;
} else {
    if ($statusCaixa['statuscaixa'] == 1) {
        $caixa = true;
    } else {
        $caixa = false;
    }
}

?>

<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->

        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap">MÓDULOS</li>
                <li>
                    <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-cash"></i><span
                                class="hide-menu">Financeiro </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <?php if($caixa && $_SESSION['perfil'] == 1 || $_SESSION['perfil'] == 5 || $_SESSION['perfil'] == 2){ ?>
                            <li>
                                <a class="has-arrow" aria-expanded="false">Caixa</a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="?page=resumo-caixa">Resumo de Caixa</a></li>
                                    <li><a href="?page=caixa">Abrir/Fechar Caixa</a></li>
                                    <li><a href="?page=movimentacoes-caixa">Movimentações</a></li>
                                </ul>
                            </li>
                            <li><a href="?page=compras">Compras</a></li>
                            <li><a href="?page=contas">Contas a Pagar</a></li>
                            <li><a href="?page=dizimos">Dízimos</a></li>
                            <li><a href="?page=ofertas">Ofertas</a></li>
                            <li><a href="?page=tipo-conta">Tipos de Conta</a></li>
                        <?php }else{ ?>
                            <li>
                                <a class="has-arrow" aria-expanded="false">Caixa</a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="?page=resumo-caixa">Resumo de Caixa</a></li>
                                    <li><a href="?page=caixa">Abrir/Fechar Caixa</a></li>
                                    <li><a href="?page=movimentacoes-caixa">Movimentações</a></li>
                                </ul>
                            </li>
                        <li><a href="?page=tipo-conta">Tipos de Conta</a></li>
                        <?php } ?>
                        <li>
                            <a class="has-arrow" aria-expanded="false">Relatórios</a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="?page=relatorio-caixa">Relatório de Caixa</a></li>
                                <li><a href="?page=relatorio-compras">Relatório de Compras</a></li>
                                <li><a href="?page=relatorio-dizimos">Relatório de Dízimos</a></li>
                                <li><a href="?page=relatorio-financeiro">Relatório Financeiro</a></li>
                                <li><a href="?page=relatorio-movimentacoes">Relatório de Movimentações Financeiras</a></li>
                                <li><a href="?page=relatorio-ofertas">Relatório de Ofertas</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow " href="#" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span
                                class="hide-menu">Administrativo</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a class="has-arrow" aria-expanded="false">Campanhas</a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="?page=cadastro-campanha">Cadastrar Campanha</a></li>
                                <li><a href="?page=campanhas">Campanhas</a></li>
                            </ul>
                        </li>

                        <li><a href="?page=cargos">Cargos</a></li>
                        <li><a href="?page=classes-ebd">Classes EBD</a></li>
                        <li>
                            <a class="has-arrow" aria-expanded="false">Igreja</a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="?page=dados-igreja">Dados da Igreja</a></li>
                                <li><a href="?page=cadastro-igreja&editar=<?=$idIgrejaSessao?>">Editar Dados</a></li>
                            </ul>
                        </li>
                        <li><a href="?page=lideres">Líderes</a></li>
                        <li>
                        <a class="has-arrow" aria-expanded="false">Membros</a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="?page=cadastro-membro">Cadastrar Membro</a></li>
                            <li><a href="?page=membros">Pesquisar Membro</a></li>
                        </ul>
                        </li>
                        <!-- Ministérios -->
                        <li><a href="?page=ministerios">Ministérios</a></li>
                        <li><a href="?page=pastores">Pastores</a></li>
                        <li>
                            <a class="has-arrow" aria-expanded="false">Relatórios</a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="?page=relatorio-campanhas">Relatório de Campanhas</a></li>
                                <li><a href="?page=relatorio-membros">Relatório de Membros</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
<!--                <li>-->
<!--                    <a class="has-arrow " href="#" aria-expanded="false"><i class="mdi mdi-church"></i><span-->
<!--                                class="hide-menu">Igreja</span></a>-->
<!--                    <ul aria-expanded="false" class="collapse">-->
<!--                        <li><a href="?page=igrejas">Gerenciar Igrejas</a></li>-->
<!---->
<!--                    </ul>-->
<!--                </li>-->
<!--                <li>-->
<!--                    <a class="has-arrow " href="#" aria-expanded="false"><i class="mdi mdi-account-key"></i><span-->
<!--                                class="hide-menu">Usuários</span></a>-->
<!--                    <ul aria-expanded="false" class="collapse">-->
<!--                        <li><a href="?page=usuarios">Gerenciar Usuários</a></li>-->
<!---->
<!--                    </ul>-->
<!--                </li>-->

            </ul>
            </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
    <!-- Bottom points-->
    <div class="sidebar-footer ">
        <a href="?page=logout" class="link" data-toggle="tooltip"  title="Sair"><i class="mdi mdi-power"></i></a>
    </div>
    <!-- End Bottom points-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
