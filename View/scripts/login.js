$('document').ready(function(){
    $("#btnLogin").click(function(){
//		var data = $("#login-form").serialize();                           ;
        var data = {
            txtLogin: $("#txtLogin").val(),
            txtSenha: $("#txtSenha").val()
        };
        $.ajax({
            type : 'POST',
            url  : 'validacao.php',
            data : data,
            dataType: 'json',
            beforeSend: function()
            {
                $("#btnLogin").html('Validando ...');
            },
            success :  function(response){
                if(response.codigo == "1"){
                    $("#btnLogin").html('ENTRAR');
                    $("#login-alert").css('display', 'none');
                    window.location.href = "?page=dashboard";
                }else{
//                                                                        alert("CHEGOU AK");
                    $("#btnLogin").html('ENTRAR');
                    $("#login-alert").css('display', 'block');
                    $("#login-alert").addClass('alert alert-danger');
                    $("#mensagem").html('<strong>Erro! </strong>' + response.mensagem);
                }
            },
            error: function(){
                alert("erro de req.");
            }
        });
    });

});