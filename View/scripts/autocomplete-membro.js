$(function() {

    // Dispara o Autocomplete a partir do segundo caracter
    $( "#txtMembro").autocomplete({
        minLength: 2,
        source: function( request, response ) {
            $.ajax({
                url: "Util/search-membro.php",
                dataType: "json",
                data: {
                    acao: 'autocomplete',
                    parametro: $('#txtMembro').val()
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        focus: function( event, ui ) {
            $("#txtMembro").val( ui.item.nome );
            return false;
        },
        select: function( event, ui ) {
            $("#txtMembro").val( ui.item.nome );
            $("#txtMembroId").val(ui.item.idmembro);
            return false;
        }
    })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" )
            .append( "<a><b>Código de Barra: </b>" + item.codigo_barra + "<br><b>Título: </b>" + item.titulo + " - <b> Categoria: </b>" + item.categoria + "</a><br>" )
            .appendTo( ul );
    };
});