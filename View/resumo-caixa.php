<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 15/06/2018
 * Time: 02:12
 */
require_once './_autoload.php';
include "template/header.php";
include "template/menu.php";
session_start();

use Controller\CaixaController;

$idIgrejaSessao = isset($_SESSION['idigreja']) ? $_SESSION['idigreja'] : null;
$caixaController = new CaixaController();


?>
    <div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">Resumo de Caixa</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Início</a></li>
                    <li class="breadcrumb-item active">Resumo de Caixa</li>
                </ol>
            </div>
        </div>

        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round align-self-center round-success"><i class="ti-wallet"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h3 class="m-b-0"> <?php

                                    $somaCaixa = $caixaController->ResumoCaixa($idIgrejaSessao);
                                    echo number_format($somaCaixa['somacaixa'], 2, ',', '.');
                                    ?>
                                </h3>
                                <h5 class="text-muted m-b-0">Saldo Atual</h5></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <?php include "View/Template/service-panel.php"; ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

<?php include "View/Template/footer.php"; ?>
<!-- This page plugins -->
<!-- ============================================================== -->
<script src="assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="assets/plugins/gauge/gauge.min.js"></script>
<script src="js/widget-data.js"></script>
