<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 25/04/2018
 * Time: 23:46
 */
require_once './_autoload.php';
use DAL\Conexao;

session_start();

$pdo = new Conexao();

// Recebe os dados do formulário
$email = (isset($_POST['txtLogin'])) ? $_POST['txtLogin'] : '';
$senha = (isset($_POST['txtSenha'])) ? $_POST['txtSenha'] : '' ;


// Dica 2 - Validações de preenchimento e-mail e senha se foi preenchido o e-mail
if (empty($email)){
    $retorno = array('codigo' => 0, 'mensagem' => 'Preencha seu e-mail!');
    echo json_encode($retorno);
    exit;
}

if (empty($senha)){
    $retorno = array('codigo' => 0, 'mensagem' => 'Preencha sua senha!');
    echo json_encode($retorno);
    exit;
}

// Dica 3 - Verifica se o formato do e-mail é válido
if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
    $retorno = array('codigo' => 0, 'mensagem' => 'Formato de e-mail inválido!');
    echo json_encode($retorno);
    exit;
}

// Dica 4 - Válida os dados do usuário com o banco de dados
$sql = "SELECT * FROM usuario WHERE email = :email AND ativo = :ativo LIMIT 1";
$param = array(
    ":email" => $email,
    ":ativo" => 1
);
$retorno = $pdo->ExecuteQueryOneRow($sql, $param);

// Dica 5 - Válida a senha utlizando a API Password Hash
if(!empty($retorno) && password_verify($senha, $retorno['senha'])){
    $_SESSION['idusuario'] = $retorno['idusuario'];
    $_SESSION['nome'] = $retorno['nome'];
    $_SESSION['email'] = $retorno['email'];
    $_SESSION['idigreja'] = $retorno['idigreja'];
    $_SESSION['perfil'] = $retorno['perfil'];
    $_SESSION['logado'] = 'S';
}else{
    $_SESSION['logado'] = 'N';
}

// Se logado envia código 1, senão retorna mensagem de erro para o login
    if ($_SESSION['logado'] == 'S'){
    $retorno = array('codigo' => 1, 'mensagem' => 'Logado com sucesso!');
    echo json_encode($retorno);
    exit;
}else{
    $retorno = array('codigo' => 2, 'mensagem' => 'Email ou Senha incorreto.');
    echo json_encode($retorno);
    exit;
}

