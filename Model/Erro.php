<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 13/05/2018
 * Time: 16:49
 */

namespace Model;


class Erro
{
    private $mensagens;

    final public function add($msg)
    {
        $this->mensagens[] = $msg;
    }

    final public function getMensagens()
    {
        return $this->mensagens;
    }

    final public function possuiErros()
    {
        return isset($this->mensagens);
    }
}