<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 29/04/2018
 * Time: 21:55
 */
namespace Model;

class Usuario
{

    private $id;
    private $nome;
    private $senha;
    private $email;
    private $ativo;

    /**
     * @var $perfil
     *
     * 1 - Administrador
     * 2 - Pastor
     * 3 - Presidente/V. Presidente
     * 4 - Secretário
     * 5 - Tesoureiro
     * 6 - Membro
     *
     */
    private $perfil;
    private $usuario;
    public $igreja;

    /**
     * Usuario constructor.
     */
    public function __construct()
    {
        $this->igreja = new Igreja();
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return string
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * @param string $senha
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * @param int $ativo
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    }

    /**
     * @return int
     */
    public function getPerfil()
    {
        return $this->perfil;
    }

    /**
     * @param int $perfil
     */
    public function setPerfil($perfil)
    {
        $this->perfil = $perfil;
    }

    /**
     * @return string
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param string $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

}