<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:43
 */

namespace Model;

class MovimentoCaixa
{
    private $id;
    public $caixa;
    private $descricao;
    /**
     * @var $tipo
     *
     * 1 - Crédito
     * 2 - Débito
     * 3 - Estorno
     */
    private $tipo;
    private $valor;
    private $data;
    public $compra;
    public $contaPagar;
    public $oferta;
    public $dizimo;
    public $usuario;
    public $igreja;

    /**
     * MovimentoCaixa constructor.
     */
    public function __construct()
    {
        $this->usuario = new Usuario();
        $this->caixa = new Caixa();
        $this->compra = new Compra();
        $this->contaPagar = new ContaPagar();
        $this->dizimo = new Dizimo();
        $this->oferta = new Oferta();
        $this->igreja = new Igreja();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return mixed
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * @return mixed
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param mixed $valor
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */

}