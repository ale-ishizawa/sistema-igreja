<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:26
 */
namespace Model;

class Compra
{
    private $id;
    private $dataCompra;
    private $valor;
    private $descricao;
    private $ativo;
    private $usuario;
    public $igreja;
    private $dataCadastro;

    /**
     * Compra constructor.
     */
    public function __construct()
    {
        $this->igreja = new Igreja();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDataCompra()
    {
        return $this->dataCompra;
    }

    /**
     * @param mixed $dataCompra
     */
    public function setDataCompra($dataCompra)
    {
        $this->dataCompra = $dataCompra;
    }

    /**
     * @return mixed
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param mixed $valor
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    }

    /**
     * @return mixed
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return mixed
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param mixed $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * @param int $ativo
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    }

    /**
     * @return mixed
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param mixed $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }


}