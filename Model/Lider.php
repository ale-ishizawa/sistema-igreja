<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:40
 */

namespace Model;

class Lider extends Membro
{
    private $ministerioNome;

    /**
     * Lider constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param mixed $ministerioNome
     */
    public function setMinisterioNome($ministerioNome)
    {
        $this->ministerioNome = $ministerioNome;
    }

    /**
     * @return mixed
     */
    public function getMinisterioNome()
    {
        return $this->ministerioNome;
    }
}