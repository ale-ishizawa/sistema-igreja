<?php
/**
 * Created by PhpStorm.
 * User: DEV02
 * Date: 23/04/2018
 * Time: 17:37
 */
namespace Model;

class Caixa
{
    private $id;
    private $dataCaixa;
    private $status;
    private $horaAbertura;
    private $horaFechamento;
    private $valorInicial;
    private $valorFinal;
    public $igreja;
    public $usuario;

    /**
     * Caixa constructor.
     */
    public function __construct()
    {
        $this->igreja = new Igreja();
        $this->usuario = new Usuario();
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDataCaixa()
    {
        return $this->dataCaixa;
    }

    /**
     * @param mixed $dataCaixa
     */
    public function setDataCaixa($dataCaixa)
    {
        $this->dataCaixa = $dataCaixa;
    }
    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getHoraAbertura()
    {
        return $this->horaAbertura;
    }

    /**
     * @param mixed $horaAbertura
     */
    public function setHoraAbertura($horaAbertura)
    {
        $this->horaAbertura = $horaAbertura;
    }

    /**
     * @return mixed
     */
    public function getHoraFechamento()
    {
        return $this->horaFechamento;
    }

    /**
     * @param mixed $horaFechamento
     */
    public function setHoraFechamento($horaFechamento)
    {
        $this->horaFechamento = $horaFechamento;
    }

    /**
     * @return mixed
     */
    public function getValorInicial()
    {
        return $this->valorInicial;
    }

    /**
     * @param mixed $valorInicial
     */
    public function setValorInicial($valorInicial)
    {
        $this->valorInicial = $valorInicial;
    }

    /**
     * @return mixed
     */
    public function getValorFinal()
    {
        return $this->valorFinal;
    }

    /**
     * @param mixed $valorFinal
     */
    public function setValorFinal($valorFinal)
    {
        $this->valorFinal = $valorFinal;
    }

}