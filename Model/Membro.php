<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:14
 */

namespace Model;

class Membro
{
    private $id;
    private $nome;
    private $sexo;
    private $logradouro;
    private $numero;
    private $bairro;
    private $complemento;
    private $cep;
    private $telefone;
    private $celular;
    private $email;
    private $nivelEscolar;
    private $nascimento;
    private $naturalidade;
    private $rg;
    private $orgao;
    private $cpf;

    /**
     * @var $estadoCivil
     * 0 - Solteiro
     * 1 - Casado
     * 2 - Separado
     * 3 - Divorciado
     * 4 - Viúvo
     * 5 - Amasiado
     */
    private $estadoCivil;
    private $conjuge;
    private $dataCasamento;
    private $nomePai;
    private $nomeMae;
    private $dataBatismo;
    private $igrejaOrigem;
    public $igreja;
    public $cargo;
    public $cidade;
    private $ativo;

    /**
     * Membro constructor.
     */
    public function __construct()
    {
        $this->igreja = new Igreja();
        $this->cargo = new Cargo();
        $this->cidade = new Cidade();
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * @param mixed $sexo
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;
    }

    /**
     * @return mixed
     */
    public function getLogradouro()
    {
        return $this->logradouro;
    }

    /**
     * @param mixed $logradouro
     */
    public function setLogradouro($logradouro)
    {
        $this->logradouro = $logradouro;
    }

    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param mixed $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * @return mixed
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * @param mixed $bairro
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;
    }

    /**
     * @return mixed
     */
    public function getComplemento()
    {
        return $this->complemento;
    }

    /**
     * @param mixed $complemento
     */
    public function setComplemento($complemento)
    {
        $this->complemento = $complemento;
    }

    /**
     * @return mixed
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * @param mixed $cep
     */
    public function setCep($cep)
    {
        $this->cep = $cep;
    }

    /**
     * @return mixed
     */
    public function getTelefone()
    {
        return $this->telefone;
    }

    /**
     * @param mixed $telefone
     */
    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
    }

    /**
     * @return mixed
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * @param mixed $celular
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getNivelEscolar()
    {
        return $this->nivelEscolar;
    }

    /**
     * @param mixed $nivelEscolar
     */
    public function setNivelEscolar($nivelEscolar)
    {
        $this->nivelEscolar = $nivelEscolar;
    }

    /**
     * @return mixed
     */
    public function getNascimento()
    {
        return $this->nascimento;
    }

    /**
     * @param mixed $nascimento
     */
    public function setNascimento($nascimento)
    {
        $this->nascimento = $nascimento;
    }

    /**
     * @return mixed
     */
    public function getNaturalidade()
    {
        return $this->naturalidade;
    }

    /**
     * @param mixed $naturalidade
     */
    public function setNaturalidade($naturalidade)
    {
        $this->naturalidade = $naturalidade;
    }

    /**
     * @return mixed
     */
    public function getRg()
    {
        return $this->rg;
    }

    /**
     * @param mixed $rg
     */
    public function setRg($rg)
    {
        $this->rg = $rg;
    }

    /**
     * @return mixed
     */
    public function getOrgao()
    {
        return $this->orgao;
    }

    /**
     * @param mixed $orgao
     */
    public function setOrgao($orgao)
    {
        $this->orgao = $orgao;
    }

    /**
     * @return mixed
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param mixed $cpf
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
    }

    /**
     * @return mixed
     */
    public function getEstadoCivil()
    {
        return $this->estadoCivil;
    }

    /**
     * @param mixed $estadoCivil
     */
    public function setEstadoCivil($estadoCivil)
    {
        $this->estadoCivil = $estadoCivil;
    }

    /**
     * @return mixed
     */
    public function getConjuge()
    {
        return $this->conjuge;
    }

    /**
     * @param mixed $conjuge
     */
    public function setConjuge($conjuge)
    {
        $this->conjuge = $conjuge;
    }

    /**
     * @return mixed
     */
    public function getDataCasamento()
    {
        return $this->dataCasamento;
    }

    /**
     * @param mixed $dataCasamento
     */
    public function setDataCasamento($dataCasamento)
    {
        $this->dataCasamento = $dataCasamento;
    }

    /**
     * @return mixed
     */
    public function getNomePai()
    {
        return $this->nomePai;
    }

    /**
     * @param mixed $nomePai
     */
    public function setNomePai($nomePai)
    {
        $this->nomePai = $nomePai;
    }

    /**
     * @return mixed
     */
    public function getNomeMae()
    {
        return $this->nomeMae;
    }

    /**
     * @param mixed $nomeMae
     */
    public function setNomeMae($nomeMae)
    {
        $this->nomeMae = $nomeMae;
    }

    /**
     * @return mixed
     */
    public function getDataBatismo()
    {
        return $this->dataBatismo;
    }

    /**
     * @param mixed $dataBatismo
     */
    public function setDataBatismo($dataBatismo)
    {
        $this->dataBatismo = $dataBatismo;
    }

    /**
     * @return mixed
     */
    public function getIgrejaOrigem()
    {
        return $this->igrejaOrigem;
    }

    /**
     * @param mixed $igrejaOrigem
     */
    public function setIgrejaOrigem($igrejaOrigem)
    {
        $this->igrejaOrigem = $igrejaOrigem;
    }

    /**
     * @return Igreja
     */
    public function getIgreja(): Igreja
    {
        return $this->igreja;
    }

    /**
     * @param Igreja $igreja
     */
    public function setIgreja(Igreja $igreja)
    {
        $this->igreja = $igreja;
    }

    /**
     * @return Cargo
     */
    public function getCargo(): Cargo
    {
        return $this->cargo;
    }

    /**
     * @param Cargo $cargo
     */
    public function setCargo(Cargo $cargo)
    {
        $this->cargo = $cargo;
    }

    /**
     * @return Cidade
     */
    public function getCidade(): Cidade
    {
        return $this->cidade;
    }

    /**
     * @param Cidade $cidade
     */
    public function setCidade(Cidade $cidade)
    {
        $this->cidade = $cidade;
    }

    /**
     * @return mixed
     */
    public function getAtivo()
    {
        return $this->ativo;
    }/**
     * @param mixed $ativo
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    }


}