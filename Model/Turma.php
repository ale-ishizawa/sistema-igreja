<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:32
 */

namespace Model;


class Turma
{
    public $membro;
    public $classe;

    /**
     * Turma constructor.
     */
    public function __construct()
    {
        $this->membro = new Membro();
        $this->classe = new ClasseEbd();
    }


}