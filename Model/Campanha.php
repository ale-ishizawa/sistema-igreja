<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:36
 */

namespace Model;


class Campanha
{
    private $id;
    private $ativo;
    private $dataInicio;
    private $dataFim;
    private $descricao;
    private $nome;
    private $valorAlvo;
    public $igreja;

    /**
     * Campanha constructor.
     */
    public function __construct()
    {
        $this->igreja = new Igreja();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * @param mixed $ativo
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    }

    /**
     * @return mixed
     */
    public function getDataInicio()
    {
        return $this->dataInicio;
    }

    /**
     * @param mixed $dataInicio
     */
    public function setDataInicio($dataInicio)
    {
        $this->dataInicio = $dataInicio;
    }

    /**
     * @return mixed
     */
    public function getDataFim()
    {
        return $this->dataFim;
    }

    /**
     * @param mixed $dataFim
     */
    public function setDataFim($dataFim)
    {
        $this->dataFim = $dataFim;
    }

    /**
     * @return mixed
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getValorAlvo()
    {
        return $this->valorAlvo;
    }

    /**
     * @param mixed $valorAlvo
     */
    public function setValorAlvo($valorAlvo)
    {
        $this->valorAlvo = $valorAlvo;
    }

    /**
     * @return Igreja
     */
    public function getIgreja(): Igreja
    {
        return $this->igreja;
    }

    /**
     * @param Igreja $igreja
     */
    public function setIgreja(Igreja $igreja)
    {
        $this->igreja = $igreja;
    }



}