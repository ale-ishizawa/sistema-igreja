<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 06/05/2018
 * Time: 16:38
 */

namespace Model;


class MembrosMinisterio
{
    public $membro;
    public $ministerio;

    /**
     * MembrosMinisterio constructor.
     */
    public function __construct()
    {
        $this->membro = new Membro();
        $this->ministerio = new Ministerio();
    }


}