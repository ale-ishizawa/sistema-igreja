<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 20/05/2018
 * Time: 11:27
 */

namespace DAL;

use Model\Lider;

class LiderDAO
{

    private $pdo;
    private $debug;

    /**
     * LiderDAO constructor.
     */
    public function __construct()
    {
        $this->pdo = new Conexao();
        $this->debug = true;
    }

    /**
     * @param Lider $lider
     * @return bool
     */
    public function Gravar(Lider $lider)
    {
        try{
            //Verifico se o Líder ja esta cadastrado

            $sql = "INSERT INTO lider(idmembro) VALUES (:id)";
            $param = array(
                ":id" => $lider->getId()
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * @param $idMembro
     * @return bool
     */
    public function Deletar($idMembro)
    {
        try{
            //Update na tabela Ministério
            $sql = "UPDATE ministerio SET idmembro = NULL 
                    WHERE idmembro = :id";
            $param = array(
                ":id" => $idMembro
            );

            if($this->pdo->ExecuteNonQuery($sql, $param)){
                $sql = "DELETE FROM lider WHERE idmembro = :id";
                return $this->pdo->ExecuteNonQuery($sql, $param);
            }else{
                $this->pdo->Rollback(); //Testing RollBack
                return false;
            }
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * @param $idIgreja
     * @return array|null
     */
    public function ObterLideres($idIgreja)
    {
        try{
            $sql = "SELECT l.idmembro, m.nome as liderNome
                    FROM lider l 
                    INNER JOIN membro m ON l.idmembro = m.idmembro
                    WHERE m.idigreja = :idIgreja";
            $param = array(
                ":idIgreja" => $idIgreja
            );

            $dados = $this->pdo->ExecuteQuery($sql, $param);
            $listaLideres = [];

            foreach ($dados as $l){
                $lider = new Lider();
                $lider->setId($l['idmembro']);
                $lider->setNome($l['liderNome']);
                $listaLideres[] = $lider;
            }

            return $listaLideres;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    /**
     * @param Lider $lider
     * @return one|null
     */
    public function VerificaCadastro(Lider $lider)
    {
        try{
            $sql = "SELECT idmembro FROM lider WHERE idmembro = :id";
            $param = array(
                ":id" => $lider->getId()
            );

            $row = $this->pdo->ExecuteQueryOneRow($sql, $param);
            return $row;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }


}