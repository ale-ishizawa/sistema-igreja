<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 20/05/2018
 * Time: 13:00
 */

namespace DAL;

use DAL\Conexao;
use Model\Ministerio;

class MinisterioDAO
{
    private $pdo;
    private $debug;
    private $cidDAO;

    /**
     * MinisterioDAO constructor.
     */
    public function __construct()
    {
        $this->pdo = new Conexao();
        $this->debug = true;
    }

    /**
     * @param Ministerio $ministerio
     * @return bool
     */
    public function Gravar(Ministerio $ministerio)
    {
        try{
            $sql = "INSERT INTO ministerio(nome, descricao, idmembro, idigreja) 
                VALUES (:nome, :descricao, :idmembro, :idigreja)";
            $param = array(
                ":nome" => $ministerio->getNome(),
                ":descricao" => $ministerio->getDescricao(),
                ":idmembro" => $ministerio->lider->getId(),
                ":idigreja" => $ministerio->igreja->getId()
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);

        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * Deletar
     *
     * Exclui todos os registros da tabela Membros_Ministerio que possuem o mesmo id do ministério e depois
     * Exclui um registro da tabela Ministerio e o lider do ministério da tabela Lider
     *
     * @param $id
     * @return bool
     */
    public function Deletar($id)
    {
        try{
            //Obtém os dados do ministério
            $ministerio = MinisterioDAO::ObterMinisterioPorCod($id);
            if($ministerio->getId() > 0){
                $sql = "DELETE FROM membros_ministerio WHERE idministerio = :id";
                $param = array(
                    ":id" => $id
                );
                if($this->pdo->ExecuteNonQuery($sql, $param)){
                    $sql = "DELETE FROM ministerio WHERE idministerio = :id";
                    $param = array(
                        ":id" => $id
                    );
                    if($this->pdo->ExecuteNonQuery($sql, $param)){
                        $sql = "DELETE FROM lider WHERE idmembro = :id";
                        $param = array(
                            ":id" => $id
                        );
                        return $this->pdo->ExecuteNonQuery($sql, $param);
                    }
                }
            }else{
                return false;
            }
           }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * @param Ministerio $ministerio
     * @return bool
     */
    public function Editar(Ministerio $ministerio)
    {
        try{
            $ministerioPrevious = MinisterioDAO::ObterMinisterioPorCod($ministerio->getId());

            $sql = "UPDATE ministerio 
                    SET nome = :nome,   
                    descricao = :descricao,
                    idmembro = :lider
                    WHERE idministerio = :id";
            $param = array(
                ":nome" => $ministerio->getNome(),
                ":descricao" => $ministerio->getDescricao(),
                ":id" => $ministerio->getId(),
                ":lider" => $ministerio->lider->getId()
            );

            if($this->pdo->ExecuteNonQuery($sql, $param)) {
                $sql = "UPDATE lider SET idmembro = :newid WHERE idmembro = :oldid";
                $param = array(
                    ":newid" => $ministerio->lider->getId(),
                    ":oldid" => $ministerioPrevious->lider->getId()
                );

                return $this->pdo->ExecuteNonQuery($sql, $param);
            }
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            $this->pdo->Rollback();
            return false;
        }
    }

    /**
     * @param $idIgreja
     * @return array|null
     */
    public function ObterMinisterios($idIgreja)
    {
        try{
            $sql = "SELECT idministerio, nome, descricao FROM ministerio WHERE idigreja = :idigreja";
            $param = array(
                ":idigreja" => $idIgreja
            );

            $dados = $this->pdo->ExecuteQuery($sql, $param);
            $listaMinisterios = [];

            foreach ($dados as $m){
                $ministerio = new Ministerio();
                $ministerio->setId($m['idministerio']);
                $ministerio->setNome($m['nome']);
                $ministerio->setDescricao($m['descricao']);
                $listaMinisterios[] = $ministerio;
            }

            return $listaMinisterios;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    public function ObterMinisterioPorCod($id)
    {
        try{
            $sql = "SELECT min.idministerio, min.nome as ministerioNome, min.descricao, min.idmembro, m.nome as membroNome
                    FROM ministerio min
                    INNER JOIN lider l ON min.idmembro = l.idmembro
                    INNER JOIN membro m ON l.idmembro = m.idmembro
                    WHERE min.idministerio = :id";
            $param = array(
                ":id" => $id
            );

            $m = $this->pdo->ExecuteQueryOneRow($sql, $param);

            $ministerio = new Ministerio();
            $ministerio->setId($m['idministerio']);
            $ministerio->setNome($m['ministerioNome']);
            $ministerio->setDescricao($m['descricao']);
            $ministerio->lider->setId($m['idmembro']);
            $ministerio->lider->setNome($m['membroNome']);

            return $ministerio;

        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

}