<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 20/05/2018
 * Time: 10:52
 */
namespace DAL;

use DAL\Conexao;
use Model\Pastor;

class PastorDAO
{
    private $pdo;
    private $debug;

    /**
     * PastorDAO constructor.
     */
    public function __construct()
    {
        $this->pdo = new Conexao();
        $this->debug = true;
    }

    public function Gravar(Pastor $pastor)
    {
        try{
            $sql = "INSERT INTO pastor(idmembro) VALUES (:id)";
            $param = array(
                ":id" => $pastor->getId()
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }


    /**
     * @param $idMembro
     * @return bool
     */
    public function Deletar($idMembro)
    {
        try{

            $sql = "DELETE FROM pastor WHERE idmembro = :id";
            $param = array(
                ":id" => $idMembro
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    public function ObterPastores($idIgreja)
    {
        try{
            $sql = "SELECT p.idmembro, m.nome
                    FROM pastor p 
                    INNER JOIN membro m ON p.idmembro = m.idmembro 
                    WHERE m.idigreja = :idIgreja";
            $param = array(
                ":idIgreja" => $idIgreja
            );

            $dados = $this->pdo->ExecuteQuery($sql, $param);
            $listaPastores = [];

            foreach ($dados as $p){
                $pastor = new Pastor();
                $pastor->setId($p['idmembro']);
                $pastor->setNome($p['nome']);
                $listaPastores[] = $pastor;
            }

            return $listaPastores;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    /**
     * @param Pastor $pastor
     * @return one|null
     */
    public function VerificaCadastro(Pastor $pastor)
    {
        try{
            $sql = "SELECT idmembro FROM pastor WHERE idmembro = :id";
            $param = array(
                ":id" => $pastor->getId()
            );

            $row = $this->pdo->ExecuteQueryOneRow($sql, $param);
            return $row;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }
}