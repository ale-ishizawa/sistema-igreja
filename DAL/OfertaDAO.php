<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 05/06/2018
 * Time: 23:10
 */
namespace DAL;
session_start();
use Model\Oferta;
use DateTime;

class OfertaDAO
{
    /**
     * @var Conexao $pdo
     */
    private $pdo;
    private $debug;

    /**
     * OfertaDAO constructor.
     */
    public function __construct()
    {
        $this->pdo = new Conexao();
        $this->debug = true;
    }

    public function Gravar(Oferta $oferta, $idCaixa)
    {
        try{
            $sql = "INSERT INTO oferta(ativo, valor, data, idmembro, idcampanha)
                    VALUES (:ativo, :valor, :datacad, :idmembro, :idcampanha)";
            $param = array(
                ":ativo" => $oferta->getAtivo(),
                ":valor" => $oferta->getValor(),
                ":datacad" => $oferta->getData(),
                ":idmembro" => $oferta->membro->getId(),
                ":idcampanha" => $oferta->campanha->getId(),
            );

            if($this->pdo->ExecuteNonQuery($sql, $param)){
                //Oferta foi gravada com sucesso, em seguida gravo a movimentação do caixa
                $ultimoIdOferta = $this->pdo->GetLastID();

                //Cadastro o Movimento de Caixa
                $sql = "INSERT INTO movimento_caixa(idcaixa, idigreja, descricao, tipo, valor, idoferta, idusuario, datamovimento) 
                            VALUES(:idcaixa, :idigreja, :descricao, :tipo, :valor, :idoferta, :idusuario, :datamovimento)";
                $param = array(
                    ":idcaixa" => $idCaixa,
                    ":idigreja" => $_SESSION['idigreja'],
                    ":descricao" => 'Lançamento de Oferta',
                    ":tipo" => 1,
                    ":valor" => $oferta->getValor(),
                    ":idoferta" => $ultimoIdOferta,
                    ":idusuario" => $_SESSION['idusuario'],
                    ":datamovimento" => $oferta->getData()
                );

                return $this->pdo->ExecuteNonQuery($sql, $param);
            }else{
                return false;
            }
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    public function ObterOfertas($idIgreja)
    {
        try{
            $dataAtual = new DateTime();
            $interval = new \DateInterval('P1M');
            $dataAtual->sub($interval);
            $dataParam = $dataAtual->format('Y-m-d');

            $sql = "SELECT o.idoferta, o.ativo, o.valor, o.data, m.nome as membronome, c.nome as campanhanome
                    FROM oferta o
                    INNER JOIN movimento_caixa mc ON o.idoferta = mc.idoferta
                    INNER JOIN membro m ON o.idmembro = m.idmembro
                    INNER JOIN campanha c ON o.idcampanha = c.idcampanha                   
                    WHERE o.data > :dataparam AND mc.idigreja = :idigreja
                    ORDER BY o.idoferta DESC";
            $param = array(
                ":dataparam" => $dataParam,
                ":idigreja" => $idIgreja
            );

            $dados = $this->pdo->ExecuteQuery($sql, $param);
            $listaOfertas = [];

            foreach ($dados as $ofer){
                $oferta = new Oferta();
                $oferta->setId($ofer['idoferta']);
                $oferta->setValor($ofer['valor']);
                $oferta->setData($ofer['data']);
                $oferta->setAtivo($ofer['ativo']);
                $oferta->membro->setNome($ofer['membronome']);
                $oferta->campanha->setNome($ofer['campanhanome']);
                $listaOfertas[] = $oferta;
            }

            return $listaOfertas;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    /**
     * RelatorioOfertas
     *
     * Retorna uma lista de ofertas em um período de uma determinada igreja
     *
     * @param $idIgreja
     * @param $inicio
     * @param $fim
     * @return array|null
     */
    public function RelatorioOfertas($idIgreja, $inicio, $fim)
    {
        try{
            $sql = "SELECT o.idoferta, o.valor, o.data, m.nome 
                    FROM oferta o
                    INNER JOIN membro m ON o.idmembro = m.idmembro 
                    WHERE m.idigreja = :idigreja AND o.data BETWEEN :inicio AND :fim AND o.ativo = 1";
            $param = array(
                ":idigreja" => $idIgreja,
                ":inicio" => $inicio,
                ":fim" => $fim
            );

            $dados = $this->pdo->ExecuteQuery($sql, $param);
            $listaOfertas = [];

            foreach ($dados as $o){
                $oferta = new Oferta();
                $oferta->setId($o['idoferta']);
                $oferta->setData($o['data']);
                $oferta->setValor($o['valor']);
                $oferta->membro->setNome($o['nome']);
                $listaOfertas[] = $oferta;
            }

            return $listaOfertas;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

}