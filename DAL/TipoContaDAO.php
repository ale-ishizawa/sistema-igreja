<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 07/05/2018
 * Time: 01:12
 */

namespace DAL;

use DAL\Conexao;
use Model\TipoConta;

class TipoContaDAO
{
    private $pdo;
    private $debug;

    /**
     * TipoContaDAO constructor.
     */
    public function __construct()
    {
        $this->pdo = new Conexao();
        $this->debug = true;
    }

    /**
     * @param TipoConta $tipo
     * @return bool
     */
    public function Gravar(TipoConta $tipo)
    {
        try{
            $sql = "INSERT INTO tipo_conta (descricao, ativo) VALUES (:desc, :ativo)";
            $param = array(
                ":desc" => $tipo->getDescricao(),
                ":ativo" => $tipo->getAtivo()
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        }catch (\PDOException $ex){
            if($this->debug){
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * @return array|null
     */
    public function ObterTiposConta()
    {
        try{
            $sql = "SELECT * FROM tipo_conta WHERE ativo = 1 ORDER BY descricao ASC";

            $dados = $this->pdo->ExecuteQuery($sql);
            $lista = [];

            foreach ($dados as $d){
                $tipo = new TipoConta();
                $tipo->setDescricao($d['descricao']);
                $tipo->setId($d['idtipoconta']);
                $lista[] = $tipo;
            }

            return $lista;
        }catch (PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    /**
     * @param $id
     * @return bool
     */
    public function Inativar($id)
    {
        try{
            $sql = "UPDATE tipo_conta 
                    SET ativo = 2 
                    WHERE idtipoconta = :id";
            $param = array(
                ":id" => $id
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        }catch (\PDOException $ex){
            if($this->debug){
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * Realiza o update na tabela se o tipo de conta estiver relacionada com a tabela conta a pagar, caso contrário realiza a exclusão.
     *
     * @param $id
     * @return bool
     */
    public function Deletar($id)
    {
        try{
            $sql = "SELECT idcontapagar, idtipoconta FROM conta_pagar WHERE idtipoconta = :id";
            $param = array(
                ":id" => $id
            );

            $result = $this->pdo->ExecuteQueryOneRow($sql, $param);
            if($result != false){
                //Existe algum registo de conta a pagar com esse tipo de conta, nesse caso apenas inativa o tipo de conta, para mantes o registro das contas.
                $sql = "UPDATE tipo_conta SET ativo = 2 WHERE idtipoconta = :id";
                $param = array(
                    ":id" => $id
                );

                return $this->pdo->ExecuteNonQuery($sql, $param);
            }else{
                //Nesse caso realiza a exclusão do tipo de conta, visto que não possui relacionamento algum com a tabela Contas_pagar
                $sql = "DELETE FROM tipo_conta WHERE idtipoconta = :id";
                $param = array(
                    ":id" => $id
                );

                return $this->pdo->ExecuteNonQuery($sql, $param);
            }
        }catch (\PDOException $ex){
            if($this->debug){
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * @param TipoConta $tipoConta
     * @return bool
     */
    public function Editar(TipoConta $tipoConta)
    {
        try{
            $sql = "UPDATE tipo_conta 
                    SET descricao = :descricao
                    WHERE idtipoconta = :id";
            $param = array(
                ":id" => $tipoConta->getId(),
                ":descricao" => $tipoConta->getDescricao()
            );

            return $this->pdo->ExecuteNonQuery($sql,$param);
        }catch (\PDOException $ex){
            if($this->debug){
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * @param $id
     * @return TipoConta|null
     */
    public function ObterTipoContaPorCod($id)
    {
        try{
            $sql = "SELECT * FROM tipo_conta WHERE idtipoconta = :id";
            $param = array(
                ":id" => $id
            );

            $tc = $this->pdo->ExecuteQueryOneRow($sql, $param);

            $tipoConta = new TipoConta();
            $tipoConta->setId($tc['idtipoconta']);
            $tipoConta->setDescricao($tc['descricao']);
            $tipoConta->setAtivo($tc['ativo']);

            return $tipoConta;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

}