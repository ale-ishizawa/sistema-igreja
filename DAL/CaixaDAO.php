<?php
/**
 * Created by PhpStorm.
 * User: DEV02
 * Date: 23/04/2018
 * Time: 17:37
 */
namespace DAL;

use Model\Caixa;

class CaixaDAO
{

    private $pdo;
    private $debug;

    /**
     * CaixaDAO constructor.
     */
    public function __construct()
    {
        $this->pdo = new Conexao();
        $this->debug = true;
    }


    /**
     * VerificaStatusCaixa
     *
     * Verifica se o caixa está aberto ou fechado.
     * Se estiver aberto retorna o id do caixa e o status = 1
     * Se estiver fechado retorna o id do caixa e o status = 2
     * Se o caixa ainda não foi aberto/fechado no dia retorna NULL
     *
     * @return array|null
     */
    public function VerificaStatusCaixa($idIgreja)
    {
        //ALTERAR DATA CAIXA PARA DATE NO BANCO
        try{
            $dataAtual = date('Y-m-d');

            $sql = "SELECT MAX(idcaixa) as ultimoid
                    FROM caixa
                    WHERE datacaixa = :dataatual AND idigreja = :idigreja";
            $param = array(
                ":dataatual" => $dataAtual,
                ":idigreja" => $idIgreja
            );

            $ultimoIdCaixa = $this->pdo->ExecuteQueryOneRow($sql, $param);
            $ultimoIdCaixa = $ultimoIdCaixa['ultimoid'];
            if($ultimoIdCaixa > 0){
                $sql = "SELECT datacaixa, statuscaixa, idcaixa FROM caixa WHERE idcaixa = :idcaixa";
                $param = array(
                    ":idcaixa" => $ultimoIdCaixa
                );

                return $this->pdo->ExecuteQueryOneRow($sql, $param);
            }else{
                return null;
            }
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    public function ObterCaixas($idIgreja)
    {
        try{
            $dataAtual = date('Y-m-d');
            $sql = "SELECT idcaixa, datacaixa, statuscaixa, horaabertura, horafechamento, valorinicial, valorfinal, u.nome as usuarionome
                    FROM caixa 
                    INNER JOIN usuario u ON caixa.idusuario = u.idusuario
                    WHERE caixa.idigreja = :idigreja AND datacaixa = :dataatual";
            $param = array(
                ":idigreja" => $idIgreja,
                ":dataatual" => $dataAtual
            );

            $dados = $this->pdo->ExecuteQuery($sql, $param);
            $listaCaixa = [];

            foreach ($dados as $ca){
                $caixa = new Caixa();
                $caixa->setId($ca['idcaixa']);
                $caixa->setDataCaixa($ca['datacaixa']);
                $caixa->setStatus($ca['statuscaixa']);
                $caixa->setHoraAbertura($ca['horaabertura']);
                $caixa->setHoraFechamento($ca['horafechamento']);
                $caixa->setValorInicial($ca['valorinicial']);
                $caixa->setValorFinal($ca['valorfinal']);
                $caixa->usuario->setNome($ca['usuarionome']);
                $listaCaixa[] = $caixa;
            }

            return $listaCaixa;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }


    public function ResumoCaixa($idIgreja)
    {
        try{
            $sql = "SELECT SUM(valorfinal) as somacaixa FROM caixa WHERE idigreja = :idigreja";
            $param = array(
                ":idigreja" => $idIgreja
            );

            return $this->pdo->ExecuteQueryOneRow($sql, $param);
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    public function AbrirCaixa(Caixa $caixa)
    {
        try{
            $sql = "INSERT INTO caixa(datacaixa, statuscaixa, horaabertura, valorinicial, idigreja, idusuario) 
                    VALUES (:datacaixa, :status, :horaabertura, :valorinicial, :idigreja, :idusuario)";
            $param = array(
                ":datacaixa" => $caixa->getDataCaixa(),
                ":status" => $caixa->getStatus(),
                ":horaabertura" => $caixa->getHoraAbertura(),
                ":valorinicial" => $caixa->getValorInicial(),
                ":idigreja" => $caixa->igreja->getId(),
                ":idusuario" => $caixa->usuario->getId()
            );

            $result = $this->pdo->ExecuteNonQuery($sql, $param);
            if($result){
                $ultimoId = $this->pdo->GetLastID();

                //GERO A MOVIMENTAÇÃO DO VALOR INICIAL NO CAIXA
                $sql = "INSERT INTO movimento_caixa(idcaixa, idigreja, descricao, tipo, valor, idusuario) 
                        VALUES (:idcaixa, :idigreja, :descricao, :tipo, :valor, :idusuario)";
                $param = array(
                    ":idcaixa" => $ultimoId,
                    ":idigreja" => $caixa->igreja->getId(),
                    ":descricao" => "Abertura de Caixa",
                    ":tipo" => 1,
                    ":valor" => $caixa->getValorInicial(),
                    ":idusuario" => $caixa->usuario->getId()
                );

                return $this->pdo->ExecuteNonQuery($sql, $param);
            }else{
                return false;
            }

        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }

    }


    public function FecharCaixa(Caixa $caixa)
    {
        try{
            //SOMA DOS CRÉDITOS
            $sql = "SELECT SUM(valor) as somacreditos FROM movimento_caixa 
                    WHERE idcaixa = :idcaixa AND tipo = 1";
            $param = array(
                ":idcaixa" => $caixa->getId()
            );

            $somaCreditos = $this->pdo->ExecuteQueryOneRow($sql, $param);

            //SOMA DOS DÉBITOS
            $sql = "SELECT SUM(valor) as somadebitos FROM movimento_caixa 
                    WHERE idcaixa = :idcaixa AND tipo = 2";
            $param = array(
                ":idcaixa" => $caixa->getId()
            );

            $somaDebitos = $this->pdo->ExecuteQueryOneRow($sql, $param);

            (float) $valorFinalCaixa = (float) $somaCreditos['somacreditos'] - (float) $somaDebitos['somadebitos'];

            $sql = "INSERT INTO caixa(datacaixa, statuscaixa, horafechamento, valorfinal, idigreja, idusuario) 
                    VALUES (:datacaixa, 2, :fechamento, :valorfinal, :idigreja, :idusuario)";
            $param = array(
                ":datacaixa" => $caixa->getDataCaixa(),
                ":fechamento" => $caixa->getHoraFechamento(),
                ":valorfinal" => $valorFinalCaixa,
                ":idigreja" => $caixa->igreja->getId(),
                ":idusuario" => $caixa->usuario->getId()
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * RelatorioCaixa
     *
     * Retorna uma lista de abertura/fechamento de caixa em um período de uma determinada igreja
     *
     * @param $idIgreja
     * @param $inicio
     * @param $fim
     * @return array|null
     */
    public function RelatorioCaixa($idIgreja, $inicio, $fim)
    {
        try{
            $sql = "SELECT c.datacaixa, c.statuscaixa, c.horaabertura, c.horafechamento, c.valorinicial, c.valorfinal, u.nome 
                    FROM caixa c
                    INNER JOIN usuario u ON c.idusuario = u.idusuario
                    WHERE c.idigreja = :idigreja AND c.datacaixa BETWEEN :inicio AND :fim";
            $param = array(
                ":idigreja" =>$idIgreja,
                ":inicio" => $inicio,
                ":fim" => $fim
            );

            $dados = $this->pdo->ExecuteQuery($sql, $param);
            $listaCaixa = [];

            foreach ($dados as $c){
                $caixa = new Caixa();
                $caixa->setDataCaixa($c['datacaixa']);
                $caixa->setStatus($c['statuscaixa']);
                $caixa->setHoraAbertura($c['horaabertura']);
                $caixa->setHoraFechamento($c['horafechamento']);
                $caixa->setValorInicial($c['valorinicial']);
                $caixa->setValorFinal($c['valorfinal']);
                $caixa->usuario->setNome($c['nome']);
                $listaCaixa[] = $caixa;
            }

            return $listaCaixa;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }
}