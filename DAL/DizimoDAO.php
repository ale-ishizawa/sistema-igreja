<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 04/06/2018
 * Time: 22:14
 */
namespace DAL;
session_start();
use Model\Dizimo;
use DateTime;


class DizimoDAO
{
    /**
     * @var Conexao $pdo
     */
    private $pdo;
    private $debug;

    /**
     * DizimoDAO constructor.
     */
    public function __construct()
    {
        $this->pdo = new Conexao();
        $this->debug = true;
    }

    /**
     * @param Dizimo $dizimo
     * @param $idCaixa
     * @return bool
     */
    public function Gravar(Dizimo $dizimo, $idCaixa)
    {
        try{
            $sql = "INSERT INTO dizimo(ativo, idmembro, valor, data) 
                    VALUES (:ativo, :idmembro, :valor, :datacad)";
            $param = array(
                ":ativo" => $dizimo->getAtivo(),
                ":idmembro" => $dizimo->membro->getId(),
                ":valor" => $dizimo->getValor(),
                ":datacad" => $dizimo->getData()
            );

            if($this->pdo->ExecuteNonQuery($sql, $param)){
                //Compra foi gravada com sucesso, em seguida gravo a movimentação do caixa
                $ultimoIdDizimo = $this->pdo->GetLastID();

                //Cadastro o Movimento de Caixa
                $sql = "INSERT INTO movimento_caixa(idcaixa, idigreja, descricao, tipo, valor, iddizimo, idusuario, datamovimento) 
                            VALUES(:idcaixa, :idigreja, :descricao, :tipo, :valor, :iddizimo, :idusuario, :datamovimento)";
                $param = array(
                    ":idcaixa" => $idCaixa,
                    ":idigreja" => $_SESSION['idigreja'],
                    ":descricao" => 'Lançamento de Dízimo',
                    ":tipo" => 1,
                    ":valor" => $dizimo->getValor(),
                    ":iddizimo" => $ultimoIdDizimo,
                    ":idusuario" => $_SESSION['idusuario'],
                    ":datamovimento" => $dizimo->getData()
                );

                return $this->pdo->ExecuteNonQuery($sql, $param);
            }else{
                return false;
            }
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     *ObterDizimos
     *
     * Obtém os dízimos lançados nos últimos 30 dias de uma determinada igreja
     *
     * @param $idIgreja
     * @return array|null
     * @throws \Exception
     */
    public function ObterDizimos($idIgreja)
    {
        try{
            $dataAtual = new DateTime();
            $interval = new \DateInterval('P1M');
            $dataAtual->sub($interval);
            $dataParam = $dataAtual->format('Y-m-d');

            $sql = "SELECT d.iddizimo, d.ativo, d.valor, d.data, m.nome as membronome
                    FROM dizimo d
                    INNER JOIN movimento_caixa mc ON d.iddizimo = mc.iddizimo
                    INNER JOIN membro m ON d.idmembro = m.idmembro
                    WHERE d.data > :dataparam AND mc.idigreja = :idigreja
                    ORDER BY d.iddizimo DESC";
            $param = array(
                ":dataparam" => $dataParam,
                ":idigreja" => $idIgreja
            );

            $dados = $this->pdo->ExecuteQuery($sql, $param);
            $listaDizimos = [];

            foreach ($dados as $diz){
                $dizimo = new Dizimo();
                $dizimo->setId($diz['iddizimo']);
                $dizimo->setValor($diz['valor']);
                $dizimo->setData($diz['data']);
                $dizimo->setAtivo($diz['ativo']);
                $dizimo->membro->setNome($diz['membronome']);
                $listaDizimos[] = $dizimo;
            }

            return $listaDizimos;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }


    /**
     * RelatorioDizimos
     *
     * Retorna uma lista de dízimos dentro de um período e de uma determinada igreja
     *
     * @param $idIgreja
     * @param $inicio
     * @param $fim
     * @return array|null
     */
    public function RelatorioDizimos($idIgreja, $inicio, $fim)
    {
        try{
            $sql = "SELECT d.iddizimo, d.valor, d.data, m.nome
                    FROM dizimo d 
                    INNER JOIN membro m ON d.idmembro = m.idmembro
                    WHERE m.idigreja = :idigreja AND d.data BETWEEN :inicio AND :fim AND d.ativo = 1";
            $param = array(
                ":idigreja" => $idIgreja,
                ":inicio" => $inicio,
                ":fim" => $fim
            );

            $dados = $this->pdo->ExecuteQuery($sql, $param);
            $listaDizimos = [];

            foreach ($dados as $d){
                $dizimo = new Dizimo();
                $dizimo->setId($d['iddizimo']);
                $dizimo->setData($d['data']);
                $dizimo->setValor($d['valor']);
                $dizimo->membro->setNome($d['nome']);
                $listaDizimos[] = $dizimo;
            }

            return $listaDizimos;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

}