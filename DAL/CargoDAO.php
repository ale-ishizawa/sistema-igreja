<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 07/05/2018
 * Time: 00:20
 */

namespace DAL;

use DAL\Conexao;
use Model\Cargo;

class CargoDAO
{

    private $pdo;
    private $debug;

    /**
     * CargoDAO constructor.
     */
    public function __construct()
    {
        $this->pdo = new Conexao();
        $this->debug = true;
    }

    /**
     * @param Cargo $cargo
     * @return bool
     */
    public function Gravar(Cargo $cargo)
    {
        try{
            $sql = "INSERT INTO cargo (nome, descricao) VALUES (:nome, :desc)";
            $param = array(
                ":nome" => $cargo->getNome(),
                ":desc" => $cargo->getDescricao()
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        }catch (\PDOException $ex){
            if($this->debug){
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * @param Cargo $cargo
     * @return bool
     */
    public function Editar(Cargo $cargo)
    {
        try{
            $sql = "UPDATE cargo 
                    SET nome = :nome,
                    descricao = :descricao 
                    WHERE idcargo = :id";
            $param = array(
                ":nome" => $cargo->getNome(),
                ":descricao" => $cargo->getDescricao(),
                ":id" => $cargo->getId()
            );

            return $this->pdo->ExecuteNonQuery($sql,$param);
        }catch (\PDOException $ex){
            if($this->debug){
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * Deletar
     *
     * Exclui um cargo do banco de dados, e antes atualiza todos os membros que possuem o cargo
     * para que não tenha erros de FK
     *
     * @param $id
     * @return bool
     */
    public function Deletar($id)
    {
        try{
            $sql = "UPDATE membro SET idcargo = NULL WHERE idcargo = :id";
            $param = array(
                ":id" => $id
            );
            $this->pdo->ExecuteNonQuery($sql, $param);

            $sql = "DELETE FROM cargo WHERE idcargo = :id";

            return $this->pdo->ExecuteNonQuery($sql, $param);
        }catch (\PDOException $ex){
            if($this->debug){
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * @return array|null
     */
    public function ObterCargos()
    {
        try{
            $sql = "SELECT * FROM cargo ORDER BY nome ASC";

            $dados = $this->pdo->ExecuteQuery($sql);
            $listaCargos = [];

            foreach ($dados as $d){
                $cargo = new Cargo();
                $cargo->setNome($d['nome']);
                $cargo->setDescricao($d['descricao']);
                $cargo->setId($d['idcargo']);
                $listaCargos[] = $cargo;
            }

            return $listaCargos;
        }catch (PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    /**
     * @return array|null
     */
    public function ObterCargosSelect()
    {
        try{
            $sql = "SELECT idcargo, nome FROM cargo ORDER BY nome ASC";

            $dados = $this->pdo->ExecuteQuery($sql);
            $listaCargos = [];

            foreach ($dados as $c) {
                $cargo = new Cargo();
                $cargo->setId($c['idcargo']);
                $cargo->setNome($c['nome']);
                $listaCargos[] = $cargo;
            }

            return $listaCargos;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    /**
     * @param $id
     * @return Cargo|null
     */
    public function ObterCargoPorCod($id)
    {
        try{
            $sql = "SELECT * FROM cargo WHERE idcargo = :id";
            $param = array(
                ":id" => $id
            );

            $c = $this->pdo->ExecuteQueryOneRow($sql, $param);

            $cargo = new Cargo();
            $cargo->setId($c['idcargo']);
            $cargo->setNome($c['nome']);
            $cargo->setDescricao($c['descricao']);

            return $cargo;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

}