<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 13/05/2018
 * Time: 15:36
 */
namespace DAL;
session_start();
use Model\Compra;
use DateTime;

class CompraDAO
{
    /**
     * @var Conexao $pdo
     */
    private $pdo;
    private $debug;

    /**
     * CompraDAO constructor.
     */
    public function __construct()
    {
        $this->pdo = new Conexao();
        $this->debug = true;
    }

    public function Gravar(Compra $compra)
    {
        try{
            $sql = "INSERT INTO compra(datacompra, datacadastro, valor, descricao, idigreja, ativo) 
                    VALUES (:datacompra, :datacadastro, :valor, :descricao, :igreja, 1)";
            $param = array(
                ":datacompra" => $compra->getDataCompra(),
                ":valor" => $compra->getValor(),
                ":descricao" => $compra->getDescricao(),
                ":igreja" => $compra->igreja->getId(),
                ":datacadastro" => $compra->getDataCadastro()
            );

            $retorno = $this->pdo->ExecuteNonQuery($sql, $param);
            if($retorno == true){
                //Compra foi gravada com sucesso, em seguida gravo a movimentação do caixa
                $ultimoIdCompra = $this->pdo->GetLastID();

                $caixaDAO = new CaixaDAO();
                $idCaixa = $caixaDAO->VerificaStatusCaixa($compra->igreja->getId());

                if($idCaixa != false && $idCaixa['statuscaixa'] == 1){
                    //Cadastro o Movimento de Caixa
                    $sql = "INSERT INTO movimento_caixa(idcaixa, idigreja, descricao, tipo, valor, idcompra, idusuario, datamovimento) 
                            VALUES(:idcaixa, :idigreja, :descricao, :tipo, :valor, :idcompra, :idusuario, :datamovimento)";
                    $param = array(
                        ":idcaixa" => $idCaixa['idcaixa'],
                        ":idigreja" => $compra->igreja->getId(),
                        ":descricao" => $compra->getDescricao(),
                        ":tipo" => 2,
                        ":valor" => $compra->getValor(),
                        ":idcompra" => $ultimoIdCompra,
                        ":idusuario" => $_SESSION['idusuario'],
                        ":datamovimento" => $compra->getDataCompra()
                    );

                    return $this->pdo->ExecuteNonQuery($sql, $param);
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * ObterCompras
     *
     * Obtém as compras realizadas nos últimos 30 dias daquela igreja.
     *
     * @param $idIgrejaSessao
     * @return array|null
     * @throws \Exception
     */
    public function ObterCompras($idIgrejaSessao)
    {
        try{
            $dataAtual = new DateTime();
            $interval = new \DateInterval('P1M');
            $dataAtual->sub($interval);

            $dataParam = $dataAtual->format('Y-m-d');

            $sql = "SELECT * FROM compra WHERE datacompra > :dataparam AND idigreja = :idigreja";
            $param = array(
                ":dataparam" => $dataParam,
                ":idigreja" => $idIgrejaSessao
            );

            $dados =  $this->pdo->ExecuteQuery($sql, $param);
            $listaCompras = [];

            foreach ($dados as $com){
                $compra = new Compra();
                $compra->setId($com['idcompra']);
                $compra->igreja->setId($com)['idigreja'];
                $compra->setDescricao($com['descricao']);
                $compra->setValor($com['valor']);
                $compra->setDataCompra($com['datacompra']);
                $compra->setDataCadastro($com['datacadastro']);
                $compra->setAtivo($com['ativo']);
                $listaCompras[] = $compra;
            }

            return $listaCompras;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    /**
     * Estornar
     *
     * Realiza o estorno da compra.
     *
     * @param $idCompra
     * @return bool
     */
    public function Estornar($idCompra, $idCaixa)
    {
        try{
            $compra = $this->ObterCompra($idCompra);
            if($compra != NULL){
                //Realiza o estorno
                $dataAtual = date('Y-m-d H:i:s');
                $sql = "UPDATE compra 
                        SET datacadastro = :dataatual, 
                        ativo = 3
                        WHERE idcompra = :id";
                $param = array(
                    ":dataatual" => $dataAtual,
                    ":id" => $compra->getId()
                );

                if($this->pdo->ExecuteNonQuery($sql, $param)){
                    $movimentoDAO = new MovimentoCaixaDAO();
                    $estorno = $movimentoDAO->EstornarCompra($compra, $idCaixa);

                    return $estorno;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * @param $id
     * @return Compra|null
     */
    public function ObterCompra($id)
    {
        try{
            $sql = "SELECT idcompra, datacompra, valor, idigreja FROM compra
                    WHERE idcompra = :id";
            $param = array(
                ":id" => $id
            );

            $c = $this->pdo->ExecuteQueryOneRow($sql, $param);

            $compra = new Compra();
            $compra->setId($c['idcompra']);
            $compra->setDataCompra($c['datacompra']);
            $compra->setValor($c['valor']);
            $compra->igreja->setId($c['idigreja']);

            return $compra;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    /**
     * RelatorioCompras
     *
     * Retorna uma lista de compras em um determinado período
     *
     * @param $idIgreja
     * @param $inicio
     * @param $fim
     * @return array|null
     */
    public function RelatorioCompras($idIgreja, $inicio, $fim)
    {
        try{
            $sql = "SELECT c.idcompra, c.datacompra, c.valor, c.descricao, u.nome as usuarioNome
                    FROM compra c
                    INNER JOIN movimento_caixa mc ON c.idcompra = mc.idcompra 
                    INNER JOIN usuario u ON mc.idusuario = u.idusuario
                    WHERE datacompra BETWEEN :inicio AND :fim AND c.ativo = 1 AND c.idigreja = :idigreja";
            $param = array(
                ":inicio" => $inicio,
                ":fim" => $fim,
                ":idigreja" => $idIgreja
            );

            $dados = $this->pdo->ExecuteQuery($sql, $param);
            $listaCompras = [];

            foreach ($dados as $c){
                $compra = new Compra();
                $compra->setId($c['idcompra']);
                $compra->setDataCompra($c['datacompra']);
                $compra->setValor($c['valor']);
                $compra->setDescricao($c['descricao']);
                $compra->setUsuario($c['usuarioNome']);
                $listaCompras[] = $compra;
            }

            return $listaCompras;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }
}