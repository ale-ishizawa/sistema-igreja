--
-- ER/Studio Developer Edition SQL Code Generation
-- Project :      igreja.DM1
--
-- Date Created : Monday, June 11, 2018 19:25:01
-- Target DBMS : MySQL 5.x
--

-- 
-- TABLE: caixa 
--

CREATE TABLE caixa(
    idcaixa           INT               AUTO_INCREMENT,
    datacaixa         DATE              NOT NULL,
    statuscaixa       INT               NOT NULL,
    horaabertura      DATETIME,
    horafechamento    DATETIME,
    valorinicial      DECIMAL(12, 2),
    valorfinal        DECIMAL(12, 2),
    idigreja          INT               NOT NULL,
    idusuario         INT               NOT NULL,
    PRIMARY KEY (idcaixa)
)ENGINE=INNODB
;



-- 
-- TABLE: campanha 
--

CREATE TABLE campanha(
    idcampanha    INT               AUTO_INCREMENT,
    ativo         INT,
    datainicio    TIMESTAMP,
    datafim       TIMESTAMP,
    descricao     VARCHAR(255),
    nome          VARCHAR(60),
    valoralvo     DECIMAL(12, 2),
    idigreja      INT               NOT NULL,
    PRIMARY KEY (idcampanha)
)ENGINE=INNODB
;



-- 
-- TABLE: cargo 
--

CREATE TABLE cargo(
    idcargo      INT             AUTO_INCREMENT,
    nome         VARCHAR(100)    NOT NULL,
    descricao    VARCHAR(255),
    PRIMARY KEY (idcargo)
)ENGINE=INNODB
;



-- 
-- TABLE: cidade 
--

CREATE TABLE cidade(
    idcidade    INT            AUTO_INCREMENT,
    nome        VARCHAR(60),
    uf          CHAR(2),
    PRIMARY KEY (idcidade)
)ENGINE=INNODB
;



-- 
-- TABLE: classe_ebd 
--

CREATE TABLE classe_ebd(
    idclasse            INT             AUTO_INCREMENT,
    datainicio          DATE,
    dataencerramento    DATE,
    nome                VARCHAR(100),
    descricao           VARCHAR(255),
    PRIMARY KEY (idclasse)
)ENGINE=INNODB
;



-- 
-- TABLE: compra 
--

CREATE TABLE compra(
    idcompra        INT               AUTO_INCREMENT,
    ativo           INT               NOT NULL,
    datacadastro    DATETIME,
    datacompra      DATE,
    valor           DECIMAL(12, 2)    NOT NULL,
    descricao       VARCHAR(255),
    idigreja        INT               NOT NULL,
    PRIMARY KEY (idcompra)
)ENGINE=INNODB
;



-- 
-- TABLE: conta_pagar 
--

CREATE TABLE conta_pagar(
    idcontapagar      INT               AUTO_INCREMENT,
    ativo             INT               NOT NULL,
    datavencimento    TIMESTAMP,
    datapagamento     TIMESTAMP,
    valor             DECIMAL(12, 2),
    idtipoconta       INT               NOT NULL,
    idigreja          INT               NOT NULL,
    PRIMARY KEY (idcontapagar)
)ENGINE=INNODB
;



-- 
-- TABLE: dizimo 
--

CREATE TABLE dizimo(
    iddizimo    INT               AUTO_INCREMENT,
    ativo       INT               NOT NULL,
    idmembro    INT,
    valor       DECIMAL(12, 2)    NOT NULL,
    data        TIMESTAMP,
    PRIMARY KEY (iddizimo)
)ENGINE=INNODB
;



-- 
-- TABLE: igreja 
--

CREATE TABLE igreja(
    idigreja       INT             AUTO_INCREMENT,
    ativo          INT,
    telefone       VARCHAR(15),
    email          VARCHAR(60),
    site           VARCHAR(100),
    nome           VARCHAR(100),
    cnpj           VARCHAR(20),
    ie             CHAR(20),
    sede           INT,
    logradouro     VARCHAR(60),
    numero         VARCHAR(10),
    complemento    VARCHAR(20),
    bairro         VARCHAR(45),
    cep            VARCHAR(10),
    idcidade       INT             NOT NULL,
    PRIMARY KEY (idigreja)
)ENGINE=INNODB
;



-- 
-- TABLE: lider 
--

CREATE TABLE lider(
    idmembro    INT    NOT NULL,
    PRIMARY KEY (idmembro)
)ENGINE=INNODB
;



-- 
-- TABLE: membro 
--

CREATE TABLE membro(
    idmembro         INT             AUTO_INCREMENT,
    ativo            INT,
    nome             VARCHAR(50)     NOT NULL,
    sexo             CHAR(1)         NOT NULL,
    logradouro       VARCHAR(100),
    numero           VARCHAR(10),
    bairro           VARCHAR(45),
    complemento      VARCHAR(45),
    cep              VARCHAR(10),
    telefone         VARCHAR(20),
    celular          VARCHAR(20),
    email            VARCHAR(60),
    nivelescolar     INT,
    nascimento       TIMESTAMP       NOT NULL,
    naturalidade     VARCHAR(60),
    rg               VARCHAR(20),
    orgao            CHAR(3),
    cpf              VARCHAR(15),
    estadocivil      INT             NOT NULL,
    conjuge          VARCHAR(60),
    datacasamento    TIMESTAMP,
    nomepai          VARCHAR(60),
    nomemae          VARCHAR(60),
    databatismo      TIMESTAMP,
    igrejaorigem     VARCHAR(60),
    idigreja         INT             NOT NULL,
    idcargo          INT,
    idcidade         INT             NOT NULL,
    PRIMARY KEY (idmembro)
)ENGINE=INNODB
;



-- 
-- TABLE: membros_ministerio 
--

CREATE TABLE membros_ministerio(
    idmembro        INT    NOT NULL,
    idministerio    INT    NOT NULL,
    PRIMARY KEY (idmembro, idministerio)
)ENGINE=INNODB
;



-- 
-- TABLE: ministerio 
--

CREATE TABLE ministerio(
    idministerio    INT             AUTO_INCREMENT,
    idmembro        INT             NOT NULL,
    nome            VARCHAR(100)    NOT NULL,
    descricao       VARCHAR(255),
    idigreja        INT             NOT NULL,
    PRIMARY KEY (idministerio)
)ENGINE=INNODB
;



-- 
-- TABLE: movimento_caixa 
--

CREATE TABLE movimento_caixa(
    idmovimentocaixa    INT               AUTO_INCREMENT,
    idcaixa             INT               NOT NULL,
    idigreja            INT               NOT NULL,
    datamovimento       DATETIME,
    descricao           VARCHAR(100),
    tipo                INT,
    valor               DECIMAL(12, 2),
    idcompra            INT,
    idcontapagar        INT,
    idoferta            INT,
    iddizimo            INT,
    idusuario           INT,
    PRIMARY KEY (idmovimentocaixa, idcaixa, idigreja)
)ENGINE=INNODB
;



-- 
-- TABLE: oferta 
--

CREATE TABLE oferta(
    idoferta      INT               AUTO_INCREMENT,
    ativo         INT               NOT NULL,
    valor         DECIMAL(12, 2)    NOT NULL,
    data          TIMESTAMP,
    idmembro      INT,
    idcampanha    INT               NOT NULL,
    PRIMARY KEY (idoferta)
)ENGINE=INNODB
;



-- 
-- TABLE: pastor 
--

CREATE TABLE pastor(
    idmembro    INT    NOT NULL,
    PRIMARY KEY (idmembro)
)ENGINE=INNODB
;



-- 
-- TABLE: tipo_conta 
--

CREATE TABLE tipo_conta(
    idtipoconta    INT            AUTO_INCREMENT,
    ativo          INT,
    descricao      VARCHAR(60)    NOT NULL,
    PRIMARY KEY (idtipoconta)
)ENGINE=INNODB
;



-- 
-- TABLE: turma 
--

CREATE TABLE turma(
    idmembro    INT    NOT NULL,
    idclasse    INT    NOT NULL,
    PRIMARY KEY (idmembro, idclasse)
)ENGINE=INNODB
;



-- 
-- TABLE: usuario 
--

CREATE TABLE usuario(
    idusuario    INT             AUTO_INCREMENT,
    usuario      VARCHAR(60),
    nome         VARCHAR(100)    NOT NULL,
    senha        VARCHAR(255),
    email        VARCHAR(100),
    ativo        INT             NOT NULL,
    perfil       INT,
    idigreja     INT,
    PRIMARY KEY (idusuario)
)ENGINE=INNODB
;



-- 
-- TABLE: caixa 
--

ALTER TABLE caixa ADD CONSTRAINT Refigreja34 
    FOREIGN KEY (idigreja)
    REFERENCES igreja(idigreja)
;

ALTER TABLE caixa ADD CONSTRAINT Refusuario36 
    FOREIGN KEY (idusuario)
    REFERENCES usuario(idusuario)
;


-- 
-- TABLE: campanha 
--

ALTER TABLE campanha ADD CONSTRAINT Refigreja4 
    FOREIGN KEY (idigreja)
    REFERENCES igreja(idigreja)
;


-- 
-- TABLE: compra 
--

ALTER TABLE compra ADD CONSTRAINT Refigreja14 
    FOREIGN KEY (idigreja)
    REFERENCES igreja(idigreja)
;


-- 
-- TABLE: conta_pagar 
--

ALTER TABLE conta_pagar ADD CONSTRAINT Reftipo_conta20 
    FOREIGN KEY (idtipoconta)
    REFERENCES tipo_conta(idtipoconta)
;

ALTER TABLE conta_pagar ADD CONSTRAINT Refigreja27 
    FOREIGN KEY (idigreja)
    REFERENCES igreja(idigreja)
;


-- 
-- TABLE: dizimo 
--

ALTER TABLE dizimo ADD CONSTRAINT Refmembro12 
    FOREIGN KEY (idmembro)
    REFERENCES membro(idmembro)
;


-- 
-- TABLE: igreja 
--

ALTER TABLE igreja ADD CONSTRAINT Refcidade32 
    FOREIGN KEY (idcidade)
    REFERENCES cidade(idcidade)
;


-- 
-- TABLE: lider 
--

ALTER TABLE lider ADD CONSTRAINT Refmembro9 
    FOREIGN KEY (idmembro)
    REFERENCES membro(idmembro)
;


-- 
-- TABLE: membro 
--

ALTER TABLE membro ADD CONSTRAINT Refigreja3 
    FOREIGN KEY (idigreja)
    REFERENCES igreja(idigreja)
;

ALTER TABLE membro ADD CONSTRAINT Refcargo8 
    FOREIGN KEY (idcargo)
    REFERENCES cargo(idcargo)
;

ALTER TABLE membro ADD CONSTRAINT Refcidade30 
    FOREIGN KEY (idcidade)
    REFERENCES cidade(idcidade)
;


-- 
-- TABLE: membros_ministerio 
--

ALTER TABLE membros_ministerio ADD CONSTRAINT Refministerio6 
    FOREIGN KEY (idministerio)
    REFERENCES ministerio(idministerio)
;

ALTER TABLE membros_ministerio ADD CONSTRAINT Refmembro7 
    FOREIGN KEY (idmembro)
    REFERENCES membro(idmembro)
;


-- 
-- TABLE: ministerio 
--

ALTER TABLE ministerio ADD CONSTRAINT Refigreja41 
    FOREIGN KEY (idigreja)
    REFERENCES igreja(idigreja)
;

ALTER TABLE ministerio ADD CONSTRAINT Reflider39 
    FOREIGN KEY (idmembro)
    REFERENCES lider(idmembro)
;


-- 
-- TABLE: movimento_caixa 
--

ALTER TABLE movimento_caixa ADD CONSTRAINT Refusuario43 
    FOREIGN KEY (idusuario)
    REFERENCES usuario(idusuario)
;

ALTER TABLE movimento_caixa ADD CONSTRAINT Refigreja44 
    FOREIGN KEY (idigreja)
    REFERENCES igreja(idigreja)
;

ALTER TABLE movimento_caixa ADD CONSTRAINT Refcaixa18 
    FOREIGN KEY (idcaixa)
    REFERENCES caixa(idcaixa)
;

ALTER TABLE movimento_caixa ADD CONSTRAINT Refcompra19 
    FOREIGN KEY (idcompra)
    REFERENCES compra(idcompra)
;

ALTER TABLE movimento_caixa ADD CONSTRAINT Refconta_pagar23 
    FOREIGN KEY (idcontapagar)
    REFERENCES conta_pagar(idcontapagar)
;

ALTER TABLE movimento_caixa ADD CONSTRAINT Refoferta24 
    FOREIGN KEY (idoferta)
    REFERENCES oferta(idoferta)
;

ALTER TABLE movimento_caixa ADD CONSTRAINT Refdizimo25 
    FOREIGN KEY (iddizimo)
    REFERENCES dizimo(iddizimo)
;


-- 
-- TABLE: oferta 
--

ALTER TABLE oferta ADD CONSTRAINT Refmembro13 
    FOREIGN KEY (idmembro)
    REFERENCES membro(idmembro)
;

ALTER TABLE oferta ADD CONSTRAINT Refcampanha28 
    FOREIGN KEY (idcampanha)
    REFERENCES campanha(idcampanha)
;


-- 
-- TABLE: pastor 
--

ALTER TABLE pastor ADD CONSTRAINT Refmembro2 
    FOREIGN KEY (idmembro)
    REFERENCES membro(idmembro)
;


-- 
-- TABLE: turma 
--

ALTER TABLE turma ADD CONSTRAINT Refmembro10 
    FOREIGN KEY (idmembro)
    REFERENCES membro(idmembro)
;

ALTER TABLE turma ADD CONSTRAINT Refclasse_ebd11 
    FOREIGN KEY (idclasse)
    REFERENCES classe_ebd(idclasse)
;


-- 
-- TABLE: usuario 
--

ALTER TABLE usuario ADD CONSTRAINT Refigreja42 
    FOREIGN KEY (idigreja)
    REFERENCES igreja(idigreja)
;


