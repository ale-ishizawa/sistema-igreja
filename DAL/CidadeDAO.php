<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 14/05/2018
 * Time: 01:02
 */

namespace DAL;

use DAL\Conexao;
use Model\Cidade;

class CidadeDAO
{
    /**
     * @var Conexao $pdo
     */
    private $pdo;
    private $debug;

    /**
     * CidadeDAO constructor.
     */
    public function __construct()
    {
        $this->pdo = new Conexao();
        $this->debug = true;
    }

    public function ObterCidade(Cidade $cidade)
    {
        try {
            $sql = "SELECT idcidade, nome, uf FROM cidade WHERE nome = :nome AND uf = :uf";
            $param = array(
                ":nome" => $cidade->getNome(),
                ":uf" => $cidade->getUf()
            );

            $dt = $this->pdo->ExecuteQueryOneRow($sql, $param);

            $cid = new Cidade();
            $cid->setId($dt['idcidade']);
            $cid->setUf($dt['uf']);
            $cid->setNome($dt['nome']);

            return $cid;

        } catch (\PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    public function Gravar(Cidade $cidade)
    {
        try {
            $sql = "INSERT INTO cidade(nome, uf) VALUES(:nome, :uf)";
            $param = array(
                ":nome" => $cidade->getNome(),
                ":uf" => $cidade->getUf()
            );
            return $this->pdo->ExecuteNonQuery($sql, $param);
        } catch (\PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

}