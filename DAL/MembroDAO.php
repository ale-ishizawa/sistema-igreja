<?php
/**
 * Created by PhpStorm.
 * User: Alessandro
 * Date: 17/05/2018
 * Time: 16:19
 */

namespace DAL;

use Model\Membro;
use Model\Cidade;

class MembroDAO
{
    private $pdo;
    private $debug;
    private $cidDAO;

    /**
     * MembroDAO constructor.
     */
    public function __construct()
    {
        $this->pdo = new Conexao();
        $this->debug = true;
        $this->cidDAO = new CidadeDAO();
    }

    /**
     * Retorna uma lista de membros para exibição no Autocomplete
     *
     * @param $nome
     * @return null|string
     */
    public function ObterMembrosAutocomplete($nome, $idIgreja)
    {
        sleep(1);
        try {
            $sql = "SELECT idmembro, nome FROM membro WHERE nome LIKE '%:nome%' AND idigreja = :idigreja";
            $param = array(
                ":nome" => $nome,
                ":idigreja" => $idIgreja
            );

            $dados = $this->pdo->ExecuteQuery($sql, $param);
            $listaMembros = [];

            foreach ($dados as $m) {
                $membro = new Membro();
                $membro->setId($m['idmembro']);
                $membro->setNome($m['nome']);
                $listaMembros[] = $membro;
            }

            return json_encode($listaMembros);

        } catch (\PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }


    /**
     * ObterMembroPorCod
     *
     * Retorna um objeto Membro para edição dos dados
     *
     * @param $id
     * @return Membro|null
     */
    public function ObterMembroPorCod($id)
    {
        try {
            $sql = "SELECT m.idmembro, m.nome as membronome, m.ativo, m.sexo, m.logradouro, m.numero, m.bairro, m.complemento, m.cep, m.telefone, m.celular, m.email,
                    m.nivelescolar, m.nascimento, m.naturalidade, m.rg, m.orgao, m.cpf, m.estadocivil, m.conjuge, m.datacasamento, m.nomepai, m.nomemae,
                    m.databatismo, m.databatismo, m.igrejaorigem, m.idigreja, m.idcargo, m.idcidade, ca.nome as cargonome, c.nome as cidadenome, c.uf
                    FROM membro m
                    LEFT JOIN cargo ca ON m.idcargo = ca.idcargo
                    INNER JOIN cidade c ON m.idcidade = c.idcidade
                    WHERE idmembro = :id";
            $param = array(
                ":id" => $id
            );

            $m = $this->pdo->ExecuteQueryOneRow($sql, $param);

            $membro = new Membro();
            $membro->setId($m['idmembro']);
            $membro->setNome($m['membronome']);
            $membro->setAtivo($m['ativo']);
            $membro->setSexo($m['sexo']);
            $membro->setLogradouro($m['logradouro']);
            $membro->setNumero($m['numero']);
            $membro->setBairro($m['bairro']);
            $membro->setComplemento($m['complemento']);
            $membro->setCep($m['cep']);
            $membro->setTelefone($m['telefone']);
            $membro->setCelular($m['celular']);
            $membro->setEmail($m['email']);
            $membro->setNivelEscolar($m['nivelescolar']);
            $membro->setNascimento($m['nascimento']);
            $membro->setNaturalidade($m['naturalidade']);
            $membro->setRg($m['rg']);
            $membro->setOrgao($m['orgao']);
            $membro->setCpf($m['cpf']);
            $membro->setEstadoCivil($m['estadocivil']);
            $membro->setConjuge($m['conjuge']);
            $membro->setDataCasamento($m['datacasamento']);
            $membro->setNomePai($m['nomepai']);
            $membro->setNomeMae($m['nomemae']);
            $membro->setDataBatismo($m['databatismo']);
            $membro->setIgrejaOrigem($m['igrejaorigem']);
            $membro->igreja->setId($m['idigreja']);
            $membro->cargo->setId($m['idcargo']);
            $membro->cargo->setNome($m['cargonome']);
            $membro->cidade->setId($m['idcidade']);
            $membro->cidade->setNome($m['cidadenome']);
            $membro->cidade->setUf($m['uf']);

            return $membro;
        } catch (\PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    /**
     * @param Membro $membro
     * @return bool
     */
    public function Gravar(Membro $membro)
    {
        try {
            $cidade = new Cidade();
            $cidade->setNome($membro->cidade->getNome());
            $cidade->setUf($membro->cidade->getUf());

            //Verifico se a cidade já existe no banco de dados, se existir não é necessário realizar o cadastro
            $existe = $this->cidDAO->ObterCidade($cidade);

            if ($existe->getId() == NULL) {
                //Gravo a cidade
                $retorno = $this->cidDAO->Gravar($cidade);
                if ($retorno == true) {
                    $membro->cidade->setId($this->pdo->GetLastID());
                }
            } else {
                $membro->cidade->setId($existe->getId());
            }

            $sql = "INSERT INTO membro(ativo, nome, sexo, logradouro, numero, bairro, complemento, cep, telefone, celular, email, nivelescolar, nascimento, naturalidade, rg, orgao, cpf, estadocivil, conjuge, 
                                       datacasamento, nomepai, nomemae, databatismo, igrejaorigem, idigreja, idcargo, idcidade) VALUES(:ativo, :nome, :sexo, :logradouro, :numero, :bairro, :complemento, :cep, 
                                       :telefone, :celular, :email, :nivelescolar, :nascimento, :naturalidade, :rg, :orgao, :cpf, :estadocivil, :conjuge, :datacasamento, :nomepai, :nomemae, :databatismo, 
                                       :igrejaorigem, :idigreja, :idcargo, :idcidade)";
            $param = array(
                ":ativo" => $membro->getAtivo(),
                ":nome" => $membro->getNome(),
                ":sexo" => $membro->getSexo(),
                ":logradouro" => $membro->getLogradouro(),
                ":numero" => $membro->getNumero(),
                ":bairro" => $membro->getBairro(),
                ":complemento" => $membro->getComplemento(),
                ":cep" => $membro->getCep(),
                ":telefone" => $membro->getTelefone(),
                ":celular" => $membro->getCelular(),
                ":email" => $membro->getEmail(),
                ":nivelescolar" => $membro->getNivelEscolar(),
                ":nascimento" => $membro->getNascimento(),
                ":naturalidade" => $membro->getNaturalidade(),
                ":rg" => $membro->getRg(),
                ":orgao" => $membro->getOrgao(),
                ":cpf" => $membro->getCpf(),
                ":estadocivil" => $membro->getEstadoCivil(),
                ":conjuge" => $membro->getConjuge(),
                ":datacasamento" => $membro->getDataCasamento(),
                ":nomepai" => $membro->getNomePai(),
                ":nomemae" => $membro->getNomeMae(),
                ":databatismo" => $membro->getDataBatismo(),
                ":igrejaorigem" => $membro->getIgrejaOrigem(),
                ":idigreja" => $membro->igreja->getId(),
                ":idcargo" => $membro->cargo->getId(),
                ":idcidade" => $membro->cidade->getId()
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        } catch (\PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    public function Editar(Membro $membro)
    {
        try {
            $cidade = new Cidade();
            $cidade->setNome($membro->cidade->getNome());
            $cidade->setUf($membro->cidade->getUf());

            //Verifico se a cidade já existe no banco de dados, se existir não é necessário realizar o cadastro
            $existe = $this->cidDAO->ObterCidade($cidade);

            if ($existe->getId() == NULL) {
                //Gravo a cidade
                $retorno = $this->cidDAO->Gravar($cidade);
                if ($retorno == true) {
                    $membro->cidade->setId($this->pdo->GetLastID());
                }
            } else {
                $membro->cidade->setId($existe->getId());
            }

            $sql = "UPDATE membro 
                    SET ativo = :ativo, 
                    nome = :nome, 
                    sexo = :sexo, 
                    logradouro = :logradouro,
                    numero = :numero, 
                    bairro = :bairro, 
                    complemento = :complemento, 
                    cep = :cep,
                    telefone = :telefone, 
                    celular = :celular, 
                    email = :email, 
                    nivelescolar = :nivelescolar,
                    nascimento = :nascimento,
                    naturalidade = :naturalidade, 
                    rg = :rg,
                    orgao = :orgao, 
                    cpf = :cpf,
                    estadocivil = :estadocivil,
                    conjuge = :conjuge, 
                    datacasamento = :datacasamento,
                    nomepai = :nomepai, 
                    nomemae = :nomemae, 
                    databatismo = :databatismo,
                    igrejaorigem = :igrejaorigem, 
                    idigreja = :idigreja,
                    idcargo = :idcargo,
                    idcidade = :idcidade 
                    WHERE idmembro = :idmembro";
            $param = array(
                ":ativo" => $membro->getAtivo(),
                ":nome" => $membro->getNome(),
                ":sexo" => $membro->getSexo(),
                ":logradouro" => $membro->getLogradouro(),
                ":numero" => $membro->getNumero(),
                ":bairro" => $membro->getBairro(),
                ":complemento" => $membro->getComplemento(),
                ":cep" => $membro->getCep(),
                ":telefone" => $membro->getTelefone(),
                ":celular" => $membro->getCelular(),
                ":email" => $membro->getEmail(),
                ":nivelescolar" => $membro->getNivelEscolar(),
                ":nascimento" => $membro->getNascimento(),
                ":naturalidade" => $membro->getNaturalidade(),
                ":rg" => $membro->getRg(),
                ":orgao" => $membro->getOrgao(),
                ":cpf" => $membro->getCpf(),
                ":estadocivil" => $membro->getEstadoCivil(),
                ":conjuge" => $membro->getConjuge(),
                ":datacasamento" => $membro->getDataCasamento(),
                ":nomepai" => $membro->getNomePai(),
                ":nomemae" => $membro->getNomeMae(),
                ":databatismo" => $membro->getDataBatismo(),
                ":igrejaorigem" => $membro->getIgrejaOrigem(),
                ":idigreja" => $membro->igreja->getId(),
                ":idcargo" => $membro->cargo->getId(),
                ":idcidade" => $membro->cidade->getId(),
                ":idmembro" => $membro->getId()
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        } catch (\PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }


    /**
     * ObterMembros
     *
     * Retorna lista de objetos de Membro relacionados à igreja que o usuário logado está relacionado para exibição em tabela
     *
     * @param $idIgreja
     * @return array|null
     */
    public function ObterMembros($idIgreja)
    {
        try {
            $sql = "SELECT m.idmembro, m.nome, m.celular, m.telefone, m.email, m.nascimento, m.databatismo, m.idcargo, m.logradouro, m.numero, m.bairro, c2.nome AS nomecargo
                    FROM membro m
                    LEFT JOIN cargo c2 ON m.idcargo = c2.idcargo
                    WHERE idigreja = :idigreja AND ativo = 1";
            $param = array(
                ":idigreja" => $idIgreja
            );

            $dados = $this->pdo->ExecuteQuery($sql, $param);
            $listaMembros = [];

            foreach ($dados as $m) {
                $membro = new Membro();
                $membro->setId($m['idmembro']);
                $membro->setNome($m['nome']);
                $membro->setTelefone($m['telefone']);
                $membro->setCelular($m['celular']);
                $membro->setEmail($m['email']);
                $membro->setNascimento($m['nascimento']);
                $membro->setDataBatismo($m['databatismo']);
                $membro->setLogradouro($m['logradouro']);
                $membro->setNumero($m['numero']);
                $membro->setBairro($m['bairro']);
                $membro->cargo->setId($m['idcargo']);
                $membro->cargo->setNome($m['nomecargo']);
                $listaMembros[] = $membro;
            }

            return $listaMembros;
        } catch (\PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    /**
     * Inativa um membro do roll de membros da igreja
     *
     * @param $id
     * @return bool
     */
    public function Inativar($id)
    {
        try {
            $sql = "UPDATE membro SET ativo = 2 WHERE idmembro = :id";
            $param = array(
                ":id" => $id
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);

        } catch (\PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * @param $idIgreja
     * @return array|null
     */
    public function CountMembros($idIgreja)
    {
        try{
            $sql = "SELECT COUNT(*) FROM membro WHERE idigreja = :idigreja";
            $param = array(
                ":idigreja" => $idIgreja
            );

            return $this->pdo->ExecuteQuery($sql, $param);
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }


    /**
     * @param $idIgreja
     * @param $sexo
     * @param $situacao
     * @return array|null
     */
    public function RelatorioMembros($idIgreja, $sexo, $situacao)
    {
        try{
            $where = "";
            if($sexo != "0"){
                $where = " AND sexo = '".$sexo."' ";
            }
            
            $sql = "SELECT idmembro, nome, logradouro, numero, bairro, celular, email, sexo, ativo
                    FROM membro
                    WHERE idigreja = :idigreja AND ativo = :situacao".$where;
            $param = array(
                ":idigreja" => $idIgreja,
                ":situacao" => $situacao
            );

            return $this->pdo->ExecuteQuery($sql, $param);
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }
}