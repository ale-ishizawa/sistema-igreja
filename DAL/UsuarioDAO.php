<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 29/04/2018
 * Time: 21:52
 */
namespace DAL;

use DAL\Conexao;
use Model\Usuario;

class UsuarioDAO
{
    /**
     * @var Conexao $pdo
     */
    private $pdo;
    private $debug;

    /**
     * UsuarioDAO constructor.
     */
    public function __construct()
    {
        $this->pdo = new Conexao();
        $this->debug = true;
    }

    function Gravar(Usuario $user){
        try{
            $sql = "INSERT INTO usuario(nome, usuario, email, senha, ativo, perfil, idigreja) VALUES(:nome, :usuario, :email, :senha, :ativo, :perfil, :idigreja)";
            $param = array(
                ":nome" => $user->getNome(),
                ":usuario" => $user->getUsuario(),
                ":email" => $user->getEmail(),
                ":senha" => $user->getSenha(),
                ":ativo" => $user->getAtivo(),
                ":perfil" => $user->getPerfil(),
                ":idigreja" => $user->igreja->getId()
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);

        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    public function VerificaUsuarioExiste(string $user)
    {
        try{
            $sql = "SELECT usuario FROM usuario WHERE usuario = :usuario";
            $param = array(
                ":usuario" => $user
            );

            $retorno = $this->pdo->ExecuteQueryOneRow($sql, $param);

            if(!empty($retorno)){
                return 1;
            }else{
                return -1;
            }

        }catch (\PDOException $ex){
            if($this->debug){
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
           return null;
        }
    }

    public function VerificaEmailExiste(string $email)
    {
        try {
            $sql = "SELECT email FROM usuario WHERE email = :email";

            $param = array(
                ":email" => $email
            );

            $retorno = $this->pdo->ExecuteQueryOneRow($sql, $param);


            if (!empty($retorno)) {
                return 1;
            } else {
                return -1;
            }
        } catch (PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    public function ObterUsuarios()
    {
        try{
            $sql = "SELECT u.idusuario, u.nome, u.perfil, u.idigreja, i.nome as igrejanome
            FROM usuario u
            INNER JOIN igreja i ON u.idigreja = i.idigreja 
            GROUP BY u.nome ASC";

            $dados = $this->pdo->ExecuteQuery($sql);
            $listaUsuarios = [];

            foreach ($dados as $u) {
                $usuario = new Usuario();
                $usuario->setId($u['idusuario']);
                $usuario->setNome($u['nome']);
                $usuario->setPerfil($u['perfil']);
                $usuario->igreja->setId($u['idigreja']);
                $usuario->igreja->setNome($u['igrejanome']);
                $listaUsuarios[] = $usuario;
            }

            return $listaUsuarios;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

}