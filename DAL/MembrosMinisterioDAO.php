<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 20/05/2018
 * Time: 15:34
 */

namespace DAL;

use DAL\Conexao;
use Model\MembrosMinisterio;

class MembrosMinisterioDAO
{
    private $pdo;
    private $debug;

    /**
     * MembrosMinisterioDAO constructor.
     */
    public function __construct()
    {
        $this->pdo = new Conexao();
        $this->debug = true;
    }

    public function Gravar(MembrosMinisterio $membro)
    {
        try {
            $sql = "INSERT INTO membros_ministerio(idmembro, idministerio) VALUES (:idmembro, :idministerio)";
            $param = array(
                ":idmembro" => $membro->membro->getId(),
                ":idministerio" => $membro->ministerio->getId()
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        } catch (\PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * Deletar
     *
     * Exclui um membro de um determinado ministerio
     *
     * @param $idMembro
     * @param $idMinisterio
     * @return bool
     */
    public function Deletar($idMembro, $idMinisterio)
    {
        try{
            $sql = "DELETE FROM membros_ministerio WHERE idmembro = :idmembro AND idministerio = :idministerio";
            $param = array(
                ":idmembro" => $idMembro,
                ":idministerio" => $idMinisterio
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * @param MembrosMinisterio $memMinisterio
     * @return one|null
     */
    public function VerificaCadastro(MembrosMinisterio $memMinisterio)
    {
        try{
            $sql = "SELECT idmembro FROM membros_ministerio WHERE idmembro = :id AND idministerio = :idministerio";
            $param = array(
                ":id" => $memMinisterio->membro->getId(),
                ":idministerio" => $memMinisterio->ministerio->getId()
            );

            $row = $this->pdo->ExecuteQueryOneRow($sql, $param);
            return $row;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    /**
     * @param $idIgrejaSessao
     * @param $idMinisterio
     * @return array|null
     */
    public function ObterMembrosMinisterio($idMinisterio)
    {
        try{
            $sql = "SELECT mm.idmembro, mm.idministerio, m.nome as nomeMembro
                    FROM membros_ministerio mm 
                    INNER JOIN membro m ON mm.idmembro = m.idmembro
                    WHERE mm.idministerio = :idministerio";
            $param = array(
                ":idministerio" => $idMinisterio
            );

            $dados = $this->pdo->ExecuteQuery($sql, $param);
            $listaMembros = [];

            foreach ($dados as $mem){
                $memMinisterio = new MembrosMinisterio();
                $memMinisterio->membro->setId($mem['idmembro']);
                $memMinisterio->ministerio->setId($mem['idministerio']);
                $memMinisterio->membro->setNome($mem['nomeMembro']);
                $listaMembros[] = $memMinisterio;
            }

            return $listaMembros;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

}