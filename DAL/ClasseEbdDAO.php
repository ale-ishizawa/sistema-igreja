<?php
/**
 * Created by PhpStorm.
 * User: Alessandro
 * Date: 13/06/2018
 * Time: 17:45
 */

namespace DAL;

use DAL\Conexao;
use Model\ClasseEbd;

class ClasseEbdDAO
{
    private $pdo;
    private $debug;

    /**
     * ClasseEbdDAO constructor.
     */
    public function __construct()
    {
        $this->pdo = new Conexao();
        $this->debug = true;
    }

    /**
     * @param ClasseEbd $classe
     * @return bool
     */
    public function Gravar(ClasseEbd $classe)
    {
        try{

            $sql = "INSERT INTO classe_ebd(datainicio, dataencerramento, nome, descricao, idigreja)
                    VALUES(:inicio, :encerramento, :nome, :descricao, :idigreja)";
            $param = array(
                ":inicio" => $classe->getDataInicio(),
                ":encerramento" => $classe->getDataEncerramento(),
                ":nome" => $classe->getNome(),
                "descricao" => $classe->getDescricao(),
                ":idigreja" => $classe->igreja->getId()
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        }catch (\PDOException $ex)
        {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * @param ClasseEbd $classe
     * @return bool
     */
    public function Editar(ClasseEbd $classe)
    {
        try{
            $sql = "UPDATE classe_ebd
                    SET descricao = :descricao, 
                    datainicio = :inicio, 
                    dataencerramento =  :encerramento, 
                    nome = :nome
                    WHERE idclasse = :id";
            $param = array(
                ":descricao" => $classe->getDescricao(),
                ":inicio" => $classe->getDataInicio(),
                ":encerramento" => $classe->getDataEncerramento(),
                ":nome" => $classe->getNome(),
                ":id" => $classe->getId()
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }


    /**
     * @param $id
     * @return ClasseEbd|null
     */
    public function ObterClassePorCod($id)
    {
        try{
            $sql = "SELECT * FROM classe_ebd WHERE idclasse = :id";
            $param = array(
                ":id" => $id
            );

            $result = $this->pdo->ExecuteQueryOneRow($sql, $param);

            $classe = new ClasseEbd();
            $classe->setId($result['idclasse']);
            $classe->setDataInicio($result['datainicio']);
            $classe->setDataEncerramento($result['dataencerramento']);
            $classe->setDescricao($result['descricao']);
            $classe->setNome($result['nome']);

            return $classe;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    /**
     * @param $idIgreja
     * @return array|null
     */
    public function ObterClasses($idIgreja)
    {
        try{
            $sql = "SELECT * FROM classe_ebd WHERE idigreja = :idigreja";
            $param = array(
                ":idigreja" => $idIgreja
            );

            $dados = $this->pdo->ExecuteQuery($sql, $param);
            $listaClasses = [];

            foreach ($dados as $c){
                $classe = new ClasseEbd();
                $classe->setId($c['idclasse']);
                $classe->setNome($c['nome']);
                $classe->setDescricao($c['descricao']);
                $classe->setDataInicio($c['datainicio']);
                $classe->setDataEncerramento($c['dataencerramento']);
                $listaClasses[] = $classe;
            }

            return $listaClasses;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    /**
     * @param $id
     * @return bool
     */
    public function Deletar($id)
    {
        try{
            //Obtém os dados da classe
            $classe = ClasseEbdDAO::ObterClassePorCod($id);
            if($classe->getId() > 0){
                $sql = "DELETE FROM turma WHERE idclasse = :id";
                $param = array(
                    ":id" => $id
                );
                if($this->pdo->ExecuteNonQuery($sql, $param)){
                    $sql = "DELETE FROM classe_ebd WHERE idclasse = :id";
                    $param = array(
                        ":id" => $id
                    );
                    return $this->pdo->ExecuteNonQuery($sql, $param);
                }
            }else{
                return false;
            }
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

}