<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 07/05/2018
 * Time: 22:26
 */

namespace DAL;

use DAL\Conexao;
use DAL\CidadeDAO;

use Model\Cidade;
use Model\Igreja;

class IgrejaDAO
{
    private $pdo;
    private $debug;
    private $cidDAO;

    /**
     * IgrejaDAO constructor.
     */
    public function __construct()
    {
        $this->pdo = new Conexao();
        $this->debug = true;
        $this->cidDAO = new CidadeDAO();
    }

    /**
     * @param Igreja $igreja
     * @return bool
     */
    public function Gravar(Igreja $igreja)
    {
        try {
            $cidade = new Cidade();
            $cidade->setNome($igreja->cidade->getNome());
            $cidade->setUf($igreja->cidade->getUf());

            //Verifico se a cidade já existe no banco de dados, se existir não é necessário realizar o cadastro
            $existe = $this->cidDAO->ObterCidade($cidade);

            if ($existe->getId() == NULL) {
                //Gravo a cidade
                $retorno = $this->cidDAO->Gravar($cidade);
                if ($retorno == true) {
                    $igreja->cidade->setId($this->pdo->GetLastID());
                }
            } else {
                $igreja->cidade->setId($existe->getId());
            }

            //Salvo os dados da igreja
            $sql = "INSERT INTO igreja(telefone, email, site, nome, cnpj, ie, sede, logradouro, numero, complemento, bairro, cep, idcidade, ativo) 
                    VALUES(:telefone, :email, :site, :nome, :cnpj, :ie, :sede, :logradouro, :numero, :complemento, :bairro, :cep, :cidade, :ativo)";
            $param = array(
                ":telefone" => $igreja->getTelefone(),
                ":email" => $igreja->getEmail(),
                ":site" => $igreja->getSite(),
                ":nome" => $igreja->getNome(),
                ":cnpj" => $igreja->getCnpj(),
                ":ie" => $igreja->getIe(),
                ":sede" => $igreja->getSede(),
                ":logradouro" => $igreja->getLogradouro(),
                ":numero" => $igreja->getNumero(),
                ":complemento" => $igreja->getComplemento(),
                ":bairro" => $igreja->getBairro(),
                ":cep" => $igreja->getCep(),
                ":cidade" => $igreja->cidade->getId(),
                ":ativo" => $igreja->getAtivo()
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);

        } catch (\PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * @param Igreja $igreja
     * @return bool
     */
    public function Editar(Igreja $igreja)
    {
        try {
            $cidade = new Cidade();
            $cidade->setNome($igreja->cidade->getNome());
            $cidade->setUf($igreja->cidade->getUf());

            //Verifico se a cidade já existe no banco de dados, se existir não é necessário realizar o cadastro
            $existe = $this->cidDAO->ObterCidade($cidade);

            if ($existe->getId() == NULL) {
                //Gravo a cidade
                $retorno = $this->cidDAO->Gravar($cidade);
                if ($retorno == true) {
                    $igreja->cidade->setId($this->pdo->GetLastID());
                }
            } else {
                $igreja->cidade->setId($existe->getId());
            }

            $sql = "UPDATE igreja 
                    SET telefone = :telefone,
                    email = :email,
                    site = :site,
                    nome = :nome,
                    cnpj = :cnpj,
                    ie = :ie,
                    logradouro = :logradouro,
                    numero = :numero,
                    complemento = :complemento,
                    bairro = :bairro,
                    cep = :cep,
                    sede = :sede,
                    idcidade = :idcidade                      
                    WHERE idigreja = :id";
            $param = array(
                ":telefone" => $igreja->getTelefone(),
                ":email" => $igreja->getEmail(),
                ":site" => $igreja->getSite(),
                ":nome" => $igreja->getNome(),
                ":cnpj" => $igreja->getCnpj(),
                ":ie" => $igreja->getIe(),
                ":sede" => $igreja->getSede(),
                ":logradouro" => $igreja->getLogradouro(),
                ":numero" => $igreja->getNumero(),
                ":complemento" => $igreja->getComplemento(),
                ":bairro" => $igreja->getBairro(),
                ":cep" => $igreja->getCep(),
                ":id" => $igreja->getId(),
                ":idcidade" => $igreja->cidade->getId()
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);

        } catch (\PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * @return array|null
     */
    public function ObterIgrejasSelect()
    {
        try {
            $sql = "SELECT idigreja, nome FROM igreja WHERE ativo = 1 ORDER BY nome ASC";

            $dados = $this->pdo->ExecuteQuery($sql);
            $listaIgrejas = [];

            foreach ($dados as $ig) {
                $igreja = new Igreja();
                $igreja->setId($ig['idigreja']);
                $igreja->setNome($ig['nome']);
                $listaIgrejas[] = $igreja;
            }

            return $listaIgrejas;
        } catch (\PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    /**
     * @param $idIgreja
     * @return Igreja|null
     */
    public function ObterIgreja($idIgreja)
    {
        try {
            $sql = "SELECT i.idigreja, i.ativo, i.telefone, i.email, i.site, i.nome as igrejanome, i.cnpj, i.ie, i.sede, i.logradouro, i.numero, i.complemento, i.bairro,
                    i.cep, cid.nome as cidadenome, cid.uf 
                    FROM igreja i 
                    INNER JOIN cidade cid ON i.idcidade = cid.idcidade
                    WHERE idigreja = :id";
            $param = array(
                ":id" => $idIgreja
            );

            $dt = $this->pdo->ExecuteQueryOneRow($sql, $param);
            $igreja = new Igreja();
            $igreja->setId($dt['idigreja']);
            $igreja->setNome($dt['igrejanome']);
            $igreja->setCnpj($dt['cnpj']);
            $igreja->setEmail($dt['email']);
            $igreja->setBairro($dt['bairro']);
            $igreja->setComplemento($dt['complemento']);
            $igreja->setNumero($dt['numero']);
            $igreja->setLogradouro($dt['logradouro']);
            $igreja->setCep($dt['cep']);
            $igreja->setIe($dt['ie']);
            $igreja->setSede($dt['sede']);
            $igreja->setSite($dt['site']);
            $igreja->setTelefone($dt['telefone']);
            $igreja->cidade->setNome($dt['cidadenome']);
            $igreja->cidade->setUf($dt['uf']);
            $igreja->setAtivo($dt['ativo']);

            return $igreja;
        } catch (\PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    /**
     * Inativar
     *
     * Inativa a igreja ao invés de excluir
     */
    public function Inativar($idIgreja)
    {
        try{
            $sql = "UPDATE igreja SET ativo = 2 WHERE idigreja = :id";
            $param = array(
                ":id" => $idIgreja
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }


}