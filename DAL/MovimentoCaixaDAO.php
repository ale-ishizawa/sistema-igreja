<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 31/05/2018
 * Time: 16:33
 */
namespace DAL;

use DateTime;
use Model\MovimentoCaixa;
use Model\Compra;

class MovimentoCaixaDAO
{
    private $pdo;
    private $debug;

    /**
     * MovimentoCaixaDAO constructor.
     */
    public function __construct()
    {
        $this->pdo = new Conexao();
        $this->debug = true;
    }

    /**
     * EstornarCompra
     *
     * Realiza o estorno de uma compra. Grava uma nova movimentação de caixa do tipo 1(Crédito), como devolução do valor da compra
     *
     * @param Compra $compra
     * @return bool
     */
    public function EstornarCompra(Compra $compra, $idCaixa)
    {
        try{
            $sql = "INSERT INTO movimento_caixa(idcaixa, idigreja, descricao, tipo, valor, idcompra, idusuario)
                    VALUES(:idcaixa, :idigreja, :descricao, :tipo, :valor, :idcompra, :idusuario)";
            $param = array(
                ":idcaixa" => $idCaixa,
                ":idigreja" => $compra->igreja->getId(),
                ":descricao" => "Estorno de Compra",
                ":tipo" => 1,
                ":valor" => $compra->getValor(),
                ":idcompra" => $compra->getId(),
                ":idusuario" => $_SESSION['idusuario']
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        } catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * @param MovimentoCaixa $movimento
     * @return bool
     */
    public function Gravar(MovimentoCaixa $movimento)
    {
        try{
            $sql = "INSERT INTO movimento_caixa(idcaixa, idigreja, descricao, tipo, valor, datamovimento, idusuario)
                    VALUES(:idcaixa, :idigreja, :descricao, :tipo, :valor, :datamovimento, :idusuario)";
            $param = array(
                ":idcaixa" => $movimento->caixa->getId(),
                ":idigreja" => $movimento->igreja->getId(),
                ":descricao" => $movimento->getDescricao(),
                ":tipo" => $movimento->getTipo(),
                ":valor" => $movimento->getValor(),
                ":datamovimento" => $movimento->getData(),
                ":idusuario" => $movimento->usuario->getId()
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * ObterMovimentacoes
     *
     * Obtém as movimentações de caixa dos últimos 30 dias de uma determinada igreja.
     *
     * @param $idIgreja
     * @return array|null
     * @throws \Exception
     */
    public function ObterMovimentacoes($idIgreja)
    {
        try{
            $dataAtual = new DateTime();
            $interval = new \DateInterval('P1M');
            $dataAtual->sub($interval);
            $dataParam = $dataAtual->format('Y-m-d');

            $sql = "SELECT mov.idmovimentocaixa, mov.idigreja, mov.datamovimento, mov.descricao, mov.tipo, mov.valor, u.nome 
                    FROM movimento_caixa mov
                    INNER JOIN usuario u ON mov.idusuario = u.idusuario
                    WHERE datamovimento > :dataparam AND mov.idigreja = :idigreja
                    GROUP BY mov.idmovimentocaixa DESC";
            $param = array(
                ":dataparam" => $dataParam,
                ":idigreja" => $idIgreja
            );

            $dados = $this->pdo->ExecuteQuery($sql, $param);
            $listaMovimentacoes = [];

            foreach ($dados as $mov){
                $movimento = new MovimentoCaixa();
                $movimento->setId($mov['idmovimentocaixa']);
                $movimento->igreja->setId($mov['idigreja']);
                $movimento->setData($mov['datamovimento']);
                $movimento->setDescricao($mov['descricao']);
                $movimento->setTipo($mov['tipo']);
                $movimento->setValor($mov['valor']);
                $movimento->usuario->setNome($mov['nome']);
                $listaMovimentacoes[] = $movimento;
            }

            return $listaMovimentacoes;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    public function RelatorioMovimentacoes($idIgreja, $inicio, $fim)
    {
        try{
            $sql = "SELECT idmovimentocaixa, datamovimento, descricao, tipo, valor, u.nome
                    FROM movimento_caixa 
                    INNER JOIN usuario u ON movimento_caixa.idusuario = u.idusuario
                    WHERE datamovimento BETWEEN :inicio AND :fim AND movimento_caixa.idigreja = :idigreja";
            $param = array(
                ":inicio" => $inicio,
                ":fim" => $fim,
                ":idigreja" => $idIgreja
            );

            $dados = $this->pdo->ExecuteQuery($sql, $param);
            $listaMovimentos = [];

            foreach ($dados as $m){
                $movimento = new MovimentoCaixa();
                $movimento->setId($m['idmovimentocaixa']);
                $movimento->setData($m['datamovimento']);
                $movimento->setValor($m['valor']);
                $movimento->setDescricao($m['descricao']);
                $movimento->usuario->setNome($m['nome']);
                $movimento->setTipo($m['tipo']);
                $listaMovimentos[] = $movimento;
            }

            return $listaMovimentos;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    public function RelatorioFinanceiro($idIgreja, $inicio, $fim)
    {
        try{
            $sql = "SELECT SUM(valor) as soma, tipo
                    FROM movimento_caixa
                    WHERE idigreja = :idigreja AND datamovimento BETWEEN :inicio AND :fim
                    GROUP BY tipo";
            $param = array(
                ":idigreja" =>$idIgreja,
                ":inicio" => $inicio,
                ":fim" => $fim
            );

            return $this->pdo->ExecuteQuery($sql, $param);
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

}