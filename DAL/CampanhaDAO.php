<?php
/**
 * Created by PhpStorm.
 * User: Alessandro
 * Date: 17/05/2018
 * Time: 17:36
 */

namespace DAL;

use DAL\Conexao;
use Model\Campanha;
use DateTime;

class CampanhaDAO
{
    /**
     * @var Conexao $pdo
     */
    private $pdo;
    private $debug;

    /**
     * CampanhaDAO constructor.
     */
    public function __construct()
    {
        $this->pdo = new Conexao();
        $this->debug = true;
    }

    public function Gravar(Campanha $campanha)
    {
        try {
            $sql = "INSERT INTO campanha(ativo, datainicio, datafim, descricao, nome, valoralvo, idigreja)
                    VALUES(:ativo, :inicio, :fim, :descricao, :nome, :valor, :igreja)";
            $param = array(
                ":ativo" => 1,
                ":inicio" => $campanha->getDataInicio(),
                ":fim" => $campanha->getDataFim(),
                ":descricao" => $campanha->getDescricao(),
                ":nome" => $campanha->getNome(),
                ":valor" => $campanha->getValorAlvo(),
                ":igreja" => $campanha->igreja->getId()
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);

        } catch (\PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * Encerrar
     *
     * Inativa uma campanha
     *
     * @param $idCampanha
     * @return bool
     */
    public function Encerrar($idCampanha)
    {
        try {
            $sql = "UPDATE campanha 
                    SET ativo = 2 
                    WHERE idcampanha = :id";
            $param = array(
                ":id" => $idCampanha
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);

        } catch (\PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * Editar
     * Realiza a alteração dos dados da campanha,
     *
     * @param Campanha $campanha
     * @return bool
     */
    public function Editar(Campanha $campanha)
    {
        try {
            $sql = "UPDATE campanha 
                    SET datainicio = :inicio,
                    datafim = :fim, 
                    descricao = :descricao,
                    nome = :nome,
                    valoralvo = :valor
                    WHERE idcampanha = :id";
            $param = array(
                ":inicio" => $campanha->getDataInicio(),
                ":fim" => $campanha->getDataFim(),
                ":descricao" => $campanha->getDescricao(),
                ":nome" => $campanha->getNome(),
                ":valor" => $campanha->getValorAlvo(),
                ":id" => $campanha->getId()
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);

        } catch (\PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * Deletar
     *
     * Exclui uma campanha que não possui lançamentos, se tiver apenas inativa a campanha e altera a datafim para a data atual
     * da operação.
     *
     * @param $id
     * @return bool
     */
    public function Deletar($id)
    {
        try {
            $sql = "SELECT idoferta, idcampanha FROM oferta WHERE idcampanha = :idcampanha";
            $param = array(
                ":idcampanha" => $id
            );

            $result = $this->pdo->ExecuteQueryOneRow($sql, $param);
            if($result != false){
                //Existe algum registo de oferta com a campanha, nesse caso apenas inativa a campanha, para manter o registro das ofertas.
                $sql = "UPDATE campanha SET ativo = 2 WHERE idcampanha = :id";
                $param = array(
                    ":id" => $id
                );

                return $this->pdo->ExecuteNonQuery($sql, $param);
            }else{
                //REALIZA A EXCLUSÃO
                $sql = "DELETE FROM campanha WHERE idcampanha = :id";
                $param = array(
                    ":id" => $id
                );

                return $this->pdo->ExecuteNonQuery($sql, $param);
            }
        } catch (\PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * ObterCampanhas
     *
     * Retorna as campanhas ativas da igreja relacionada ao usuário logado.
     *
     * @return array|null
     * @param int $idIgreja
     */
    public function ObterCampanhas($idIgreja)
    {
        $dataAtual = new DateTime();
        $dataParam = $dataAtual->format('Y-m-d');

        try {
            $sql = "SELECT * FROM campanha WHERE ativo = 1 AND idigreja = :id AND datafim > :datahoje";
            $param = array(
                ":id" => $idIgreja,
                ":datahoje" => $dataParam
            );

            $dados = $this->pdo->ExecuteQuery($sql, $param);
            $listaCampanhas = [];

            foreach ($dados as $c) {
                $campanha = new Campanha();
                $campanha->setId($c['idcampanha']);
                $campanha->setNome($c['nome']);
                $campanha->setDescricao($c['descricao']);
                $campanha->setDataInicio($c['datainicio']);
                $campanha->setDataFim($c['datafim']);
                $campanha->setValorAlvo($c['valoralvo']);
                $campanha->igreja->setId($c['idigreja']);
                $listaCampanhas[] = $campanha;
            }

            return $listaCampanhas;

        } catch (\PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    /**
     * ObterCampanha
     *
     * Retorna uma campanha para edição dos dados
     *
     * @param $id
     * @return Campanha|null
     */
    public function ObterCampanha($id)
    {
        try {
            $sql = "SELECT * FROM campanha WHERE idcampanha = :id";
            $param = array(
                ":id" => $id
            );

            $c = $this->pdo->ExecuteQueryOneRow($sql, $param);

            $campanha = new Campanha();
            $campanha->setId($c['idcampanha']);
            $campanha->setNome($c['nome']);
            $campanha->setDescricao($c['descricao']);
            $campanha->setDataInicio($c['datainicio']);
            $campanha->setDataFim($c['datafim']);
            $campanha->setValorAlvo($c['valoralvo']);
            $campanha->igreja->setId($c['idigreja']);

            return $campanha;

        } catch (\PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    /**
     * VerificaLancamentoCampanha
     *
     * Verifica se a campanha já teve algum lançamento de oferta.
     *
     * @param $id
     * @return one|null
     */
    public function VerificaLancamentoCampanha($id)
    {
        try {
            $sql = "SELECT idcampanha FROM oferta WHERE idcampanha = :id";
            $param = array(
                ":id" => $id
            );

            return $this->pdo->ExecuteQueryOneRow($sql, $param);
        } catch (\PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    public function RelatorioCampanhas($idIgreja, $inicio, $fim)
    {
        try{
            $sql = "SELECT c.descricao, c.nome, c.valoralvo, SUM(o.valor) as soma
                    FROM campanha c
                    INNER JOIN oferta o ON c.idcampanha = o.idcampanha
                    WHERE c.idigreja = :idigreja AND o.data BETWEEN :inicio AND :fim AND o.ativo = 1
                    GROUP BY c.idcampanha";
            $param = array(
                ":idigreja" => $idIgreja,
                ":inicio" => $inicio,
                ":fim" => $fim
            );

            $dados = $this->pdo->ExecuteQuery($sql, $param);
            return $dados;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }
}