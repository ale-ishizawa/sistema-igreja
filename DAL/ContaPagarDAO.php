<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 01/06/2018
 * Time: 19:12
 */
namespace DAL;
session_start();
use Model\ContaPagar;
use DateTime;


class ContaPagarDAO
{

    /**
     * @var Conexao $pdo
     */
    private $pdo;
    private $debug;

    /**
     * ContaPagarDAO constructor.
     */
    public function __construct()
    {
        $this->pdo = new Conexao();
        $this->debug = true;
    }

    /**
     * @param ContaPagar $contaPagar
     * @return bool
     */
    public function Gravar(ContaPagar $contaPagar)
    {
        try{
            $sql = "INSERT INTO conta_pagar(ativo, datavencimento, valor, idtipoconta, idigreja) 
                    VALUES (1,:vencto, :valor, :tipo, :igreja)";
            $param = array(
                ":vencto" => $contaPagar->getDataVencimento(),
                ":valor" => $contaPagar->getValor(),
                ":tipo" => $contaPagar->tipoConta->getId(),
                ":igreja" => $contaPagar->igreja->getId()
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * @param ContaPagar $conta
     * @return bool
     */
    public function Editar(ContaPagar $conta)
    {
        try{
            $sql = "UPDATE conta_pagar
                    SET datavencimento = :vencto,
                    valor = :valor,
                    idtipoconta = :tipoconta
                    WHERE idcontapagar = :id";
            $param = array(
                ":vencto" => $conta->getDataVencimento(),
                ":valor" => $conta->getValor(),
                ":tipoconta" => $conta->tipoConta->getId(),
                ":id" => $conta->getId()
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        }catch (\PDOException $ex){

        }
    }

    /**
     * @param $id
     * @return bool
     */
    public function Deletar($id)
    {
        try{
            $sql = "DELETE FROM conta_pagar WHERE idcontapagar = :id";
            $param = array(
                ":id" => $id
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        }catch (\PDOException $ex){
            if($this->debug){
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * ObterContasPagar
     *
     * Obtém todas as contas que estão em aberto/vencidas de uma determinada igreja
     *
     * @param $idIgreja
     * @return array|null
     */
    public function ObterContasPagar($idIgreja)
    {
        try{
            $sql = "SELECT *, tc.descricao  
                    FROM conta_pagar
                    INNER JOIN tipo_conta tc ON conta_pagar.idtipoconta = tc.idtipoconta
                    WHERE idigreja = :idigreja AND datapagamento IS NULL OR datapagamento = '0000-00-00'";
            $param = array(
                ":idigreja" =>$idIgreja
            );

            $dados = $this->pdo->ExecuteQuery($sql, $param);
            $listaContas = [];

            foreach ($dados as $con){
                $conta = new ContaPagar();
                $conta->setId($con['idcontapagar']);
                $conta->setValor($con['valor']);
                $conta->setDataVencimento($con['datavencimento']);
                $conta->tipoConta->setDescricao($con['descricao']);
                $listaContas[] = $conta;
            }
            return $listaContas;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

    /**
     * @param $idConta
     * @param $idCaixa
     * @return bool
     */
    public function BaixarConta($idConta, $idCaixa)
    {
        try{
            $dataPagto = date('Y-m-d H:i:s');

            $sql = "UPDATE conta_pagar 
                    SET datapagamento = :datapagto
                    WHERE idcontapagar = :id";
            $param = array(
                ":datapagto" => $dataPagto,
                ":id" => $idConta
            );

            if($this->pdo->ExecuteNonQuery($sql, $param)){
                //Se a conta for paga, gero a movimentação no caixa, do tipo 2(Débito)
                $conta = $this->ObterContaPorCod($idConta);
                if($conta->getId() != null){
                    $dataMovimento = date('Y-m-d H:i:s');

                    $sql = "INSERT INTO movimento_caixa(idcaixa, idigreja, descricao, tipo, valor, idcontapagar, idusuario, datamovimento) 
                            VALUES(:idcaixa, :idigreja, :descricao, :tipo, :valor, :idcontapagar, :idusuario, :datamovimento)";
                    $param = array(
                        ":idcaixa" => $idCaixa,
                        ":idigreja" => $conta->igreja->getId(),
                        ":descricao" => $conta->tipoConta->getDescricao(),
                        ":tipo" => 2,
                        ":valor" => $conta->getValor(),
                        ":idcontapagar" => $conta->getId(),
                        ":idusuario" => $_SESSION['idusuario'],
                        ":datamovimento" => $dataMovimento
                    );

                    return $this->pdo->ExecuteNonQuery($sql, $param);
                }
            }else{
                return false;
            }
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * @param $idConta
     * @return ContaPagar|null
     */
    public function ObterContaPorCod($idConta)
    {
        try{
            $sql = "SELECT cp.idcontapagar, cp.datavencimento, cp.datapagamento, cp.valor, cp.idigreja, tc.descricao, tc.idtipoconta
                    FROM conta_pagar cp
                    INNER JOIN tipo_conta tc ON cp.idtipoconta = tc.idtipoconta
                    WHERE idcontapagar = :id";
            $param = array(
                ":id" => $idConta
            );

            $c = $this->pdo->ExecuteQueryOneRow($sql, $param);

            $conta = new ContaPagar();
            $conta->setId($c['idcontapagar']);
            $conta->setDataVencimento($c['datavencimento']);
            $conta->setDataPagamento($c['datapagamento']);
            $conta->setValor($c['valor']);
            $conta->igreja->setId($c['idigreja']);
            $conta->tipoConta->setId($c['idtipoconta']);
            $conta->tipoConta->setDescricao($c['descricao']);

            return $conta;

        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }
}