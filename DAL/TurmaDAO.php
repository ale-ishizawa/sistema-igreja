<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 15/06/2018
 * Time: 01:46
 */

namespace DAL;

use Model\Turma;

class TurmaDAO
{
    private $pdo;
    private $debug;

    /**
     * TurmaDAO constructor.
     */
    public function __construct()
    {
        $this->pdo = new Conexao();
        $this->debug = true;
    }

    /**
     * @param Turma $turma
     * @return bool
     */
    public function Gravar(Turma $turma)
    {
        try {
            $sql = "INSERT INTO turma(idmembro, idclasse) VALUES (:idmembro, :idclasse)";
            $param = array(
                ":idmembro" => $turma->membro->getId(),
                ":idclasse" => $turma->classe->getId()
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        } catch (\PDOException $ex) {
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }

    /**
     * @param $idMembro
     * @param $idClasse
     * @return bool
     */
    public function Deletar($idMembro, $idClasse)
    {
        try{
            $sql = "DELETE FROM turma WHERE idmembro = :idmembro AND idclasse = :idclasse";
            $param = array(
                ":idmembro" => $idMembro,
                ":idclasse" => $idClasse
            );

            return $this->pdo->ExecuteNonQuery($sql, $param);
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return false;
        }
    }


    /**
     * @param Turma $turma
     * @return one|null
     */
    public function VerificaCadastro(Turma $turma)
    {
        try{
            $sql = "SELECT idmembro FROM turma WHERE idmembro = :id AND idclasse = :idclasse";
            $param = array(
                ":id" => $turma->membro->getId(),
                ":idclasse" => $turma->classe->getId()
            );

            $row = $this->pdo->ExecuteQueryOneRow($sql, $param);
            return $row;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }


    /**
     * @param $idClasse
     * @return array|null
     */
    public function ObterMembrosTurma($idClasse)
    {
        try{
            $sql = "SELECT t.idmembro, t.idclasse, m.nome as nomeMembro
                    FROM turma t 
                    INNER JOIN membro m ON t.idmembro = m.idmembro
                    WHERE t.idclasse = :idclasse";
            $param = array(
                ":idclasse" => $idClasse
            );

            $dados = $this->pdo->ExecuteQuery($sql, $param);
            $listaMembros = [];

            foreach ($dados as $mem){
                $turma = new Turma();
                $turma->membro->setId($mem['idmembro']);
                $turma->classe->setId($mem['idclasse']);
                $turma->membro->setNome($mem['nomeMembro']);
                $listaMembros[] = $turma;
            }

            return $listaMembros;
        }catch (\PDOException $ex){
            if ($this->debug) {
                echo "ERRO: {$ex->getMessage()} LINE: {$ex->getLine()}";
            }
            return null;
        }
    }

}