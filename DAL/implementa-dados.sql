/* INSERÇÃO DE IGREJA */

INSERT INTO cidade(nome, uf) VALUES ('Regente Feijó', 'SP');

INSERT INTO igreja(ativo, telefone, email, site, nome, cnpj, ie, sede, logradouro, numero, complemento, bairro, cep, idcidade)
VALUES (1, '1832791050', 'igreja@igreja.com.br', 'www.ibregentefeijo.com.br', 'Igreja Batista em Regente Feijó', '86.902.387/0001-97',
        '123456', '1', 'Av brigadeiro Tobias', '864', '', 'centro', '19570-000', 1);

#INSERT Cargo
INSERT INTO cargo(idcargo, nome, descricao)
VALUES (1, 'Tesoureiro (a)', 'Financeiro da igreja.'),
       (2, 'Secretário (a)', 'Administrativo da igreja.');

#INSERT Membro
INSERT INTO membro(ativo, nome, sexo, logradouro, numero, bairro, complemento, cep, telefone, celular, email, nivelescolar, nascimento,
                   naturalidade, rg, orgao, cpf, estadocivil, conjuge, datacasamento, nomepai, nomemae, databatismo, igrejaorigem, idigreja, idcargo, idcidade)
VALUES (1, 'Flávio Llorente', 'M', 'Av Teste', '102', 'Centro', 'Casa', '19570-000', '18996178342', '18996178342', 'flavio@teste.com', '2', '1990-05-06',
                'Regente Feijó', '12312321', 'SSP', '123123', '2', 'Cristiane L.', '1995-05-06', 'Nome do Pai', 'Nome da Mãe', '1995-05-06', 'Origem igreja', 1, 1, 1);