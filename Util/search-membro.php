<?php
include "Util/config.php";
session_start();
$idIgrejaSessao = isset($_SESSION['idigreja']) ? $_SESSION['idigreja'] : null;

// Dados da conexão com o banco de dados Localhost
//define('SERVER', 'robb0358.publiccloud.com.br:3306');
//define('DBNAME', 'ibrf_banco');
//define('USER', 'ibrf_admin');
//define('PASSWORD', 'raKf7@49');

// Dados da conexão com o banco de dados Produção
define('SERVER', 'robb0358.publiccloud.com.br:3306');
define('DBNAME', 'grigollette_database');
define('USER', 'grigo_admin');
define('PASSWORD', 'Nqqv6&90');

if(isset($_POST['search'])) {
    $search = $_POST['search'];

    // Configura uma conexão com o banco de dados
    $opcoes = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8');
    $conexao = new PDO("mysql:host=".SERVER."; dbname=".DBNAME, USER, PASSWORD, $opcoes);

    $select = $conexao->prepare("SELECT idmembro, nome FROM membro WHERE nome LIKE '%".$search."%' AND idigreja = :idigreja");
    $select->bindParam(":idigreja", $idIgrejaSessao, PDO::PARAM_STR);
    $select->execute();

    $response = array();
    while($row = $select->fetch(PDO::FETCH_ASSOC)){
        $response[] = array(
            "value" => $row['idmembro'],
            "label" => $row['nome']
        );
    }

    echo json_encode($response);
}

exit;