<?php
/**
 * Created by PhpStorm.
 * User: AlessandroNavarro
 * Date: 25/04/2018
 * Time: 22:07
 */

switch ($_GET['page']) {

    //Gerenciar Membros
    case 'cadastro-membro':
        include("View/cadastro-membro.php");
        break;

    case 'membros':
        include ("View/membros.php");
        break;

    case 'perfil-membro':
        include ("View/perfil-membro.php");
        break;

    case 'relatorio-membros':
        include("View/relatorios/relatorio-membros.php");
        break;

    case 'gera-pdf-relatorio-membros':
        include("View/relatorios/gera-pdf-relatorio-membros.php");
        break;

    //Líderes
    case 'lideres':
        include ("View/lideres.php");
        break;

    //Pastores
    case 'pastores':
        include ("View/pastores.php");
        break;

    //Gerenciar Usuários
    case 'cadastro-usuario':
        include("View/cadastro-usuario.php");
        break;

    case 'usuarios':
        include("View/usuarios.php");
        break;

    //Gerenciar Igrejas
    case 'cadastro-igreja':
        include("View/cadastro-igreja.php");
        break;

    case 'dados-igreja':
        include("View/dados-igreja.php");
        break;

    //Gerenciar Cargos
    case 'cargos':
        include("View/cargos.php");
        break;

    //Gerenciar Compras
    case 'compras':
        include("View/compras.php");
        break;

    case 'relatorio-compras':
        include("View/relatorios/relatorio-compras.php");
        break;

    case 'gera-pdf-relatorio-compras':
        include("View/relatorios/gera-pdf-relatorio-compras.php");
        break;

    //Gerenciar Caixa
    case 'caixa':
        include("View/caixa.php");
        break;

    case 'movimentacoes-caixa':
        include("View/movimentacoes-caixa.php");
        break;

    case 'resumo-caixa':
        include("View/resumo-caixa.php");
        break;

    case 'relatorio-movimentacoes':
        include("View/relatorios/relatorio-movimentacoes.php");
        break;

    case 'gera-pdf-relatorio-movimentacoes':
        include("View/relatorios/gera-pdf-relatorio-movimentacoes.php");
        break;

    case 'relatorio-caixa':
        include("View/relatorios/relatorio-caixa.php");
        break;

    case 'gera-pdf-relatorio-caixa':
        include("View/relatorios/gera-pdf-relatorio-caixa.php");
        break;

    //Gerenciar Ministérios
    case 'ministerios':
        include("View/ministerios.php");
        break;

    //Membros Ministério
    case 'membros-ministerio':
        include("View/membros-ministerio.php");
        break;

    //Gerenciar Campanhas
    case 'campanhas':
        include("View/campanhas.php");
        break;

    case 'cadastro-campanha':
        include("View/cadastro-campanha.php");
        break;

    case 'relatorio-campanhas':
        include("View/relatorios/relatorio-campanhas.php");
        break;

    case 'gera-pdf-relatorio-campanhas':
        include("View/relatorios/gera-pdf-relatorio-campanhas.php");
        break;

    //Gerenciar Tipos de Conta
    case 'tipo-conta':
        include("View/tipos-conta.php");
        break;

    //Classes Ebd
    case 'classes-ebd':
        include("View/classes-ebd.php");
        break;

    case 'turma-ebd':
        include("View/turma-ebd.php");
        break;

    //Gerenciar Contas a Pagar
    case 'contas':
        include("View/contas.php");
        break;

    //Gerenciar Dízimos
    case 'dizimos':
        include("View/dizimos.php");
        break;

    case 'relatorio-dizimos':
        include("View/relatorios/relatorio-dizimos.php");
        break;

    case 'gera-pdf-relatorio-dizimos':
        include("View/relatorios/gera-pdf-relatorio-dizimos.php");
        break;

    //Gerenciar Ofertas
    case 'ofertas':
        include("View/ofertas.php");
        break;

    case 'relatorio-ofertas':
        include("View/relatorios/relatorio-ofertas.php");
        break;

    case 'gera-pdf-relatorio-ofertas':
        include("View/relatorios/gera-pdf-relatorio-ofertas.php");
        break;

    //Relatório Financeiro
    case 'relatorio-financeiro':
        include("View/relatorios/relatorio-financeiro.php");
        break;

    case 'gera-pdf-relatorio-financeiro':
        include("View/relatorios/gera-pdf-relatorio-financeiro.php");
        break;

    case 'dashboard':
        include ("View/dashboard.php");
        break;

    case 'validacao':
        include ("validacao.php");
        break;

    case 'login':
        include ("View/login.php");
        break;

    case 'logout':
        include ("logout.php");
        break;

    default: include ("View/login.php");
        break;
}
?>